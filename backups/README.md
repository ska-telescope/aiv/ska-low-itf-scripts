## This folder is intended to hold configuration information for ITF TE equipment 
## that is not currently being captured in other locations. For example, the 
## configuration of PDUs, rack controllers and the PTP/NTP network clock.