import time
import matplotlib.pyplot as plt
##import numpy as np
import calendar

import os
import sys
import subprocess

from datetime import datetime

import nmap

###############################################################################

## Phone hotspot IP address.
hotspot_lan = "172.20.10.0"
hotspot_prefix = "28"

## SCP variables. These should be changed based on the remote device.
msf_pi_user = "msf-pi"
msf_pi_addr = "10.135.150.225"
msf_pi_device_port = 22
msf_pi_mac = "DC:A6:32:39:F6:6A"
msf_pi_remote_path = "/home/%s/SKAO/sht30/temp_logs/*" % (msf_pi_user)
msf_pi_local_path = "./msf_pi_temp_logs"

## SCP variables. These should be changed based on the remote device.
msf_pi_two_user = "msf-pi-two"
msf_pi_two_addr = "10.135.150.226"
msf_pi_two_device_port = 22
msf_pi_two_mac = "E4:5F:01:AD:34:72"
msf_pi_two_remote_path = "/home/%s/SKAO/sht30/temp_logs/*" % (msf_pi_two_user)
msf_pi_two_local_path = "./msf_pi_two_temp_logs"

time_zone_offset = 8 * 3600

###############################################################################

def get_pi_ips():
    global msf_pi_addr, msf_pi_two_addr
    
    nmap_command = "nmap -sn %s/%s" % (hotspot_lan, hotspot_prefix)
    
    nmap_result = subprocess.check_output(nmap_command, shell=True)
    nmap_lines = (nmap_result.decode("utf-8")).splitlines()
    
    for i in range(0, len(nmap_lines)):
        if nmap_lines[i].find("Nmap scan report for") != -1:
            ip_address = (nmap_lines[i].split(" "))[-1]
            
            print(ip_address)
            
            if nmap_lines[i+2].find("MAC Address:") != -1:
                mac_address = ((nmap_lines[i+2].split(": "))[1]).split(" (")[0]
            
                if mac_address == msf_pi_mac:
                    print("msf-pi found on IP: %s" % (ip_address))
                    msf_pi_addr = ip_address
                    
                if mac_address == msf_pi_two_mac:
                    print("msf-pi-two found on IP: %s" % (ip_address))
                    msf_pi_two_addr = ip_address
    
    return
    
if os.path.isdir(msf_pi_local_path) != True:
    os.makedirs(msf_pi_local_path)
    
if os.path.isdir(msf_pi_two_local_path) != True:
    os.makedirs(msf_pi_two_local_path)
    
## Update the Pi IP addresses.
get_pi_ips()
print(msf_pi_addr)
print(msf_pi_two_addr)

## Copy the logging files from the controller and store locally.
ssh_proc = None
ssh_proc = subprocess.run("scp -P %i %s@%s:%s %s" % (msf_pi_device_port, msf_pi_user, msf_pi_addr, msf_pi_remote_path, msf_pi_local_path))


## Copy the logging files from the controller and store locally.
ssh_proc = None
ssh_proc = subprocess.run("scp -P %i %s@%s:%s %s" % (msf_pi_two_device_port, msf_pi_two_user, msf_pi_two_addr, msf_pi_two_remote_path, msf_pi_two_local_path))

## Normal exit.
sys.exit(0)
