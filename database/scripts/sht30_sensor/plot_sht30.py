import time
import matplotlib.pyplot as plt
##import numpy as np
import calendar

import os
import sys
import subprocess

from datetime import datetime

###############################################################################

## SCP variables. These should be changed based on the remote device.
device_user = "debian"
device_hostname = "skao-eoc.duckdns.org"
device_port = 5238

key_path = "./eoc_bbb_key"

remote_path = "/home/%s/temp_logs/*" % (device_user)
local_path = "./comms_room_temp_logs"

time_zone_offset = 8 * 3600

###############################################################################

## Three options to run the program:
## 1: python plot_sht30.py - Plots all data in local folder.
## 2: python plot_sht30.py <start_date> - Plots data from start_date to the present.
## 3: python plot_sht30.py <start_date> <end_date> - Plots data between start_date and end_date.

## Handle the input arguements.
start_time = 0 + time_zone_offset
end_time = time.time() + time_zone_offset

if len(sys.argv) > 3:
    print("Too many program call arguements.")
    print("Usage: python plot_sht30.py <start_date> <end_date>")
    print("Date format (UTC+8): YYYY-MM-DD")
    sys.exit(1)
    
if len(sys.argv) >= 2:
    try:
        ## Calculate the start time in UTC.
        start_time = calendar.timegm(time.strptime(sys.argv[1], "%Y-%m-%d"))
    except Exception as error:
        print(error)
        print("Usage: python plot_sht30.py <start_date> <end_date>")
        print("Date format (UTC+8): YYYY-MM-DD")
        sys.exit(2)
        
if len(sys.argv) == 3:
    try:
        ## Calculate the end time in UTC.
        end_time = calendar.timegm(time.strptime(sys.argv[2], "%Y-%m-%d"))
    except Exception as error:
        print(error)
        print("Usage: python plot_sht30.py <start_date> <end_date>")
        print("Date format (UTC+8): YYYY-MM-DD")
        sys.exit(2)

ssh_proc = None

## Copy the logging files from the controller and store locally.
ssh_proc = subprocess.run("scp -P %i -i %s %s@%s:%s %s" % (device_port, key_path, device_user, device_hostname, remote_path, local_path))

## Ingest the data files.
file_list = os.listdir(local_path)

line_list = []

for file_name in file_list:
    file_time_stamp = calendar.timegm((time.strptime(file_name[:10], "%Y-%m-%d")))

    if (file_time_stamp >= start_time) and (file_time_stamp <= end_time):
        with open(os.path.join(local_path, file_name), "r") as f:
            line_list += f.readlines()
            
data_time_stamps = []
data_temps = []
data_humidities = []
            
## Process the data.
for line in line_list:
    try:
        ## Strip all whitespace and split by commas.
        test_split = (line.replace(" ", "")).split(",")
        
        test_time = calendar.timegm(time.strptime(test_split[0], "%Y-%m-%d-%H-%M-%S"))
        test_temp = float(test_split[1])
        test_humid = float(test_split[2])
    except Exception as error:
        print("Line: " + line)
        print("Error: " + error)
        continue
    
    ## Data line is OK, add the data it contains to the arrays.
    data_time_stamps.append(test_time)
    data_temps.append(test_temp)
    data_humidities.append(test_humid)

## Convert the Unix time stamps to a date object, useful for plotting.
dates = []

for data_time_stamp in data_time_stamps:
    dates.append(datetime.fromtimestamp(data_time_stamp - time_zone_offset))

## Plot the data on a dual-axis graph.
print("Plotting data...")
fig, ax1 = plt.subplots()
fig.suptitle("EOC Comms Room Temperature and Relative Humidity vs Local Time")

ax1.set_xlabel("Local Time") # x label
ax1.set_ylabel("Temperature (*C)", color="r") # y label
##ax1.plot(data_time_stamps, data_temps, color="r", label="Temperature")
ax1.plot(dates, data_temps, color="r", label="Temperature")

ax2 = ax1.twinx()
ax2.set_ylabel("Relative Humidity (%)", color="b") # y label
##ax2.plot(data_time_stamps, data_humidities, color="b", label="Relative Humditity")
ax2.plot(dates, data_humidities, color="b", label="Relative Humditity")

##legend = fig.legend()
##plt.grid()

plt.show()

## Normal exit.
sys.exit(0)
