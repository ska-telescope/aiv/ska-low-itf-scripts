import numpy as np
import time
import socket
import csv
import pyvisa as visa

channel = 6
sample_rate = 800000000
a = 0.2
record_length = 837760
f = [50000000, 80000000, 110000000, 160000000, 220000000, 280000000, 340000000, 350000000]
new_wave_data_array = []
spec = []
local_port = 5025
device_ip = "10.135.151.42"
wave_name = '8tone'

for t in range(record_length):
    new_wave_data_array.append(a * np.sin(2 * np.pi * f[0] * (t / sample_rate))+a * np.sin(2 * np.pi * f[1] * (t / sample_rate))+a * np.sin(2 * np.pi * f[2] * (t / sample_rate))+a * np.sin(2 * np.pi * f[3] * (t / sample_rate))+a * np.sin(2 * np.pi * f[4] * (t / sample_rate))+a * np.sin(2 * np.pi * f[5] * (t / sample_rate))+a * np.sin(2 * np.pi * f[6] * (t / sample_rate))+a * np.sin(2 * np.pi * f[7] * (t / sample_rate)))

print(len(new_wave_data_array))





# Change this to connect to your AWG as needed
"""#################SEARCH/CONNECT#################"""
rm = visa.ResourceManager()
print(rm.list_resources())
awg = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (device_ip, local_port))

awg.timeout = 25000
awg.encoding = 'latin_1'
awg.write_termination = '\n'
awg.read_termination = '\n'

print(awg.query('*idn?'))

## Script overwrites any exisitng waveform with the same name.
awg.write('wlist:waveform:del \"%s\"' % wave_name)
awg.write('wlist:waveform:new "%s", %i' % (wave_name, record_length))

## Set up the AWG.
awg.write('clock:srate %i' % sample_rate)
awg.query('*opc?')

print(awg.query('system:error:all?'))

## Load the data array into the AWG.
load_command = 'wlist:waveform:data "%s", 0, %i, ' % (wave_name, record_length)
awg.write_binary_values(load_command, new_wave_data_array)
awg.query('*opc?')
print(awg.query('system:error:all?'))

## Close AWG connection
awg.close()

        
        
        
    
