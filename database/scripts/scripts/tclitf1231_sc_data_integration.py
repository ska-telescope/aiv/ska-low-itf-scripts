# -*- coding: utf-8 -*-
"""
@author: Lahiru.Raffel

Test instruments need to be pre-configured to run this script correctly
"""

import pyvisa as visa
import numpy as np
import time
import socket
import csv
from random import seed
import random.random as randnum
seed(1)


AWG_local_port = 5025
AWG_device_ip = "10.135.151.42"




#frequency_list = [105e6,115e6]
frequency_list = [85e6,95e6,105e6,115e6,190e6,200e6,210e6]
#AWG_power_list = [0.2,0.178,0.158,0.141,0.126,0.112,0.1,0.089,0.079,0.071]
AWG_power_list = [-2,-3,-4,-5,-6,-7,-8,-9,-10,-11]

channel = 6
sample_rate = 2400000000
amplitude = 0.2
record_length = 300000
new_wave_data_array = []
wave_name = 'tone_noise'



# Change this to connect to your AWG and RSA as needed
"""#################SEARCH/CONNECT#################"""
rm = visa.ResourceManager()
print(rm.list_resources())
awg = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (AWG_device_ip, AWG_local_port))


awg.timeout = 25000
awg.encoding = 'latin_1'
awg.write_termination = '\n'
awg.read_termination = '\n'



print(awg.query('*idn?'))
##awg.write('*rst')
##awg.write('*cls')




## Script overwrites any exisitng waveform with the same name.
awg.write('wlist:waveform:del \"%s\"' % wave_name)
awg.write('wlist:waveform:new "%s", %i' % (wave_name, record_length))
    
for t in range(record_length):
    new_wave_data_array.append(0.2 * np.sin(2 * np.pi * 90000000 * (t / sample_rate))+0.2 * np.sin(2 * np.pi * 105000000 * (t / sample_rate)))
    
new_wave_data = np.asarray(new_wave_data_array[0:-1])
print(len(new_wave_data))

## Load the data array into the AWG.
load_command = 'wlist:waveform:data "%s", 0, %i, ' % (wave_name, record_length)
awg.write_binary_values(load_command, new_wave_data)
awg.query('*opc?')


## Set up the AWG.
awg.write('clock:srate %i' % sample_rate)
    



awg.close()


    
