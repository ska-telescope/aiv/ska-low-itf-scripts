# -*- coding: utf-8 -*-
#
# This file is part of the SKA Low CBF Connector project
#
# Copyright (c) 2023 CSIRO
#
# Distributed under the terms of the CSIRO Open Source Software Licence
# Agreement. See LICENSE for more info.
#
# pylint: disable=attribute-defined-outside-init,too-many-instance-attributes,too-many-statements,no-name-in-module
"""SPS Traffic Analyser."""
import json
import logging
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
from numpy.fft import fft, fftshift
from scapy.all import UDP, PcapReader

logging.basicConfig(level=logging.INFO)
COLOURS = "rgbcmykrgb"
MAX_NUMBER_SPS_CHANNELS = 384


def get_hex_from_byte(byte, first, last):
    """
    Extract bits[first:last] from given byte(s) and converts into hex.

    Assumes valid inputs.
    """
    integer = int.from_bytes(byte, "big")
    binary = bin(integer)
    binary = binary[2:]  # extract '0b' prefix
    sub_binary_str = binary[first:last]
    return f"0x{int(sub_binary_str, 2):04x}"


ADC_SAMPLE_PERIOD = 1.28e-6
OS_FACTOR = 32 / 27
SAMPLE_RATE = (1 / ADC_SAMPLE_PERIOD) * (OS_FACTOR)
polarisation_colour = ["blue", "orange", "green", "red"]


class SPSAnalyser:
    """
    This class implements an analyser for SPD traffic.

    This class is mainly used to validate the CNIC and SPS data.
    """

    def __init__(self, configuration: str):
        """
        Do the initialisation of the SPS analyser.

        :param configuration: the json configuration string, it should
            contain the following for 2 stations and 2 channels:
            '{"coase_channels": [124, 125], "stations": [345, 350] }

        """
        self.configuration = json.loads(configuration)
        self.coarse_channels = self.configuration["coarse_channels"]
        self.stations = self.configuration["stations"]
        self.nb_station = len(self.stations)

    def inspect_spead(self, header, field=None):
        """
        Return spead field value. Assumes valid inputs.

        :params header: bytestring of spead header.
        :params field: interested spead field.

        """
        spead_lookup = {
            "magic_num": f"0x{int.from_bytes(header[0:1], 'big'):02x}",
            "version": f"0x{int.from_bytes(header[1:2], 'big'):02x}",
            "item_ptr_width": f"0x{int.from_bytes(header[2:3], 'big'):02x}",
            "heap_addr_width": f"0x{int.from_bytes(header[3:4], 'big'):02x}",
            "num_items": f"0x{int.from_bytes(header[6:8], 'big'):02x}",
            "heap_counter": get_hex_from_byte(bytes(header)[8:10], 1, 16),
            "logical_id": int.from_bytes(header[10:12], "big"),
            "packet_counter": int.from_bytes(header[12:16], "big"),
            "pkt_len": get_hex_from_byte(bytes(header)[16:18], 1, 16),
            "packet_payload_length": int.from_bytes(header[18:24], "big"),
            "sync_time": get_hex_from_byte(bytes(header)[24:26], 1, 16),
            "unix_epoch_time": int.from_bytes(header[26:32], "big"),
            "timestamp_hex": get_hex_from_byte(bytes(header)[32:34], 1, 16),
            "timestamp_ns": int.from_bytes(header[34:40], "big"),
            "center_freq": get_hex_from_byte(bytes(header)[40:42], 1, 16),
            "frequency": int.from_bytes(header[44:48], "big"),
            "csp_channel_info": get_hex_from_byte(bytes(header)[48:50], 1, 16),
            "beam_id": int.from_bytes(header[52:54], "big"),
            "frequency_id": int.from_bytes(header[54:56], "big") & 0x3FF,
            "csp_antenna_info": get_hex_from_byte(bytes(header)[56:58], 1, 16),
            "substation_id": int.from_bytes(header[58:59], "big"),
            "subarray_id": int.from_bytes(header[59:60], "big"),
            "station_id": int.from_bytes(header[60:62], "big"),
            "nof_contributing_antenna": int.from_bytes(header[62:64], "big"),
            "sample_offset": get_hex_from_byte(bytes(header)[64:66], 1, 16),
        }

        return spead_lookup[field] if field else spead_lookup

    def inspect(self, packet, protocol, field):
        """
        Print protocol's header information.

        :params packet: Packet object to inspect.
        :params protocol: a string that specifies which protocol's
                    header to inspect.
        :params field: a string that specifies which field to inspect.
        """
        if protocol == "spead":
            header = packet["UDP"].payload
            # print(protocol + '_' + field + ': ' +
            # str(inspect_spead(bytes(header)[0:72], field)))
            return self.inspect_spead(bytes(header)[0:72], field)

        # print(protocol + '_' + field + ': ' +
        # getattr(packet[protocol], field))
        return getattr(packet[protocol], field)

    def pols_pktnum(self, pol_data):
        """
        Plot packet numbers per polarisation.

        :param pol_data: the polarisation data
        :return:
        """
        veritcal_real = pol_data["VR"]
        veritcal_imaginary = pol_data["VI"]
        horizontal_real = pol_data["HR"]
        horizontal_imaginary = pol_data["HI"]
        # plot packets
        figure, axis = plt.subplots(2, 2)

        axis[0, 0].plot(veritcal_real)
        axis[0, 0].set_title("VR")

        axis[0, 1].plot(veritcal_imaginary)
        axis[0, 1].set_title("VI")

        axis[1, 0].plot(horizontal_real)
        axis[1, 0].set_title("HR")

        axis[1, 1].plot(horizontal_imaginary)
        axis[1, 1].set_title("HI")

        figure.tight_layout()

        plt.show()

    # flake8: noqa: C901
    def extract_sps_data(self, pcap: str, substation_mode: bool = False):
        """
        Extract data from SPS packets.

        :param pcap_file: the file containing packet following the pcap format
        :param substation_mode: to pick between station and substation

        """
        self.polarisations = ["VR", "VI", "HR", "HI"]

        self.samples_per_channel = defaultdict(lambda: 0)
        # self.counters = defaultdict(Counter)
        # discover frequency channels present
        self.frequencies = set()
        self.timestamps = set()
        self.timestams_all = []
        self.heaps = set()
        self.heaps_all = []
        self.stations = set()

        self.one_stn_chn_timestamps = []

        for packet in PcapReader(pcap):
            if UDP in packet:
                spead = self.inspect_spead(bytes(packet["UDP"].payload)[:72])
                print(f"spead station id: {spead['station_id']}")
                # rint(f"substation: {spead['substation_id']}")
                if not substation_mode:
                    station = spead["station_id"]
                else:
                    station = spead["substation_id"]
                self.stations.add(station)
                # if substation == SUB_STATION_TO_INSPECT:
                # for field, value in spead.items():
                #    if not (field.startswith("timestamp") or
                #    field == "packet_counter"):
                #        self.counters[field].update(value)
                channel = self.inspect(packet, "spead", "frequency_id")

                self.frequencies.add(
                    (channel, self.inspect(packet, "spead", "frequency"))
                )
                self.timestamps.add((self.inspect(packet, "spead", "timestamp_ns")))
                self.timestams_all.append(self.inspect(packet, "spead", "timestamp_ns"))
                self.heaps.add((self.inspect(packet, "spead", "packet_counter")))
                self.heaps_all.append(self.inspect(packet, "spead", "packet_counter"))
                # don't count spead header, divide by 4 for
                # (vertical, horizontal) * (real, imaginary)
                self.samples_per_channel[
                    MAX_NUMBER_SPS_CHANNELS * (station - 1) + channel
                ] += (len(packet["UDP"].payload) - 72) // len(self.polarisations)

        # print("Counters:")
        # for field, counter in sorted(self.counters.items()):
        #    if len(counter) == 1:
        #        print(f"\t{field}: {next(iter(counter))}")
        #    else:
        #        print(f"\t{field} ({len(counter)}): {counter.most_common()}")
        # print("")

        print("Channels:")
        print("\n".join(f"  {ch} ({fr}Hz)" for ch, fr in sorted(self.frequencies)))

        print("Samples per channel", self.samples_per_channel.items())
        self.channel_lookup = list(self.samples_per_channel.keys())
        self.channels = len(self.samples_per_channel)
        self.n_pol = len(self.polarisations)
        self.time_steps = max(self.samples_per_channel.values())
        self.samples = np.zeros(
            (self.channels, self.n_pol, self.time_steps), dtype=np.int8
        )
        print("samples shape", self.samples.shape)
        self.sorted_timestamp = sorted(self.timestamps)

        # dodgy - assumes no missed packets. should use timestamps instead
        self.samples_collated_per_channel = defaultdict(lambda: 0)
        self.seen = set()
        print("Channel lookup", self.channel_lookup)
        # print(sorted(times))
        for _, packet in enumerate(PcapReader(pcap)):
            spead = self.inspect_spead(bytes(packet["UDP"].payload)[:72])
            # rint(f"substation: {spead['substation_id']}")
            if not substation_mode:
                station = spead["station_id"]
            else:
                station = spead["substation_id"]
            channel = self.inspect(packet, "spead", "frequency_id")
            channel_index = self.channel_lookup.index(
                MAX_NUMBER_SPS_CHANNELS * (station - 1) + channel
            )
            # substation = inspect(packet, "spead", "substation_id")
            # packet_data = np.frombuffer(bytes(packet["UDP"].payload)[72:],
            # dtype=">B")  # 72B of SPEAD header
            # if substation == SUB_STATION_TO_INSPECT:
            packet_data = np.frombuffer(
                bytes(packet["UDP"].payload)[72:], dtype=np.int8
            )  # 72B of SPEAD header
            packet_samples = packet_data.reshape(-1, len(self.polarisations)).T
            if channel not in self.seen:
                self.seen.add(channel)

            start_sample = self.samples_collated_per_channel[
                MAX_NUMBER_SPS_CHANNELS * (station - 1) + channel
            ]
            self.samples[
                channel_index, :, start_sample : start_sample + packet_samples.shape[1]
            ] = packet_samples
            self.samples_collated_per_channel[
                MAX_NUMBER_SPS_CHANNELS * (station - 1) + channel
            ] += packet_samples.shape[1]

    def print_number_of_samples_per_channel(self):
        """
        Print the number of sample per channel.

        """
        plt.clf()
        keys = [
            (
                f"f: {channel % MAX_NUMBER_SPS_CHANNELS} "
                + f"st: {np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)}"
            )
            for channel in self.samples_per_channel.keys()
        ]
        plt.bar(keys, self.samples_per_channel.values())  # , color="blue", width=1e5
        plt.title("Samples per Channel")
        plt.xticks(rotation=45, ha="right", rotation_mode="anchor")
        plt.tight_layout()
        plt.rcParams["figure.figsize"] = (20, 5)
        plt.savefig("SPS_traffic_sample_per_channel.svg", format="svg", dpi=300)

    def print_channel_of_interest(
        self, channel_to_inspect: int, slice_of_samples: tuple
    ):
        """
        Print channel of interest for a slice of samples.

        :param channel_to_inspect: a coarse channel to inspect
        :param slice_of_samples: the slice of sample number

        """

        for n_chan, channel in enumerate(self.channel_lookup):
            if channel % MAX_NUMBER_SPS_CHANNELS == channel_to_inspect:
                plt.clf()
                # n_chan is used to index our all samples data array
                for i, polarisation in enumerate(self.polarisations):
                    plt.plot(
                        range(slice_of_samples[0], slice_of_samples[1]),
                        self.samples[n_chan][i][
                            slice_of_samples[0] : slice_of_samples[1]
                        ],
                        label=polarisation,
                        color=polarisation_colour[i],
                    )
                plt.title(
                    f"Channel {channel % MAX_NUMBER_SPS_CHANNELS} "
                    + f"Substation {np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)}"
                )
                plt.xlabel("Sample Number")
                plt.ylabel("Station Beam Amplitude")
                plt.legend()
                plt.savefig(
                    f"channel_{channel % MAX_NUMBER_SPS_CHANNELS}_"
                    + f"station_{np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)}.svg",
                    format="svg",
                    dpi=300,
                )

    def print_fft_real(self, channels_of_interest: list, station_of_interest: list):
        """
        Print FFT in real domain.

        :param channels_of_interest: The coarse channel on which the FFT is centered.
        :param station_of_interest: The origin station of the data .

        """

        # TODO - check if OS factor should be multiplied instead?

        for n_chan, channel in enumerate(self.channel_lookup):
            # n_chan is used to index our all samples data array
            if (
                int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)) in station_of_interest
                and (channel % MAX_NUMBER_SPS_CHANNELS) in channels_of_interest
            ):
                print("plotting ..")
                plt.clf()
                for n_pol, polarisation in enumerate(self.polarisations):
                    x_fft = fftshift(fft(self.samples[n_chan][n_pol]))
                    nb_samples = len(self.samples[n_chan][n_pol])
                    nb_samples_arranged = np.arange(nb_samples)
                    time_equivalent = nb_samples / SAMPLE_RATE
                    freq = nb_samples_arranged / time_equivalent
                    freq_t = (
                        freq
                        + (int(channel % MAX_NUMBER_SPS_CHANNELS) * 781250)
                        - int(max(freq) / 2)
                        + 1
                    )
                    colour = polarisation_colour[n_pol]
                    plt.stem(
                        freq_t,
                        np.abs(x_fft),
                        colour,
                        markerfmt=" ",
                        basefmt=colour,
                        label=polarisation,
                    )
                    plt.xlabel("Frequency (Hz)")
                    plt.ylabel("Station Beam power (dB)")

                plt.title(f"Channel {channel}")
                plt.legend()
                plt.savefig(
                    f"FFT_real_channel_{channel % MAX_NUMBER_SPS_CHANNELS}_"
                    + f"station_{int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))}.png",
                )

    def print_fft_complex(self, channels_of_interest: list, station_of_interest: list):
        """
        Print FFT in complex domain.

        :param channels_of_interest: The coarse channel on which the FFT is centered.
        :param station_of_interest: The origin station of the data .

        """

        for n_chan, channel in enumerate(self.channel_lookup):
            if (
                int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)) in station_of_interest
                and (channel % MAX_NUMBER_SPS_CHANNELS) in channels_of_interest
            ):
                plt.clf()
                # n_chan is used to index our all samples data array
                # if channel == CHANNEL_WITH_TONE or channel == CHANNEL_WITH_TONE+2:
                horiz = self.samples[n_chan][0] + 1j * self.samples[n_chan][1]
                # nb_samples = len(horiz)
                # window = np.hamming(len(horiz))
                vert = self.samples[n_chan][2] + 1j * self.samples[n_chan][3]
                # before_log_horiz = np.abs(fftshift(fft(horiz * window)))
                # before_log_vert = np.abs(fftshift(fft(vert * window)))
                # if averaging_5p4_kHz:
                #    print("we should implement 5.4kHz averaging")
                #    n = 4800
                #    horiz_avg_bin= [sum(before_log_horiz[i:i+n])//n
                #    for i in range(0,len(before_log_horiz),n)]
                #    before_log_horiz = [horiz_avg_bin[i//n] for
                #    i in range(len(before_log_horiz))]
                #    vert_avg_bin = [sum(before_log_vert[i:i+n])//n
                #    for i in range(0,len(before_log_vert),n)]
                #    before_log_vert = [vert_avg_bin[i//n] for i in
                #    range(len(before_log_vert))]

                horiz_fft = 20 * np.log10(
                    np.abs(fftshift(fft(horiz * np.hamming(len(horiz)))))
                )

                vert_fft = 20 * np.log10(
                    np.abs(fftshift(fft(vert * np.hamming(len(horiz)))))
                )

                nb_samples_arranged = np.arange(len(horiz))
                time_equivalent = len(horiz) / SAMPLE_RATE
                # center_channel = int(channel)*781250
                # freq = (nb_samples_arranged / time_equivalent)
                # print(max(freq))
                # print(min(freq))
                freq_t = (
                    (nb_samples_arranged / time_equivalent)
                    + (int(channel % MAX_NUMBER_SPS_CHANNELS) * 781250)
                    - int(max((nb_samples_arranged / time_equivalent)) / 2)
                    + 1
                )
                # colour = polarisation_colour[n_pol]
                fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, sharey=True)
                # ax1.stem(freq_t, horiz_fft, "blue",
                # markerfmt=" ", basefmt="blue", label="horizontal")

                ax1.stem(
                    freq_t,
                    horiz_fft,
                    "blue",
                    markerfmt=" ",
                    basefmt="blue",
                    label="horizontal",
                )
                ax1.set_xlabel("Frequency (Hz)")
                ax1.set_ylabel("Station Beam power (dB)")
                ax1.axis(ymin=0, ymax=150)
                ax1.grid()
                # ax2.stem(freq_t, vert_fft, "green", markerfmt=" ",
                # basefmt="green", label="vertical")
                ax2.stem(
                    freq_t,
                    vert_fft,
                    "green",
                    markerfmt=" ",
                    basefmt="green",
                    label="vertical",
                )
                ax2.set_xlabel("Frequency (Hz)")
                ax2.set_ylabel("Station Beam power (dB)")
                ax2.axis(ymin=0, ymax=150)
                ax2.grid()
                fig.suptitle(
                    f"Channel {channel % MAX_NUMBER_SPS_CHANNELS} "
                    + f"Station {int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))} "
                )

                fig.legend()
                plt.savefig(
                    f"FFT_channel_{channel % MAX_NUMBER_SPS_CHANNELS}_"
                    + f"station_{int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))}.png",
                )

    def rms_calculation_and_printing(
        self, channels_of_interest: list, station_of_interest: list
    ):
        """
        Calculate and save a figure containing RMS per coarse channel from SPS.

        :param channels_of_interest: The coarse channel on which the RMS is calculated.
        :param station_of_interest: The origin stations of the data.

        """

        rms_calculation = {}
        for station in station_of_interest:
            rms_calculation[station] = {}
            for channel in channels_of_interest:
                rms_calculation[station][channel] = [0, 0]

        for n_chan, channel in enumerate(self.channel_lookup):
            if (
                int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)) in station_of_interest
                and (channel % MAX_NUMBER_SPS_CHANNELS) in channels_of_interest
            ):
                horiz = self.samples[n_chan][0] + 1j * self.samples[n_chan][1]
                vert = self.samples[n_chan][2] + 1j * self.samples[n_chan][3]
                rms_calculation[int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))][
                    channel % MAX_NUMBER_SPS_CHANNELS
                ][0] = np.sqrt(np.sum(np.abs(horiz) ** 2) / len(horiz))
                rms_calculation[int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))][
                    channel % MAX_NUMBER_SPS_CHANNELS
                ][1] = np.sqrt(np.sum(np.abs(vert) ** 2) / len(vert))

        plot_rms_x = {}
        plot_rms_y = {}
        plt.clf()

        for idx, station in enumerate(station_of_interest):
            plot_rms_x[station] = []
            plot_rms_y[station] = []
            for channel in channels_of_interest:
                plot_rms_x[station].append(rms_calculation[station][channel][0])
                plot_rms_y[station].append(rms_calculation[station][channel][1])

            plt.plot(
                channels_of_interest,
                plot_rms_x[station],
                label=f"st {station} X polarisation",
                color=COLOURS[idx % len(COLOURS)],
            )
            plt.plot(
                channels_of_interest,
                plot_rms_y[station],
                label=f"st {station} Y polarisation",
                color=COLOURS[idx % len(COLOURS)],
            )
        plt.title("RMS per station")
        plt.ylabel("RMS ")
        plt.xlabel("Station Channel")
        plt.legend()
        plt.savefig(
            "rms_db.svg",
            format="svg",
            dpi=300,
        )
        return rms_calculation

    def histogram_channel_station(
        self, channels_of_interest: list, station_of_interest: list
    ):
        """
        Print histogram of values observed from SPS data per station and channel.

        :param channels_of_interest: The coarse channel on which the histogram
            is calculated.
        :param station_of_interest: The origin stations of the data.

        """
        # pylint: disable=W0641
        # disabled because ax0, ax1, ax2, ax3 are marked as unused
        # but in fact they are used in a too clever way maybe
        # will need to investigate
        for n_chan, channel in enumerate(self.channel_lookup):
            # n_chan is used to index our all samples data array
            # one subplot per polarisation
            if (
                int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS)) in station_of_interest
                and (channel % MAX_NUMBER_SPS_CHANNELS) in channels_of_interest
            ):
                plt.clf()
                fig, ((ax0, ax1), (ax2, ax3)) = plt.subplots(
                    2, 2, sharex=True, sharey=True
                )
                for i, polarisation in enumerate(self.polarisations):
                    locals()[f"ax{i}"].hist(
                        self.samples[n_chan][i],
                        bins=range(
                            min(self.samples[n_chan][i]),
                            max(self.samples[n_chan][i]) + 1,
                            1,
                        ),
                        label=polarisation,
                        color=polarisation_colour[i],
                    )
                ax0.set_ylabel("Histogram count")
                ax2.set_ylabel("Histogram count")
                ax2.set_xlabel("Station Data")
                ax3.set_xlabel("Station Data")
                fig.suptitle(
                    f"Channel {channel % MAX_NUMBER_SPS_CHANNELS} "
                    + f"Station {int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))}"
                )
                fig.legend()
                plt.savefig(
                    f"histo_{channel % MAX_NUMBER_SPS_CHANNELS}_"
                    + f"station_{int(np.ceil(channel / MAX_NUMBER_SPS_CHANNELS))}.svg",
                    format="svg",
                    dpi=300,
                )
