# -*- coding: utf-8 -*-
"""
@author: Lahiru.Raffel

Test instruments need to be pre-configured to run this script correctly
"""

import pyvisa as visa
import numpy as np
import time
import socket
import csv

AWG_local_port = 5025
AWG_device_ip = "10.135.151.42"

RSA_local_port = 5025
RSA_device_ip = "10.135.151.41"


#frequency_list = [105e6,115e6]
frequency_list = [85e6,95e6,105e6,115e6,190e6,200e6,210e6]
#AWG_power_list = [0.2,0.178,0.158,0.141,0.126,0.112,0.1,0.089,0.079,0.071]
AWG_power_list = [-2,-3,-4,-5,-6,-7,-8,-9,-10,-11]

channel = 6
sample_rate = 1995000000
amplitude = 0.2
record_length = 199500
new_wave_data_array = []
wave_name = '95_105_2tone'

## Define path, open .csv file and define columns
PATH = 'C:\\Users\\Lahiru.Raffel\\Dropbox (SKAO)\\system ITF\\Requirements and Test Cases sITF\\LOW\\SUT\\VELITF24_RF_Receiver\\TCLITF244_Rec_Dy_Rg\\LNA0082SB15x3dBFEM_PreADU16_20dB.csv'
caldata = open(PATH,'w')
caldata.write(str(time.time()))
caldata.write('\n')
caldata.write('AWG amplitude,85 MHz,95 MHz,105 MHz,115 MHz,190 MHz,200 MHz,210 MHz\n')

# Change this to connect to your AWG and RSA as needed
"""#################SEARCH/CONNECT#################"""
rm = visa.ResourceManager()
print(rm.list_resources())
awg = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (AWG_device_ip, AWG_local_port))
rsa = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (RSA_device_ip, RSA_local_port))

awg.timeout = 25000
awg.encoding = 'latin_1'
awg.write_termination = '\n'
awg.read_termination = '\n'

rsa.timeout = 25000
rsa.encoding = 'latin_1'
rsa.write_termination = '\n'
rsa.read_termination = '\n'


print(awg.query('*idn?'))
##awg.write('*rst')
##awg.write('*cls')

print(rsa.query('*idn?'))


## Script overwrites any exisitng waveform with the same name.
awg.write('wlist:waveform:del \"%s\"' % wave_name)
awg.write('wlist:waveform:new "%s", %i' % (wave_name, record_length))
    
for t in range(record_length):
    new_wave_data_array.append(0.2 * np.sin(2 * np.pi * 95000000 * (t / sample_rate))+0.2 * np.sin(2 * np.pi * 105000000 * (t / sample_rate)))
    
new_wave_data = np.asarray(new_wave_data_array[0:-1])
print(len(new_wave_data))

## Load the data array into the AWG.
load_command = 'wlist:waveform:data "%s", 0, %i, ' % (wave_name, record_length)
awg.write_binary_values(load_command, new_wave_data)
awg.query('*opc?')


## Set up the AWG.
awg.write('clock:srate %i' % sample_rate)
    

## Turn on and output from Channel 6.
awg.write('source6:casset:waveform "%s"' % wave_name)
awg.write('output6:state on')

awg.write('awgcontrol:run:immediate')
awg.query('*opc?')

    
time.sleep(10)
    

for ap in AWG_power_list:

    awg.write('source6:power:level:immediate:amplitude %f' % ap)
    print(awg.query('system:error:all?'))
    caldata.write('%f,' % (ap))
    print('%f,' % (ap))        
    time.sleep(2)
    

    for f in frequency_list:
        rsa.write('spec:freq:cent %i' % f)
        print('%i,' % (f))
    
    
       
        #awg.write('awgcontrol:run:immediate')
        time.sleep(1)
        rsa.write('spec:cle:res')
        time.sleep(4)
        rsa.write('calc:spec:mark:max')
        time.sleep(2)
        meas = rsa.query('calc:spec:mark:y?')
        caldata.write('%s,' % (meas))
        print('%s,' % (meas))
        time.sleep(1)
    #awg.write('output1:state off')
    
    caldata.write('\n')
    print('\n')        
            
awg.write('output6:state off')
awg.write('awgcontrol:stop:immediate')
awg.query('*opc?')
caldata.write(str(time.time()))
caldata.write('\n')
caldata.close()
awg.close()
rsa.close()

    
