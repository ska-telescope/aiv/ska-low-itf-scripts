import pandas as pd

obstate_map = {
    0: "EMPTY",
    1: "RESOURCING",
    2: "IDLE",
    3: "CONFIGURING",
    4: "READY",
    5: "SCANNING",
    6: "ABORTING",
    7: "ABORTED",
    8: "RESETTING",
    9: "FAULT",
    }


class TestFrame:
    """
    Show sets of attributes for multiple devices in a dataframe for ease
    of viewing.
    """
    def __init__(self, devices, attributes):
        self.devices = devices
        self.attributes = attributes
        
    def check(self) -> pd.DataFrame:
        data = self.get_attribute_data()
        df = pd.DataFrame(data)
        return df
    
    def get_attribute_data(self):
        d = {}
        d["device"] = [device.name() for device in self.devices]
        other_atts = {attr: [self.get_attribute(device, attr) for device in self.devices] for attr in self.attributes}
        d.update(other_atts)
        return d
    
    def get_attribute(self, device, attr):
        try:
            if hasattr(device, attr):
                value = getattr(device, attr)
                if callable(value):
                    value = value()
                    
                    return value
                else:
                    if attr.lower() == "obsstate":
                        return obstate_map[int(value)]
                    return value
            else:
                return ""
        except Exception as ex:
            return str(ex)
        
        
        
        
    