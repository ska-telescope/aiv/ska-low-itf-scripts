---
attachments: [ Clipboard_2023-08-08-10-58-57.png, Clipboard_2023-08-08-11-00-41.png, Clipboard_2023-08-14-16-19-35.png ]
favorited: true
tags: [ ITF, jupyter, SKAO ]
title: Running jupyter notebooks in the Low ITF
created: '2023-08-08T00:30:23.508Z'
modified: '2023-08-14T08:00:08.832Z'
---

# Running jupyter notebooks in the Low ITF

## Pre-conditions

To work on the Low ITF compute infrastructure you need to have

* an Azure AD account, which comes with your Atlassian tool access (JIRA/Confluence)
* a SKAO VPN account with access to the AU-EOC via e.g. `anyconnect`

## Get set up

* Go to the [Low ITF jupyterhub home page](https://k8s.lowitf.internal.skao.int/binderhub/jupyterhub/hub/home)
* authenticate using your Azure AD account credentials

## To use

* Click `Start My Server`

![](attachments/Clipboard_2023-08-08-10-58-57.png)

* Select `ITF test development`
  ![](attachments/Clipboard_2023-08-08-11-00-41.png)

* Patience while it loads and gets initialised.

![](attachments/Clipboard_2023-08-14-16-19-35.png)

The Low ITF provides you with an image of the notebook containing all the major tools you need such as

* ITango for Tango Controls access to the environment
* a persistent home directory
* a shared folder available to all users, where you can find e.g. `examples`
* a clone of this repository permanently commit scripts and official tests
* a python `support` module for testing

The best way to get started is to navigate to the folder `shared-data/examples` and double-click on one of the notebooks
there. If asked to choose a `kernel` select `ITango`. Now the notebook should be ready to use to interact with the
environments. We provide access to the

* test-environment - Tango devices to support testing
* sut - the Tango devices for the system under test (SUT)

The default Tango environment (that is, the default value of the TANGO_HOST environment variable) is test-environment.
This means that if you create a DeviceProxy with `Device("<tango/device/name>")`, you will by default only have access
to test instruments, not SUT devices.

To change databases, use the ITango magic command `switchdb`:

```jupyterpython
switchdb tango-databaseds.sut 10000
```

## To develop

tba
