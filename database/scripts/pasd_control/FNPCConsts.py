FNPC_CONSTS_VER = "1.4"

# Default timing if you call toggling/walking functions here.
PDOC_TOGGLE_DELAY = 2
WALKING_PDOCS_DELAY = 1

# FNPC Modbus Address in decimal
FNPC_ADDR = 101

FNPC_NUM_PDOCS = 28
FNPC_NUM_THERMISTORS = 4

# FNPC Specific Polling registers
FNPC_SYS_48V1_V = 17 # Voltage of (only) COSEL PSU output
FNPC_SYS_48V2_V = 18 # Voltage of non-existtent COSEL 2, floating in hardware!
FNPC_SYS_48V_I = 19 # Current consumption 48V rail
FNPC_SYS_48V1_TEMP = 20 # First thermistor in COSEL section
FNPC_SYS_48V2_TEMP = 21 # Second thermistor in COSEL section
FNPC_SYS_PANELTEMP = 22 # Not in hardware
FNPC_SYS_FNCBTEMP = 23 # SHT-31 temperature reading (on FNCB)
FNPC_SYS_HUMIDITY = 24 # SHT-31 humidity reading (on FNCB)
FNPC_SYS_STATUS = 25
FNPC_SYS_LIGHTS = 26 # Blue and multicolour light settings
FNPC_SYS_SENSE01 = 27 # Thermistor on top of comms gateway
FNPC_SYS_SENSE02 = 28 # Thermistor on top of Power module enclosure
FNPC_SYS_SENSE03 = 29 # Thermistor on floor of FNDH EP
FNPC_SYS_SENSE04 = 30 # Thermistor dangling in mid-air inside FNDH EP
FNPC_SYS_SENSE05 = 31 # 5 Unimplemented "extra" temperature sensors
FNPC_P01_STATE = 36 # Beginning of 28 PDoC Port state/control registers

# FNPC Config registers, four-word thresholds 
FNPC_SYS_48V1_V_TH = 1001 # Thresholds for the only Cosel (Volts x 100)
FNPC_SYS_48V2_V_TH = 1005 # should be set to zero to disable
FNPC_SYS_48V_I_TH = 1009 # Current limits (Amps x 100)
FNPC_SYS_48V1_TEMP_TH = 1013 # Limits for the Cosel output (DegC x 100)
FNPC_SYS_48V2_TEMP_TH = 1017 # Limits for the Cosel output (DegC x 100)
FNPC_SYS_PANELTEMP_TH = 1021 # Should be all zeroes coz this doesn't exist
FNPC_SYS_FNCBTEMP_TH = 1025 # Limits for the SHT-31 (DegC x 100)
FNPC_SYS_HUMIDITY_TH = 1029 # Humidity limits (no multiplier)
FNPC_SYS_SENSE01_TH = 1033 # Comms GW limits
FNPC_SYS_SENSE02_TH = 1037 # Power module top limits
FNPC_SYS_SENSE03_TH = 1041 # FNDH Floor limits
FNPC_SYS_SENSE04_TH = 1045 # Mid-air limits.
FNPC_SYS_SENSE05_TH = 1049 # Unused sensor thresholds, all should be zero to disable

FNPC_NUM_CONFIG_REGSETS = 17
FNPC_NUM_CONFIG_REGS = FNPC_NUM_CONFIG_REGSETS * 4

# applicable to both parameters and thresholds
FNPC_NUM_EMPTIES = 5 # First four are thermistors T1-T4 other five empty

# Parameter limits for FNPC - these should be in the firmware now
FNPC_48V_LIMITS = [52,50,45,40] # Volts: 
FNPC_CURR_LIMITS = [18, 16, -1, -5] # Amps, unfortunately have to ignore the entire field now because it can report negative numbers.
FNPC_COSEL_TEMP_LIMITS = [100, 85, 0, -5] # Degrees C
FNPC_FNCBTEMP_LIMITS = [85, 70, 0, -5] # Degrees C
FNPC_SENSE_LIMITS = FNPC_FNCBTEMP_LIMITS # Other thermistor harness 
FNPC_SENSE_02_LIMITS = FNPC_COSEL_TEMP_LIMITS # Degrees C
FNPC_HUMIDITY_LIMITS = [85, 75, 10, 0] # Percent RH

FNPC_PDOC_OFF = 0x2800 # Turn PDoCs off for both Online and Offline modes
FNPC_PDOC_ON = 0x3C00 # Turn PDoCs ON for both Online and Offline modes
FNPC_PDOC_ONOFF = 0x3800 # Turn PDoCs On for Online mode, and OFF for Offline mode
FNPC_PDOC_OFFON = 0x2C00 # Turn PDoCs OFF for Online mode, and ON for Offline modes!
FNPC_PDOC_POWERDOWN = 0x0200 # Set Tech override bits to b10, to power-down PDOC port

FNPC_SYS_STATE_OK = 0
FNPC_SYS_STATE_WARNING = 1
FNPC_SYS_STATE_ALARM = 2
FNPC_SYS_STATE_RECOVERY = 3
FNPC_SYS_STATE_UNINITIALISED = 4
FNPC_SYS_STATE_POWERUP = 5

# Long status strings for mainline output, and short strings for in-line output
FNPC_STATE_STRINGS = ["STATE_OK", "STATE_WARNING",  "STATE_ALARM", "STATE_RECOVERY", "STATE_UNINIT", "STATE_POWERUP"]
FNPC_CONV_STATUS = ['OK', 'Warn', 'ALARM!', 'Recovery', 'UnInit', 'PWRup']

# Alarm & warning flag bits
FNPC_AW_MAXFLAGS = 12
FNPC_AW_48V1 = 0x0001
FNPC_AW_48V2 = 0x0002
FNPC_AW_48VI = 0x0004
FNPC_AW_48T1 = 0x0008
FNPC_AW_48T2 = 0x0010
FNPC_AW_PANT = 0x0020
FNPC_AW_FNCT = 0x0040
FNPC_AW_HUMD = 0x0080
FNPC_AW_SN01 = 0x0100
FNPC_AW_SN02 = 0x0200
FNPC_AW_SN03 = 0x0400
FNPC_AW_SN04 = 0x0800

# Alarm & warning flag strings
FNPC_AW_STRINGS = ['48V1', '48V2', '48VI', '48T1', '48T2', 'PANT', 'FNCT', 'HUMD', 'SN01', 'SN02', 'SN03', 'SN04']
                   

