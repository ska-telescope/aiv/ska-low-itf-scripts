from ModBusSupport import *
from FNPCConsts import *
import time

FNPC_FUNCS_VER = "1.4"

def DoWalkingPDoCS(conn, ToggleDelay=PDOC_TOGGLE_DELAY, WalkStepDelay=WALKING_PDOCS_DELAY):

    """ Send messages to control PDOC states
    """

    PDOCOffMessage = ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFF, FNPC_NUM_PDOCS)
    
    print("All PDoCs off")
    SendMessage(conn,PDOCOffMessage)
    time.sleep(0.1)
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
    time.sleep(ToggleDelay) # wait two seconds
    
    print("All PDoCs on")
    SendMessage(conn, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_ON, FNPC_NUM_PDOCS)) # turn on all PDoCS
    time.sleep(0.1)
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
    time.sleep(ToggleDelay)
     
    print("Walking zeroes")
    # turn them all off, one at a time
    for i in (range(FNPC_NUM_PDOCS)): # runs i from 0..27
        SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + i, FNPC_PDOC_OFF)) # turn off the given PDOC
        SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
        time.sleep(WalkStepDelay)

    print("Walking ones")
    # turn them all on, one at a time
    for i in (range(FNPC_NUM_PDOCS)): # runs i from 0..27, so just add to P01_STATE
        SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + i, FNPC_PDOC_ON)) # turn on the given PDOC
        time.sleep(0.1)
        SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
        time.sleep(WalkStepDelay)

def GetFNPCThresholds(conn):

    print("\nFNPC Show live 48V reading")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_V), CONV_TYPE_VOLTAGE)
    print("FNPC Get 48V limits, V1 nominal, V2 all zeroes => disable")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V1_V_TH, 2), CONV_TYPE_VOLTAGE) # Read them back.

    print("\nFNPC Show live cosel current")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V_I), CONV_TYPE_CURRENT)
    print("FNPC Get Current limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V_I_TH), CONV_TYPE_CURRENT) # Read them back.

    print("\nFNPC Show live Power Module Thermistors")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_TEMP, 2), CONV_TYPE_TEMP)
    print("FNPC Get Power Module thermistor limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V1_TEMP_TH, 2), CONV_TYPE_TEMP)

    print("\nFNPC ignore non-existent Panel Temp")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_PANELTEMP_TH))

    print("\nFNPC Show live FNCB/SHT31 temperature value")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_FNCBTEMP), CONV_TYPE_TEMP)
    print("FNPC Get FNCB/SHT31 temperature limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_FNCBTEMP_TH), CONV_TYPE_TEMP) # read them back.
    
    print("\nFNPC Show live FNCB/SHT31 humimdity value")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_HUMIDITY), CONV_TYPE_HUMIDITY)
    print("FNPC Get FNCB SHT31 humidity limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_HUMIDITY_TH), CONV_TYPE_HUMIDITY) # read them back.

    print("\nFNPC show live CommsGW temperature")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE01), CONV_TYPE_TEMP)
    print("FNPC Get CommsGW temperature limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE01_TH), CONV_TYPE_TEMP) # read them back.

    print("\nFNPC show live PowerModule top sensor temperature")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE02), CONV_TYPE_TEMP)
    print("FNPC Get Power Module outside-top sensor limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE02_TH), CONV_TYPE_TEMP) # read them back.

    print("\nFNPC show live FNDH EP bottom and FNDH EP ambient")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE03, 2), CONV_TYPE_TEMP)
    print("FNPC Get EP bottom and EP ambient temperature limits")
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE03_TH, 2), CONV_TYPE_TEMP) # read them back.

    print("\nFNPC Get %d SENSE value limits from %d (ignored)" % (FNPC_NUM_EMPTIES, FNPC_SYS_SENSE04_TH))
    SendMessage(conn, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE05_TH, FNPC_NUM_EMPTIES))

def DoAlternatePDoCSonoff(conn):

    """ Send messages to control PDOC states
    """

    print("Odd PDoCs ON, Even PDoCs Off")
    SendMessage(conn, ModBusWriteMultiPortStateLists(FNPC_ADDR, FNPC_P01_STATE, [FNPC_PDOC_ON, FNPC_PDOC_OFF], FNPC_NUM_PDOCS >> 1))
    time.sleep(0.5)
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS))
    
    
def FNPCGetLiveValues(conn):
    print("\nInitial FNPC Read Status register")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nFNPC Read initial DoC states: 1-10, 11-20, 21-28")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, 10))
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE + 10, 10))
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE + 20, FNPC_NUM_PDOCS-20))

    print("\nFNPC Show live 48V reading")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_V), CONV_TYPE_VOLTAGE)

    print("\nFNPC Show live cosel current")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V_I), CONV_TYPE_CURRENT)

    print("\nFNPC Show live Power Module Thermistors")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_TEMP, 2), CONV_TYPE_TEMP)

    print("\nFNPC Show live FNCB temperature")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_FNCBTEMP), CONV_TYPE_TEMP)
    print("\nFNPC Show live humidity")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_HUMIDITY), CONV_TYPE_HUMIDITY)

    print("\nFNPC show live CommsGW temperature")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE01), CONV_TYPE_TEMP)

    print("\nFNPC show live PowerModule top sensor temperature")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE02), CONV_TYPE_TEMP)

    print("\nFNPC show live FNDH EP bottom and FNDH EP ambient")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE03, 2), CONV_TYPE_TEMP)

def FNPCClearALWarnBits(conn):
    print("\nFNPC Try to clear Alarms/Warnings")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, ALARMS_LSW, 0))
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, WARNINGS_LSW, 0))

def FNPCGetALWarnBits(conn):
    print("\nFNPC Get Warnings LSW")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, WARNINGS_LSW), CONV_TYPE_ALWARN)
    print("FNPC Get Alarms LSW")
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, ALARMS_LSW), CONV_TYPE_ALWARN)

def TurnOnAllPDoCS4x7(conn):

    """ Send messages to control PDOC states, turn them on sequentially
    """
  
    print("\nFNPC PDoCS on in sets of seven")
    SendMessage(conn, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_ON, 7))
    time.sleep(0.1)
    SendMessage(conn, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE + 7, FNPC_PDOC_ON, 7))
    time.sleep(0.1)
    SendMessage(conn, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE + 14, FNPC_PDOC_ON, 7))
    time.sleep(0.1)
    SendMessage(conn, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE + 21, FNPC_PDOC_ON, 7))
    time.sleep(0.1)
    
    

def MonitorPMTemps(conn):
    """
    Monitor 2 x COSEL temps, PM Top and FNDH EP Bottom temps
    """
    while True:
        print("Monitor 2 x COSEL temps, PM Top and FNDH EP Bottom temps")
        SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_TEMP, 2), CONV_TYPE_TEMP) # final arg says "treat quads as temperatures"
        SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE02, 2), CONV_TYPE_TEMP) # ditto
        time.sleep(2)    

def ServiceOnFNDH(conn):
    print("\nFNPC Turn on service LED")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_ON))
    

def ServiceOffFNDH(conn):
    print("\nFNPC turn off service LED")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_OFF))
    
def FNPCSysLights(conn):
    print("\nFNPC Show current LED state")
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))

    print("FNPC Service LED on")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_ON))
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))
    time.sleep(3)

    print("FNPC Service LED 5Hz flash")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_5HZ))
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))
    time.sleep(3)

    print("FNPC Service LED 2.5Hz flash")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_2HZ5))
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))
    time.sleep(3)

    print("FNPC Service LED 1.25Hz flash")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_1HZ25))
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))
    time.sleep(3)

    print("FNPC Service LED 0.625Hz flash")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_0HZ625))
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))
    time.sleep(3)

    print("FNPC Service LED off")
    SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_OFF))
    SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))

    
