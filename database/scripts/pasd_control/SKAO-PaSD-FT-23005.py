import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.3"

"""
(Firmware) Functional Test FT-23005
MCCS Read FNPC initial state after power applied
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNPC Read STATUS Register")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nFNPC Read PORT STATE Registers")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS))

    print("\nFNPC Read Configuration Registers")
    print("FNPC Read 48V voltage limits")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V1_V_TH), CONV_TYPE_VOLTAGE)

    print("\nFNPC Read 48V current limits")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V_I_TH), CONV_TYPE_CURRENT)

    print("\nFNPC Read Temperature limits at top and bottom of COSEL SMPS baseplate")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V1_TEMP_TH, 2), CONV_TYPE_TEMP)

    print("\nFNPC Read FNCB Temperature limits")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_FNCBTEMP_TH), CONV_TYPE_TEMP)

    print("\nFNPC Read Humidity limits")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_HUMIDITY_TH), CONV_TYPE_HUMIDITY)

    print("\nFNPC Read FNDP EP Temperature limits - external surface of CG")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE01_TH), CONV_TYPE_TEMP)

    print("\nFNPC Read FNDP EP Temperature limits - external surface of PM")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE02_TH), CONV_TYPE_TEMP)

    print("\nFNPC Read FNDP EP Temperature limits - \"floor\" of enclosure")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE03_TH), CONV_TYPE_TEMP)
    
    print("\nFNPC Read FNDP EP Temperature limits - \"roof\" of enclosure")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE04_TH), CONV_TYPE_TEMP)

    print("\nFNPC Read disabled threshold Configuration Registers")
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V2_V_TH))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_PANELTEMP_TH))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE05_TH, 5))
    
    
    

    target.close()
    
if __name__ == "__main__":
    main()
