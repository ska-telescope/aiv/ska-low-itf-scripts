"""
    Turns all FNDH PDoCs on (seven at a time)
     then does the quick checks on SB01 (or am alternate Smartbox provided on the command line)

"""

VERSION = "1.3"

import sys
import time
import argparse
import serial
from FNPCFuncs import *
from FNSCFuncs import *
from OpenComms import *

# All in seconds.
# These override defaults in DoWalkingFEMs()
FEM_TOGGLE_DELAY = 0.25
WALKING_FEMS_DELAY = 0.1

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    ServiceOnFNDH(target)

    print("\nFNPC Request STATUS_OK")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    TurnOnAllPDoCS4x7(target) # Turn on PDoCs

    time.sleep(1) # Give any potential smartbox time to boot up

    StartSmartBox(target, SBAddress)

    DoWalkingFEMs(target, SBAddress, FEM_TOGGLE_DELAY, WALKING_FEMS_DELAY)

    ServiceOffSBox(target, SBAddress)

    print("\nFNPC All PDoCS off")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFF, FNPC_NUM_PDOCS))

    ServiceOffFNDH(target) # Turn them all off.

    target.close()
    
if __name__ == "__main__":
    main()
