import sys
import time
import argparse
import serial
from FNPCFuncs import *
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.3"

log_folder = ".\\pasd_log_files"

time_zone_offset = 8 * 60 * 60
initial_time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))

def write_to_log(device_name, log_text, print_log=True):
    
    log_file_name = initial_time_stamp + "_" + device_name + ".txt"
    
    if os.path.isdir(log_folder) != True:
        os.makedirs(log_folder)
    
    with open(os.path.join(log_folder, log_file_name), "a") as file:
        time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))
        
        file.write(time_stamp + "," + log_text + "\n")
    
    if print_log == True:
        print(time_stamp + "," + log_text + "\n")
        
def check_pdocs():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    write_to_log("FNDH set all PDoCs to OFF")
    response = SendMessage(conn,ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFF, FNPC_NUM_PDOCS))
    time.sleep(1)
    response = SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
    write_to_log("fndh", response)
    
    for i in range(0, 28):
        write_to_log("Turning on PDoC: " + str(i))
        response = SendMessage(conn, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + i, FNPC_PDOC_ON)) # turn on the given PDOC
        time.sleep(1)
        response = SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
        write_to_log("fndh", response)
        input("Press enter to continue.)
        
    write_to_log("FNDH set all PDoCs to OFF")
    response = SendMessage(conn,ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFF, FNPC_NUM_PDOCS))
    time.sleep(1)
    response = SendMessage(conn,ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS)) # read back PDOC register states
    write_to_log("fndh", response)

    target.close()

def main():
    (target, SBAddress, PDoCPort, FEMPorts) = OpenComms()

    print("\nFNPC Clear STATUS")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    ServiceOnFNDH(target)

    DoWalkingPDoCS(target)

    time.sleep(1) # Give any potential smartbox time to boot up

    StartSmartBox(target, 1)

    DoWalkingFEMs(target, 1)

    ServiceOffSBox(target, 1)

    ServiceOffFNDH(target)
    
    target.close()
    
################################################################################
##################################### MAIN #####################################
################################################################################

try:
    (target, SBAddress, PDoCPort, FEMPorts) = OpenComms()
except Exception as error:
    
    sys.exit(1)