import sys
import time
import argparse
import serial
from FNPCFuncs import *
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.3"

def main():
    (target, SBAddress, PDoCPort, FEMPorts) = OpenComms()

    print("\nFNPC Clear STATUS")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    ServiceOnFNDH(target)

    DoWalkingPDoCS(target)

    time.sleep(1) # Give any potential smartbox time to boot up

    StartSmartBox(target, 1)

    DoWalkingFEMs(target, 1)

    ServiceOffSBox(target, 1)

    ServiceOffFNDH(target)
    
    target.close()
    
if __name__ == "__main__":
    main()
