import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNSC(%02d) Set Temperature limits for SYS_SENSE01 as %s" % (SBAddress, FNPC_COSEL_TEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(SBAddress, FNSC_SYS_SENSE01_TH, FNPC_COSEL_TEMP_LIMITS, TEMP_MULTIPLIER))

    target.close()
    
if __name__ == "__main__":
    main()
