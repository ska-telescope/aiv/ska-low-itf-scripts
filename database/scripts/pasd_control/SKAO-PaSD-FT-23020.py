import sys
import time
import argparse
import serial
from FNSCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.2"

"""
(Firmware) Functional Test FT-23020
MCCS Read SMART Box FEM ports to confirm functionality of Tech button override and
interaction with MCCS write STATE_OK (should only clear TO bits 0b01 to 0b00)
User must operate tech interface button for desired intervals.
"""

POLLING_DELAY_SECONDS = 1
STATE_OK_DELAY = 0.5
SECONDS_PER_TEST = 15
TIMES_TO_LOOP = int(SECONDS_PER_TEST / POLLING_DELAY_SECONDS)

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    while True:
        print("FNSC(%02d) Monitor a few Pxx_STATE for FEM powered on (0x--40) and Tech override bits (0x-3--)" % (SBAddress))
        print("FNSC(%02d) Monitor SYS_STATUS for SYS_POWERDOWN" % (SBAddress))
        for i in range(TIMES_TO_LOOP):
            SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, 4), CONV_TYPE_TOBITS)
            rV = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
            # print('rV=' + str(rV))
            if (rV[0] == 1) and (rV[1] == FNSC_SYS_STATE_POWERDOWN):
                print("Powerdown! Turn off PDoC%02d via TO bits" % (PDoCPort))
                SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort - 1), FNPC_PDOC_POWERDOWN))
                return
            time.sleep(POLLING_DELAY_SECONDS)
        print("FNSC(%02d) Attempt to set STATE_OK - should only restore TO bits 0b01 to 0b00" % (SBAddress))
        SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_ON))
        SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
        time.sleep(STATE_OK_DELAY)
        SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_OFF))

    target.close()

if __name__ == "__main__":
    main()
