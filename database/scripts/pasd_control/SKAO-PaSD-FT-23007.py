import sys
import time
import argparse
import serial
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.2"

"""
(Firmware) Functional Test FT-23007
MCCS Read SMART Box initial state after power applied
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNSC(%02d) Read SYS_STATUS Register" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nFNSC(%02d) Read FEM Port STATE Registers" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))

    print("\nFNSC(%02d) Read Configuration Registers" % (SBAddress))
    print("FNSC(%02d) Read 48V voltage limits" % (SBAddress))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_48V_V_TH), CONV_TYPE_VOLTAGE)

    print("\nFNSC(%02d) Read PSU output voltage limits" % (SBAddress))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_PSU_V_TH), CONV_TYPE_VOLTAGE)

    print("\nFNSC(%02d) Read PSU Temperature limits" % (SBAddress))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_PSUTEMP_TH), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Read FEMP Ambient Temperature limits" % (SBAddress))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_AMBTEMP_TH), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Read FEM / Heatsink Temperature limits" % (SBAddress))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_SENSE01_TH, 4), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Read disabled threshold Configuration Registers" % (SBAddress))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_PCB_TEMP_TH))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_EMPTY_TH, 8))
    
    print("\nFNSC(%02d) Read FEM current limits" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_CURRENT_THRESHOLDS), CONV_TYPE_FEM_MA)

    target.close()
    
if __name__ == "__main__":
    main()
