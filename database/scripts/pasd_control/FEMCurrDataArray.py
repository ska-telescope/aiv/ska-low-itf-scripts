import sys
import time
import argparse
import serial
import numpy as np
import matplotlib.pyplot as plt
from ModBusSupport import *
from OpenComms import *

## Define path, open .csv file and define columns
#PATH = 'C:\\Users\\Lahiru.Raffel\\Dropbox (SKAO)\\system ITF\\PaSD1_SB1_noisyEP_1s_samprate.csv'
#caldata = open(PATH,'w')
#caldata.write(str(time.time()))
#caldata.write('\n')
#caldata.write('AWG amplitude,85 MHz,95 MHz,105 MHz,115 MHz,190 MHz,200 MHz,210 MHz\n')

FEM1array = []
FEM2array = []
FEM3array = []
FEM4array = []
FEM5array = []
FEM6array = []
FEM7array = []
FEM8array = []
FEM9array = []
FEM10array = []
FEM11array = []



VERSION = "1.1"

(target, SBAddress, PDoCPort, FEMPort) = OpenComms()

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
print(type(response))
print(len(response))
print(response[0])
print(response[11])
print(type(response[0]),type(response[11]))
print(response)

maxFEM1 = response[1]
minFEM1 = response[1]
maxFEM2 = response[2]
minFEM2 = response[2]
maxFEM3 = response[3]
minFEM3 = response[3]
maxFEM4 = response[4]
minFEM4 = response[4]
maxFEM5 = response[5]
minFEM5 = response[5]
maxFEM6 = response[6]
minFEM6 = response[6]
maxFEM7 = response[7]
minFEM7 = response[7]
maxFEM8 = response[8]
minFEM8 = response[8]
maxFEM9 = response[9]
minFEM9 = response[9]
maxFEM10 = response[10]
minFEM10 = response[10]
maxFEM11 = response[11]
minFEM11 = response[11]

for i in range (1,1800):
    time.sleep(1)
    response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
    
    FEM1array.append(response[1])
    FEM2array.append(response[2])
    FEM3array.append(response[3])
    FEM4array.append(response[4])
    FEM5array.append(response[5])
    FEM6array.append(response[6])
    FEM7array.append(response[7])
    FEM8array.append(response[8])
    FEM9array.append(response[9])
    FEM10array.append(response[10])
    FEM11array.append(response[11])
    
    if response[1] > maxFEM1:
        maxFEM1 = response[1]
        
    if response[1] < minFEM1:
        minFEM1 = response[1]
    
    if response[2] > maxFEM2:
        maxFEM2 = response[2]
        
    if response[2] < minFEM2:
        minFEM2 = response[2]
    
    if response[3] > maxFEM3:
        maxFEM3 = response[3]
        
    if response[3] < minFEM3:
        minFEM3 = response[3]
    
    if response[4] > maxFEM4:
        maxFEM4 = response[4]
        
    if response[4] < minFEM4:
        minFEM4 = response[4]
        
    if response[5] > maxFEM5:
        maxFEM5 = response[5]
        
    if response[5] < minFEM5:
        minFEM5 = response[5]
        
    if response[6] > maxFEM6:
        maxFEM6 = response[6]
        
    if response[6] < minFEM6:
        minFEM6 = response[6]
    
    if response[7] > maxFEM7:
        maxFEM7 = response[7]
        
    if response[7] < minFEM7:
        minFEM7 = response[7]
    
    if response[8] > maxFEM8:
        maxFEM8 = response[8]
        
    if response[8] < minFEM8:
        minFEM8 = response[8]
    
    if response[9] > maxFEM9:
        maxFEM9 = response[9]
        
    if response[9] < minFEM9:
        minFEM9 = response[9]
    
    if response[10] > maxFEM10:
        maxFEM10 = response[10]
        
    if response[10] < minFEM10:
        minFEM10 = response[10] 
        
    if response[11] > maxFEM11:
        maxFEM11 = response[11]
        
    if response[11] < minFEM11:
        minFEM11 = response[11] 

print('maxFEM1 = ',maxFEM1,'minFEM1 = ',minFEM1)
print('maxFEM2 = ',maxFEM2,'minFEM2 = ',minFEM2)
print('maxFEM3 = ',maxFEM3,'minFEM3 = ',minFEM3)
print('maxFEM4 = ',maxFEM4,'minFEM4 = ',minFEM4)
print('maxFEM5 = ',maxFEM5,'minFEM5 = ',minFEM5)
print('maxFEM6 = ',maxFEM6,'minFEM6 = ',minFEM6)
print('maxFEM7 = ',maxFEM7,'minFEM7 = ',minFEM7)
print('maxFEM8 = ',maxFEM8,'minFEM8 = ',minFEM8)
print('maxFEM9 = ',maxFEM9,'minFEM9 = ',minFEM9)
print('maxFEM10 = ',maxFEM10,'minFEM10 = ',minFEM10)
print('maxFEM11 = ',maxFEM11,'minFEM11 = ',minFEM11)

plt.plot(FEM1array)
plt.plot(FEM2array)
plt.plot(FEM3array)
plt.plot(FEM4array)
plt.plot(FEM5array)
plt.plot(FEM5array)
plt.plot(FEM6array)
plt.plot(FEM7array)
plt.plot(FEM8array)
plt.plot(FEM9array)
plt.plot(FEM10array)
plt.plot(FEM11array)
plt.ylim([200.0,550.0])
plt.show()

target.close()