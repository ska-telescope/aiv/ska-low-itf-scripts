import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.3"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNPC Try to set STATUS_OK")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    target.close()
    
if __name__ == "__main__":
    main()
