import sys
import time
import argparse
import serial
from FNSCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.0"

"""
(Firmware) Functional Test FT-23023
MCCS Read some PDoC ports to confirm detection of PDoC fault conditins where
port register POWER bit does not equal port register PWR_SENSE bit
USER must connect two working PDoCs to ports 1 and 2, and
jumper-dummy the PWR_SENSE pin low on PDoC port 3 (IDC pin 6 to IDC pin 1) and
jumper-dummy the PWR_SETNSE pin high on PDoC port 4 (IDC pin 6 to IDC pin 2)
prior to conducting this test.
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    print("FNPC Turn OFF PDoC 1 and 3, and turn ON PDoC 2 and 4")
    SendMessage(target, ModBusWriteMultiPortStateLists(FNPC_ADDR, FNPC_P01_STATE, [FNPC_PDOC_OFF, FNPC_PDOC_ON], 2))
    print("... wait until PDoCs that are off have discharged")
    time.sleep(1)
    print("FNPC Read first four Pxx_STATEs for bottom byte being 0x--00 (OK/OFF), 0x--C0 (OK/ON), 0x--80 (FAULT/NOT-OFF), 0x--40 (FAULT/NOT-ON)")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, 4))

    target.close()

if __name__ == "__main__":
    main()
