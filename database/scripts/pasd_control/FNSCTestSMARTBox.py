import os
import sys
import time
import argparse
import serial

from ModBusSupport import *
from OpenComms import *

from FNSCConsts import *
from FNSCFuncs import *

VERSION = "1.1"

log_folder = ".\\pasd_log_files"

time_zone_offset = 8 * 60 * 60
initial_time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))

number_of_fems = 12

def write_to_log(device_name, log_text, print_log=True):
    
    log_file_name = initial_time_stamp + "_" + device_name + ".txt"
    
    if os.path.isdir(log_folder) != True:
        os.makedirs(log_folder)
    
    with open(os.path.join(log_folder, log_file_name), "a") as file:
        time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))
        
        file.write(time_stamp + ": " + log_text + "\n")
    
    if print_log == True:
        print(time_stamp + ": " + log_text)
        
################################################################################
##################################### MAIN #####################################
################################################################################

(target, SBAddress, PDoCPort, FEMPort) = OpenComms()

device_name = "SMARTBox%02d" % (SBAddress)

write_to_log(device_name, "#################### TESTING SMARTBOX %s ####################" % (SBAddress))

write_to_log(device_name, "Getting configuration information...")

response = SendMessage(target, ModBusReadRegisters(SBAddress, 1))
write_to_log(device_name, "SYS_MBRV - %d" % (response[1]))

response = SendMessage(target, ModBusReadRegisters(SBAddress, 2))
write_to_log(device_name, "SYS_PCBREV - %d" % (response[1]))

response = SendMessage(target, ModBusReadRegisters(SBAddress, 3, 2))
cpu_id = "0x"
for reg in response[1:]:
    cpu_id += format(reg, '02x')
write_to_log(device_name, "SYS_CPDUID - %s" % (cpu_id))

response = SendMessage(target, ModBusReadRegisters(SBAddress, 5, 8))
chip_id = "0x"
for reg in response[1:]:
    chip_id += format(reg, '02x')
write_to_log(device_name, "SYS_CHIPID - %s" % (chip_id))

response = SendMessage(target, ModBusReadRegisters(SBAddress, 13))
write_to_log(device_name, "SYS_FIRMVER - %s" % (response[1]))

response = SendMessage(target, ModBusReadRegisters(SBAddress, 14, 2))
up_time = response[1] * 256 + response[2]
write_to_log(device_name, "SYS_UPTIME - %d" % up_time)

response = SendMessage(target, ModBusReadRegisters(SBAddress, 16))
write_to_log(device_name, "SYS_ADDRESS - %s" % (response[1]))

write_to_log(device_name, "Initialising SMART Box %02d..." % (SBAddress))
##print("FNSC(%02d) Set STATUS_OK" % (SBAddress))
SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
write_to_log(device_name, "SMART Box %02d initialised." % (SBAddress))

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
write_to_log(device_name, "Current device status - " + str(response[1:]))

## Get the alarm bits.
##response = FNSCGetALWarnBits(target, SBAddress)
##write_to_log(device_name, response)

## Make sure the FEMs are off before starting.
print("FNSC(%02d) Turning off all FEMs" % (SBAddress))
SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_OFF, FNSC_NUM_FEMS))
    
time.sleep(2)

## Read the currents to check they are off.
print("FNSC(%02d) FEM Currents" % (SBAddress))
response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
write_to_log(device_name, "FEM Currents - " + str(response[1:]) + " mA")

input("Are currents close to zero before continuing?")
write_to_log(device_name, "All FEMs turned off.")

## Read all the sensors
write_to_log(device_name, "Reading SMART Box sensors...")

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_48V), CONV_TYPE_VOLTAGE)
write_to_log(device_name, "Voltage Input - %.2f V" % (float(response[1]) / 100))

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_PSU_V), CONV_TYPE_VOLTAGE)
write_to_log(device_name, "Voltage Output - %.2f V" % (float(response[1]) / 100))

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_PSUTEMP), CONV_TYPE_TEMP)
write_to_log(device_name, "PSU Temp - %.2f *C" % (float(response[1]) / 100))

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_AMBTEMP, FNSC_NUM_AMBTEMPS), CONV_TYPE_TEMP)
write_to_log(device_name, "Ambient Temp - %.2f *C ## Mounted on sensor board in FEM package" % (float(response[1]) / 100))

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE01, FNSC_NUM_FEMPTEMPS), CONV_TYPE_TEMP)
write_to_log(device_name, "FEM Temp 1 - %.2f *C ## FEM6 temperature" % float(response[1] / 100))
write_to_log(device_name, "FEM Temp 2 - %.2f *C ## FEM12 (or last FEM) temperature" % float(response[2] / 100))
write_to_log(device_name, "FEM Temp 3 - %.2f *C ## Heatsink 7-12 temperature" % float(response[3] / 100))
write_to_log(device_name, "FEM Temp 4 - %.2f *C ## Heatsink 1-6 temperature" % float(response[4] / 100))

input("Press ENTER to start testing of FEM ports.")
write_to_log(device_name, "Testing FEM ports...")

for fem_port in range(1, (number_of_fems + 1)):

    input("Press ENTER to start testing FEM %i" % (fem_port))
    write_to_log(device_name, "Testing FEM %i" % (fem_port))

    print("FNSC(%02d) Turn on FEM %d" % (SBAddress, fem_port))
    response = SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_P01_STATE + fem_port - 1, FNSC_FEM_ON))
    ##write_to_log(device_name, str(response))

    time.sleep(1)

    print("FNSC(%02d) FEM Currents" % (SBAddress))
    response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
    write_to_log(device_name, "FEM Currents - " + str(response[1:]) + " mA")
    
    input("Is current OK? Expected range is 300 - 400 mA")
    write_to_log(device_name, "FEM %i current is OK" % (fem_port))
    
## Read status.
response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
write_to_log(device_name, "Current device status - " + str(response[1:]))
    
## Test is complete. Turn off the FEMs.
SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_OFF, FNSC_NUM_FEMS))
time.sleep(2)
response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
write_to_log(device_name, "TEST COMPLETE. FEMs turned off.")
write_to_log(device_name, "FEM Currents - " + str(response[1:]) + " mA")

target.close()
