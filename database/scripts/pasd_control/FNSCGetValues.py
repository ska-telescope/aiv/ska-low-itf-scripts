import sys
import time
import argparse
import serial
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    FNSCGetLiveValues(target, SBAddress)

    target.close()
    
if __name__ == "__main__":
    main()
