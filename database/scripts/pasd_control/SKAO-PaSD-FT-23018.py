import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.2"

"""
(Firmware) Functional Test FT-23018
MCCS Clear previous warnings/alarms history, try to start from STATE_OK
MCCS Observe FNPC transition to ALARM and return to RECOVERY on changes to SENSE_01
MCCS continuously monitor alarm / warning flags
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    FNPCClearALWarnBits(target)
    print("\nFNPC Try to set STATUS_OK")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    
    print("\nFNPC Monitor STATUS, WARNINGS and ALARMS flags, and SYS_SENSE01")
    while True:
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, WARNINGS_LSW), CONV_TYPE_ALWARN)
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, ALARMS_LSW), CONV_TYPE_ALWARN)
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE01), CONV_TYPE_TEMP)
       
        print("---")        
        time.sleep(1.0)

    target.close()

if __name__ == "__main__":
    main()
