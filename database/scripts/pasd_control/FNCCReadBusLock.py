import sys
import time
import argparse
import serial
from ModBusSupport import *
from FNCCConsts import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    print("\nFNCC Get Bus Lock control register")
    replyList = SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_BUS_CONTROL))
    if (replyList[0] != 1) or (replyList[1] != FNCC_BUS_FREE and replyList[1] != FNCC_BUS_LOCKED):
        print(" Error reading Bus Lock control register")
        return(-1)
    print(" SMART Box comms Bus is %s" % (FNCC_LOCK_STRING[replyList[1]]))
    

    print("\nFNCC Get Status")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_SYS_STATUS), CONV_TYPE_STATUS)
    

    target.close()
    
if __name__ == "__main__":
    main()
