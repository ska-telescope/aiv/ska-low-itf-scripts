import sys
import time
import argparse
import serial
from OpenComms import *
from ModBusSupport import *
from FNSCFuncs import *

VERSION="1.1"


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    for i in range(24):
        SendMessage(target, ModBusReadRegisters(i, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
        time.sleep(1)

    target.close()
    
if __name__ == "__main__":
    main()
