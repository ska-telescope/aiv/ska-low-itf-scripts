import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Turn on FEMs" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ON, FNSC_NUM_FEMS))
    
    time.sleep(3)

    print("FNSC(%02d) FEM Currents" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)

    target.close()
    
if __name__ == "__main__":
    main()
