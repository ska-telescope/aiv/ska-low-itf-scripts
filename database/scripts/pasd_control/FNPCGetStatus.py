import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
#    ServiceOnFNDH(target)

    print("\nFNPC Read STATUS")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

#    ServiceOffFNDH(target)

    target.close()
    
if __name__ == "__main__":
    main()
