import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

## The threshold current in mA. The default is 495 mA.
current_thresh = 700
number_of_fems = 12

for arg in sys.argv:
    if (arg[0:7] == "--curr=") and (arg[7:].isdigit() == True):
        current_thresh = int(arg[7:])
        sys.argv.remove(arg)

(target, SBAddress, PDoCPort, FEMPort) = OpenComms()

print("\nFNSC(%02d) Setting FEM %d current threshold to %d" % (SBAddress, FEMPort, current_thresh))
SendMessage(target, ModBusWriteRegister(SBAddress, (FNSC_P01_CURRENT_TH - 1) + FEMPort, current_thresh))

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))

target.close()