import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

(target, SBAddress, PDoCPort, FEMPort) = OpenComms()

response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))

target.close()