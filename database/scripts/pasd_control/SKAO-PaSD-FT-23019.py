import sys
import time
import argparse
import serial
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.2"

"""
(Firmware) Functional Test FT-23019
MCCS Observe SMART Box transition to ALARM and return to RECOVERY on changes to SENSE_01
MCCS continuously monitor alarm / warning flags
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    FNSCClearALWarnBits(target, SBAddress)
    print("\nFNSC(%02d) Try to set STATE to OK" % (SBAddress))
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
    
    print("\nFNSC(%02d) Monitor STATUS, WARNINGS and ALARMS flags" % (SBAddress))
    while True:
        SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
        SendMessage(target, ModBusReadRegisters(SBAddress, WARNINGS_LSW), CONV_TYPE_ALWARN)
        SendMessage(target, ModBusReadRegisters(SBAddress, ALARMS_LSW), CONV_TYPE_ALWARN)
        SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE01), CONV_TYPE_TEMP)
        print("---")
        time.sleep(1.0)

    target.close()

if __name__ == "__main__":
    main()
