import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23010 part 2
Set PDoCs with DSON to OFF, and DSOFF to on!, so we can see it go offline/online the other way
User must wait five minutes to observe all PDoCs turn on
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNPC Set PDoCs to turn OFF when Online and ON when Offline!")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFFON, FNPC_NUM_PDOCS))

    target.close()
    
if __name__ == "__main__":
    main()
