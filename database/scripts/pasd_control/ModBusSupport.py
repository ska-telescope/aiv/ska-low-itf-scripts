
"""
To get constants for sensible defaults for functions we use below
"""
from FNCCConsts import *
from FNPCConsts import *
from FNSCConsts import *
from PASDConsts import *

"""
Functions that support general modbus actions
"""

MBUS_SUPPORT_VER = "1.5"

VOLT_MULTIPLIER=100
AMP_MULTIPLIER=100
TEMP_MULTIPLIER=100
HUMIDITY_MULTIPLIER=1

CONV_TYPE_VOLTAGE = 0
CONV_TYPE_CURRENT = 1
CONV_TYPE_TEMP = 2
CONV_TYPE_HUMIDITY = 3
CONV_TYPE_FEM_MA = 4
CONV_TYPE_STATUS = 5
CONV_TYPE_TOBITS = 6
CONV_TYPE_UINT8S = 7
CONV_TYPE_UINT16 = 8
CONV_TYPE_UINT32 = 9
CONV_TYPE_ALWARN = 10
CONV_TYPE_PCBREV = 11

CONV_SCALE = [100.0, 100.0, 100.0, 1, 1, 0, 0, 0, 0]
CONV_UNITS = ['V','A','C','%RH','mA']
CONV_PCB_FWTYPE = ['prelim','FNSC','FNPC','FNCC']

"""ModBus commands in decimal"""
MB_CMD_READ_HRS = 3 # Read Holding Registers
MB_CMD_WRITE_HR = 6 # Write Single Holding Register
MB_CMD_WRITE_MULTI = 16 # Write Multiple Holding Registers

"""ModBus register numbers in decimal"""
# Polling registers
# Common to everyone
SYS_MBRV = 1 # ModBus register map revision
SYS_PCBREV = 2 # Board revision number (FNCB or SBox MCU)
SYS_CPUID = 3 # Should be 0x6019 for PIC24FJ1024GA610
SYS_CPUREV = 4 # Should be 6
SYS_CHIPID = 5 # Eight 16-bit words conveying the 120-bit unique chip number burnt into the PIC24
SYS_FIRMVER = 13 # Firmware version number
SYS_UPTIME = 14 # 32-bit second counter since PIC24 reset/powered-on
SYS_ADDRESS = 16 # ModBus address - redundant?

# Config / programming registers common to everyone
CRC = 10001
ADDRESS = 10002
SEGMENT_DATA = 10003
# up to 120 registers here
COMMAND = 10125
RESULT = 10126
RESULT_DATA = 10127
# two registers here
CPU_LOAD = 10129
WARNINGS_LSW = 10130
WARNINGS_MSW = 10131 # empty
ALARMS_LSW = 10132
ALARMS_MSW = 10133 # empty

## Configuration registers, all specific
NUM_THRESHOLDS = 4  # These are the Alarm High, Warn High, Warn Low, Alarm Low fields.

""" Things that can be written to ModBus registers """
SERVICE_LED_ON =0x0100
SERVICE_LED_OFF=0x0000

# Values for preventing warnings or alarms from a given parameter
IGNORE_THRESHOLDS = [0,0,0,0] # ignore a given threshold set by setting them all to zero

""" Values to write into modbus packet could fields, or modbus regisetrs
"""

# Service LED values
SERVICE_LED_OFF    = 0x0000
SERVICE_LED_ON     = 0x0100
SERVICE_LED_5HZ    = 0x0200
SERVICE_LED_2HZ5   = 0x0300
SERVICE_LED_1HZ25  = 0x0400
SERVICE_LED_0HZ625 = 0x0500

# Constants for shifting various FEM/PDOC port bits
TECHO_SHIFT = 8
DSOFF_SHIFT = 10
DSON_SHIFT = 12
TECHO_MASK = 0x03
DSOFF_MASK = 0x03
DSON_MASK = 0x03

# Constants for testing port bit pairs
TECH_NO_PRESS = 0
TECH_SHORT_PRESS = 1
TECH_MEDIUM_PRESS = 2

PORT_DS_IGNORE = 0
PORT_DS_OFF = 2
PORT_DS_ON = 3

def from_ascii(mstring):
    """
    Take a string full of ASCII-hex value pairs (and nothing else), and convert it to a list of integers, each 0-255.
    Will break horrbily if it meets a character that isn't 0-9,A-F.

    :param mstring: A string of letters, each 0-9, or A-F, or a-f. String must have an even number of characters.
    :return: A list of integers, each 0-255
    """
    data = mstring.upper()
    numbytes, remainder = divmod(len(data), 2)
    if remainder != 0:
        return []

    try:
        returnlist = [int(data[i] + data[i + 1], 16) for i in range(0, numbytes * 2, 2)]
    except:
        returnlist = [-1]

    return returnlist

def sum_string(instring):
    """
    Function to compute the byte-wide sum of a string of ASCII characters
    that represent hexadecimal strings (0..9A..F), possibly preceded by a
    colon and followed by a CRLF (both of which are ignored, if present).
    If checking a string with LRC included, result should be zero
    If creating an LRC to append, the result should be subtracted from
    256, converted to a two-digit ASCII string and appended to the original string
    """
    return sum(from_ascii(instring.lstrip(':').rstrip())) & 0xFF

def mbprint(rrList, instring, query=True, ConvType=None, ConvUintStr=''):
    """
    Prettyprint a modbus string by spacing out the groupings. Instring can have a leading colon
    and trailing CRLF, but we'll ignore those, although we always print out the colon even if it
    wasn't in the original string. We never append the carriage return.
    We're essentially part-parsing the modbus packet here...
    
    If query is True, we're parsing a messsage from master to slave, otherwise a response

    If rrList[0] is a number greater than zero, we're parsing a response from a register read,
    and we want to process the returned values and return them by appending to rrList[], if anything goes
    wrong in the conversion return -1 in the corresponding list position, else return positive integers 0..65535.

    If we're parsing a query that is a register read, return the number of registers being read in rrList[0], otherwise leave rrList[0] alone

    If ConvType is set, and we're parsing a response to a register read(s) that consists of voltages(s), FNDH current(s), temperature(s), humidity, FEM current(s)
    which we convert and print here. All registers must be of the same type, or odd things will happen (eg. 50.00V will be shown as 50.00C, etc.) If it's a status
    response, use the replying modbus address to select the correct message(s)

    If ConvType is a UINT16 or UINT32, and ConvUintStr is not an empty string, then the string describes the meaning of the number.
    If the first character of the string is uppercase, it goes before the number, else it goes after the number
    """
  
    mbusstring = instring.lstrip(':').rstrip()
    if len(mbusstring) < 7: # not long enough to parse
        return ':' + mbusstring
    
    # First pair is always modbus address, second pair is always function
    modbusaddress = from_ascii(mbusstring[:2])[0]
    funcstring = mbusstring[2:4]
    flist = from_ascii(funcstring) # convert from hex-ascii to list-of-one integer
    function = flist[0] & 0x7F # now just gimme the integer without the error bit
    errorBit = flist[0] & 0x80 # top bit signals a fault
    isFNSC = (modbusaddress > 0) and (modbusaddress <= PASD_NUM_SMARTBOXES)
    isFNPC = (modbusaddress == FNPC_ADDR)
    isFNCC = (modbusaddress == FNCC_ADDR)

    if query:
        pretty = '->'
    else:
        pretty = '<-'
            
    pretty += ':' + mbusstring[:2] + ' ' + funcstring + ' '

    if errorBit: # regardless of what's called, an error / exception returns just the original command with top bit set, and error code, and LRC
        pretty += mbusstring[4:6] + ' ' +mbusstring[6:]
        rrList[0] = 0 # if it's a read-reg exception, no values to return
    else:
   
        if function == MB_CMD_READ_HRS: # read multi, query and response differ
            if query: # remainder is address, number of registers, both as quads, and LRC
                pretty += mbusstring[4:8] + ' ' + mbusstring[8:12] + ' ' + mbusstring[12:] # here we have two quads, the register address and number of registers to read, and the LRC
                regaddrlist = from_ascii(mbusstring[4:8])
                regaddr = regaddrlist[0] * 256 + regaddrlist[1] + 1
                numregslist = from_ascii(mbusstring[8:12])
                numregs = numregslist[0] * 256 + numregslist[1]
                rrList[0] = numregs
            else: # response: remainder is byte-count as pair (twice register count), then "N" quads (where N = byte-count / 2), then LRC
                numregs = from_ascii(mbusstring[4:6])[0] >> 1
                pretty += mbusstring[4:6] + ' '
                valString = ''
                highWord = True # if we're converting UINT32s, we do the high word first
                uint32holdreg = 0 # for carrying UINT16s between halves of a UINT32 conversion.
                
                for i in range(numregs):
                    quad = mbusstring[6+i*4 : 6+(i+1)*4]
                    pretty += quad + ' ' # do the quads

                    try:
                        val = int(quad, 16) # try to convert from quad to unsigned int
                    except:
                        val = None
                    # Add the response to the end of the rrList
                    rrList.append(val)
                    
                    if (ConvType is not None):
                        if (ConvType == CONV_TYPE_ALWARN):
                            if (isFNSC): # it's a smartbox
                                for i in reversed(range(FNSC_AW_MAXFLAGS)):
                                    if (val & (1 << i) != 0): # bit is set
                                        valString += ', %s' % FNSC_AW_STRINGS[i]
                                    else:
                                        valString += ', ----'
                            elif (isFNPC): # guess
                                for i in reversed(range(FNPC_AW_MAXFLAGS)):
                                    if (val & (1 << i) != 0): # bit is set
                                        valString += ', %s' % FNPC_AW_STRINGS[i]
                                    else:
                                        valString += ', ----'
                            else:
                                valString += ", errrr"
                        elif (ConvType == CONV_TYPE_UINT8S) or (ConvType == CONV_TYPE_PCBREV):
                            if (val is not None):
                                lowByte = val & 0xFF
                                highByte = (val >> 8) & 0xFF
                                if (ConvType == CONV_TYPE_UINT8S):
                                    valString += ", %d,%d" % (highByte,lowByte)
                                else:
                                    try:
                                        fwname = CONV_PCB_FWTYPE[lowByte]
                                    except:
                                        fwname = '???'
                                    valString += ", Hardware Step %d, Entity:%s" % (highByte, fwname)
                            else:
                                valString += ", --,--"
                        elif (ConvType == CONV_TYPE_UINT16):
                            if (val is not None):
                                if (ConvUintStr):
                                    if (ConvUintStr[0].isupper()): # leads with upper-case, string first
                                        valString += ", %s %d" % (ConvUintStr, val)
                                    else: # lowercase, string follows
                                        valString += ", %d %s" % (val, ConvUintStr)
                                else: # if the string is empty just show the number
                                    valString += ", %d" % (val)
                            else:
                                valString += ", -------"
                        elif (ConvType == CONV_TYPE_UINT32):
                            if (val is not None):
                                if (highWord): # first quad is high word, just grab it for now
                                    uint32holdreg = val
                                    highWord = False # get the low word next
                                else:
                                    uint32holdreg *= 65536
                                    uint32holdreg += val
                                    if (ConvUintStr):
                                        if (ConvUintStr[0].isupper()):
                                            valString += ", %s %d" % (ConvUintStr, uint32holdreg)
                                        else:
                                            valString += ", %d %s" % (uint32holdreg, ConvUintStr)
                                    else:
                                        valString += ", %d" % (uint32holdreg)
                                    highWord = True
                            else:
                                valString += ", -------"
                                
                        elif (ConvType == CONV_TYPE_TOBITS):
                            if (val is not None):
                                TOBits = (val >> TECHO_SHIFT) & TECHO_MASK
                                valString += ', 0b' + format(TOBits,'02b')
                            else:
                                valString += ', ----'
                        else:
                            if (val is not None):
                                if val > 32767:
                                    val -= 65536 # if it's negative, make it so
                                if (CONV_SCALE[ConvType] == 1):
                                    valString += ', %d%s' % (val, CONV_UNITS[ConvType]) # no scaling, whole numbers please
                                elif (ConvType == CONV_TYPE_STATUS):
                                    valStringAdd = ', --' # default in case of errors
                                    if (isFNSC): # it's a SMART Box
                                        if (val >= FNSC_SYS_STATE_OK) and (val <= FNSC_SYS_STATE_POWERDOWN):
                                            valStringAdd = ', %s' % (FNSC_CONV_STATUS[val])
                                    elif (isFNCC):
                                        if (val >= FNCC_SYS_STATE_OK) and (val <= FNCC_SYS_STATE_MODBUS_FRAME_ERROR_STUCK):
                                            valStringAdd = ', %s' % (FNCC_CONV_STATUS[val])
                                    elif (isFNPC):
                                        if (val >= FNPC_SYS_STATE_OK) and (val <= FNPC_SYS_STATE_POWERUP):
                                            valStringAdd = ', %s' % (FNPC_CONV_STATUS[val])
                                    valString += valStringAdd
                                else:
                                    valString += ', %0.2f%s' % (val / 100.0, CONV_UNITS[ConvType]) # divide by hundred
                            else:
                                valString += ', ------'
                pretty += mbusstring[6+numregs*4:] # and the LRC
                if (ConvType is not None):
                    if (ConvType != CONV_TYPE_ALWARN) and (len(valString) > 40):
                        pretty += '\n            (' + valString[2:] + ')'
                    else:
                        pretty += ' (' + valString[2:] + ')'
                            
               
        elif function == MB_CMD_WRITE_HR: # set-single holding register, mbusstring and response are identical
            pretty += mbusstring[4:8] + ' ' + mbusstring[8:12] + ' ' + mbusstring[12:] # here we have two quads, the register address and value to write, or value that was written, and the LRC
            
        elif function == MB_CMD_WRITE_MULTI: # write-multi-reg, query and response differ
            # Both have address as a quad and number of registers as a quad
            pretty += mbusstring[4:8] + ' ' + mbusstring[8:12] + ' '
            if query: # remainder is number of bytes pair, N*data quads, LRC
                regslist = from_ascii(mbusstring[8:12]) # two integers making a 16-bit result
                numregs = regslist[0] * 256 + regslist[1]
                pretty += mbusstring[12:14] + ' '
                for i in range(numregs):
                    pretty += mbusstring[14+i*4 : 14+(i+1)*4] + ' ' # do the quads
                pretty += mbusstring[14+numregs*4:] # and the LRC            
            else: # remainder is LRC
                pretty += mbusstring[12:]

    return pretty

# Read back one or more ModBus registers, example is read Status register in SMARTBOX 1
# This means that to read a single register back you only need the first two arguments
def ModBusReadRegisters(SlaveAddress=FNSC_01_ADDR, StartingRegister=FNSC_SYS_STATUS, Quantity=1):
    """
    Create string message to read "Quantity" Registers in slave at "SlaveAddress", starting with "StartingRegister"
    """
    MBusString = ":%02X" % (SlaveAddress) + "%02X" % (MB_CMD_READ_HRS) + "%04X" % (StartingRegister - 1) + "%04X" % (Quantity)
    MBusString += "%02X" % (256 - sum_string(MBusString)) # compute and add the LRC
    return MBusString + '\r\n'

def ModBusReadThresholds(SlaveAddress=FNPC_ADDR, StartingRegister=FNPC_SYS_48V1_V_TH, Quantity = 1):
    """
    Create string message to read "Quantity" sets of Threshold Registers in slave at "SlaveAddress", starting with "StartingRegister"
    """
    MBusString = ":%02X" % (SlaveAddress) + "%02X" % (MB_CMD_READ_HRS) + "%04X" % (StartingRegister - 1) + "%04X" % (Quantity * NUM_THRESHOLDS)
    MBusString += "%02X" % (256 - sum_string(MBusString)) # compute and add the LRC
    return MBusString + '\r\n'


# Write a single register, example is Smartbox 1's SYS_STATUS register to ZERO.
def ModBusWriteRegister(SlaveAddress=FNSC_01_ADDR, Register=FNSC_SYS_STATUS, Value=0):
    """
    Create string message to write "Value" to "Register" in slave at "SlaveAddress"
    """
    MBusString = ":%02X" % (SlaveAddress) + "%02X" % (MB_CMD_WRITE_HR) + "%04X" % (Register - 1) + "%04X" % (Value)
    MBusString += "%02X" % (256 - sum_string(MBusString)) # compute and add the LRC
    return MBusString + '\r\n'
    
# This function can write the same value to a contiguous set of ModBus registers, the example being all the SB FEM current limits
def ModBusWriteSameValues(SlaveAddress=FNSC_01_ADDR, StartReg=FNSC_P01_CURRENT_TH, Value=FEM_MAX_CURRENT, Quantity=FNSC_NUM_FEMS):
    """
    Function to write multiple registers, with the same data.
    ModBus packet is as follows, number in brackets indicates how many ASCII characters in the field:
    Address(2) Command(2) StartingRegister(4) NumberOfRegisters(4) NumberOfBytes(2) Values(4 * Quantity) .... LRC(2)
    """
    MBusString = ":%02X" % (SlaveAddress) + "%02X" % (MB_CMD_WRITE_MULTI) + "%04X" % (StartReg - 1) + "%04X" % (Quantity) + "%02X" % (Quantity * 2) + ("%04X" % (Value) * Quantity)
    MBusString += "%02X" % (256 - sum_string(MBusString)) # compute and add the LRC
    return MBusString + '\r\n'

# This function can write one or more sets of threshold registers with the same AH/WH/WL/AL value set. Eg. setting multiple temperature
#  limits for thermistors expected to see similar temperatures, setting all 8 PSU voltage limits in the SMARTBox etc. etc.
def ModBusWriteMultiValueLists(SlaveAddress=FNSC_01_ADDR, StartReg=FNSC_SYS_48V_V_TH, ValueList=FNSC_48V_LIMITS, Multiplier=VOLT_MULTIPLIER, NumRegSets=1):
    """
    Function to write multiple sets of registers, with the contents of a list of values, after first multiplying each of the values by a Multiplier.
    ModBus packet is as follows, number in brackets indicates how many ASCII characters in the field:
    Address(2) Command(2) StartingRegister(4) NumberOfRegisters(4) NumberOfBytes(2) Values(4 * len(LIST) * Quantity) .... LRC(2)
    """
    # Firstly make a string for all of the items in the list, multiplying each one prior to converting to a hex quad.
    valuestring = ''.join("%04X" % (int(round(th * Multiplier)) & 0xFFFF) for th in ValueList)
    
    # This many values to write
    Quantity = len(ValueList) * NumRegSets

    MBusString = ":%02X" % (SlaveAddress) + "%02X" % (MB_CMD_WRITE_MULTI) + "%04X" % (StartReg - 1) + "%04X" % (Quantity) + "%02X" % (Quantity * 2) + (valuestring * NumRegSets)
    MBusString += "%02X" % (256 - sum_string(MBusString)) # compute and add the LRC
    return MBusString + '\r\n'

# This function can write one or more sets of PDoC or FEM STATE registers with a repeating pattern from ValueList. Eg. setting alternate ports on and off.
def ModBusWriteMultiPortStateLists(SlaveAddress=FNPC_ADDR, StartReg=FNPC_P01_STATE, ValueList=[FNPC_PDOC_ON,FNPC_PDOC_OFF], NumPortSets=1):
    """
    Function to write multiple sets of registers, with the contents of a list of values.
    ModBus packet is as follows, number in brackets indicates how many ASCII characters in the field:
    Address(2) Command(2) StartingRegister(4) NumberOfRegisters(4) NumberOfBytes(2) Values(4 * len(ValueList) * NumPDoCSets) .... LRC(2)
    """
    # Firstly make a string for all of the items in the list, converting to a hex quad.
    valuestring = ''.join("%04X" % th for th in ValueList)
    
    # This many values to write
    Quantity = len(ValueList) * NumPortSets

    MBusString = ":%02X" % (SlaveAddress) + "%02X" % (MB_CMD_WRITE_MULTI) + "%04X" % (StartReg - 1) + "%04X" % (Quantity) + "%02X" % (Quantity * 2) + (valuestring * NumPortSets)
    MBusString += "%02X" % (256 - sum_string(MBusString)) # compute and add the LRC
    return MBusString + '\r\n'

def SendMessage(conn, instring, ConvType = None, ConvUintStr = ''):
    """
    Send modbus message "instring" over connection "conn", echo to stdout, try for a reply within 1 second and echo that too.
    return the response message if given. If ConvTemps is true, convert any returned quads to deg C.

    IF the send-string is a single register read of either FNPC_SYS_STATUS or FNSC_SYS_STATUS register or a single PDoC or FEM port state register, return the value received in the response via the SendMessage return mechanism.
    """
    regReadList = [0] # list of zero things being read
    
    print(mbprint(regReadList, instring.rstrip(),True), end= " | ")
    # at this point if we just sent out a read registers, the number of registers being read will be in regReadList[0], else it'll be 0

    # print('rrQ='+str(regReadList))

    conn.write(instring.encode('ascii'))
                               
    response = ''
    for byteval in conn.read_until(b'\n'):
        if byteval < 128:
            response += chr(byteval)
        else:
            print('*0x%02X*'%(byteval),end='')

    if len(response) > 0:
        print(mbprint(regReadList, response.rstrip(),False, ConvType, ConvUintStr))
        # print('rrR='+str(regReadList))
    else:
        print('<no-resp>')
        regReadList[0] = 0

    return regReadList

def GetFWVersions(conn):
    """
    Retrieve FNPC and FNCC and any attached SMARTBOX firmware versions
    """
    print("\nFNCC     FW: ", end='')
    SendMessage(conn, ModBusReadRegisters(FNCC_ADDR, SYS_FIRMVER), CONV_TYPE_UINT16, 'Ver')

    print("FNPC     FW: ", end='')
    SendMessage(conn, ModBusReadRegisters(FNPC_ADDR, SYS_FIRMVER), CONV_TYPE_UINT16, 'Ver')

    for i in range(PASD_NUM_SMARTBOXES):
        print("FNSC(%02d) FW: " % (i + 1), end='')
        SendMessage(conn, ModBusReadRegisters(FNSC_01_ADDR + i, SYS_FIRMVER), CONV_TYPE_UINT16, 'Ver')





