import platform
import serial
import argparse
import sys
from FNPCConsts import *
from FNSCConsts import *
from PASDConsts import *

OPEN_COMMS_VER = "1.1"

# Communications timeout value in seconds
COMMS_TIMEOUT=0.5

# some typical default values
##HOST = "10.128.30.1" # Default Curtin Wiznet IP address.
##HOST = "10.137.0.129" # AAVS3 PaSD IP address.
HOST = "10.135.156.201" # ITF PaSD IP address.
#HOST = "10.135.155.51" # ITF Thermal Chamber PaSD IP address.
#HOST = "10.132.2.202" # AA0.5 s9-2 PaSD IP address.
PORT = 5000 # default TCP port
COMPORT = "COM7"

def OpenComms():

    defCom = None
    defHost = None
    
    if (platform.system() == 'Linux'):
        defComm = None
        defHost = HOST
    elif (platform.system() == 'Windows'):
        ##defComm = COMPORT
        ##defHost = None
        defComm = None
        defHost = HOST

    parser = argparse.ArgumentParser(description='Quick checkout FNDH and SMARTBox')
    parser.add_argument('--host', dest='host', default=defHost, help='IP address of an ethernet-serial gateway, eg 10.128.30.1, default as shown')
    parser.add_argument('--portnum', dest='portnum', default=5000, help='TCP port number to use, default 5000')
    parser.add_argument('--com', dest='COMport', default=defComm, help='Serial port Comport name, eg /dev/ttyS0 or COM9, default None')
    parser.add_argument('--COM', dest='COMport', default=defComm, help=argparse.SUPPRESS)
    parser.add_argument('--sb', dest='SBNumber', default=1, help='SMARTBox number/address, default 1')
    parser.add_argument('--SB', dest='SBNumber', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--sB', dest='SBNumber', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--Sb', dest='SBNumber', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--pdoc', dest='PDoC', default=1, help='PDoC port number, default 1')
    parser.add_argument('--PDOC', dest='PDoC', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--PDoC', dest='PDoC', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--Pdoc', dest='PDoC', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--fem', dest='FEM', default=1, help='FEM port number, default 1')
    parser.add_argument('--FEM', dest='FEM', default=1, help=argparse.SUPPRESS)
    parser.add_argument('--Fem', dest='FEM', default=1, help=argparse.SUPPRESS)

    args = parser.parse_args()

    if (type(args.SBNumber) == 'int'): # argument not given, default is one
        SBAddress = 1
    else:
        try:
            SBAddress = int(args.SBNumber) # try converting to an integers
        except:
            print('Error: SBNumber must be an integer between 1 and %d inclusive' % (PASD_NUM_SMARTBOXES))
            sys.exit(-2)
        if (SBAddress < 1) or (SBAddress > PASD_NUM_SMARTBOXES):
            print('Error: SBNumber must be an integer between 1 and %d inclusive' % (PASD_NUM_SMARTBOXES))
            sys.exit(-3)

    if (type(args.PDoC) == 'int'): # they didn't give this arg, use default
        PDoCPort = PDoC
    else:
        try:
            PDoCPort = int(args.PDoC) # try converting to an integers
        except:
            print('Error: PDoC must be an integer between 1 and %d inclusive' % (FNPC_NUM_PDOCS))
            sys.exit(-4)
        if (PDoCPort < 1) or (PDoCPort > FNPC_NUM_PDOCS):
            print('Error: PDoC must be an integer between 1 and %d inclusive' % (FNPC_NUM_PDOCS))
            sys.exit(-5)

    if (type(args.FEM) == 'int'): # they didn't give this arg, use default
        FEMPort = FEM
    else:
        try:
            FEMPort = int(args.FEM) # try converting to an integers
        except:
            print('Error: FEM must be an integer between 1 and %d inclusive' % (FNSC_NUM_FEMS))
            sys.exit(-6)
        if (FEMPort < 1) or (FEMPort > FNSC_NUM_FEMS):
            print('Error: FEM must be an integer between 1 and %d inclusive' % (FNSC_NUM_FEMS))
            sys.exit(-7)

    if (args.host is not None): # IP address given...
        try:
            theport = serial.serial_for_url('socket://%s:%d' % (args.host, args.portnum))
        except:
            print('Error trying to open telnet connection to %s:%d' % (args.host, args.portnum))
            sys.exit(-8)
    else: 
        try:
            theport = serial.Serial(args.COMport,baudrate=19200)
        except:
            print('Error trying to open serial port %s' % (args.COMport))
            sys.exit(-9)

    theport.timeout = COMMS_TIMEOUT # seconds to wait for a read.
    
    return (theport, SBAddress, PDoCPort, FEMPort) 

