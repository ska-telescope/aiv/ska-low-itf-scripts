import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

## The threshold current in mA. The default is 495 mA.
current_thresh = 495
number_of_fems = 12

for arg in sys.argv:
    if (arg[0:7] == "--curr=") and (arg[7:].isdigit() == True):
        current_thresh = int(arg[7:])
        sys.argv.remove(arg)

(target, SBAddress, PDoCPort, FEMPort) = OpenComms()

SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_ON, FNPC_NUM_PDOCS))

time.sleep(0.5)

for SBAddress in range(1, 25):
    print("\nFNSC(%02d) Set STATUS_OK" % (SBAddress))
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
    
    ##input("Press enter to set thresholds.")
    
    for FEMPort in range(1, number_of_fems):
        print("\nFNSC(%02d) Setting FEM %d current threshold to %d" % (SBAddress, FEMPort, current_thresh))
        SendMessage(target, ModBusWriteRegister(SBAddress, (FNSC_P01_CURRENT_TH - 1) + FEMPort, current_thresh))
        
    ##input("Press enter to turn on FEMs")
        
    print("FNSC(%02d) Turn on FEMs" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ON, FNSC_NUM_FEMS))
    
    time.sleep(0.5)

    print("FNSC(%02d) FEM Currents" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)

##response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
##response = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))

target.close()