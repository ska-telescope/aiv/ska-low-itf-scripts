import sys
import time
import argparse
import serial
from ModBusSupport import *
from FNPCConsts import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNSC(%02d) Turn off FEM %d" % (SBAddress, FEMPort))
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_P01_STATE + FEMPort - 1, FNSC_FEM_OFF))

    target.close()
    
if __name__ == "__main__":
    main()
