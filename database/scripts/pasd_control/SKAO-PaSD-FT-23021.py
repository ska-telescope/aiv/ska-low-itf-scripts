import sys
import time
import argparse
import serial
from FNPCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23021
MCCS Read FNPC PDoC ports to confirm functionality of Tech button override and
interaction with MCCS write STATE_OK (should only clear TO bits 0b01 to 0b00)
User must operate tech interface button for desired intervals.
"""

POLLING_DELAY_SECONDS = 1
STATE_OK_DELAY = 0.5
SECONDS_PER_TEST = 15
TIMES_TO_LOOP = int(SECONDS_PER_TEST / POLLING_DELAY_SECONDS)

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    while True:
        print("FNPC Monitor half of the Pxx_STATE for PDoC powered state (0x--C0) and Tech override bits")
        print("FNPC Monitor SYS_STATUS for SYS_POWERUP")
        for i in range(TIMES_TO_LOOP):
            SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS >> 1), CONV_TYPE_TOBITS)
            rV = SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)
            # print('rV=' + str(rV))
            if (rV[0] == 1) and (rV[1] == FNPC_SYS_STATE_POWERUP):
                print("POWERUP detected, test concluded")
                return
            time.sleep(POLLING_DELAY_SECONDS)
        print("FNPC Attempt to set STATE_OK")
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_ON))
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
        time.sleep(STATE_OK_DELAY)
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_LIGHTS, SERVICE_LED_OFF))

    target.close()

if __name__ == "__main__":
    main()
