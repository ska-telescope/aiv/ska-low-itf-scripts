import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23012
MCCS Updates FNPC Configuration registers (write zero to demonstrate clearing them all)
    Then re-write sensible / default values and re-confirm
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNPC Restore DSON/DSOFF after previous tests")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_ON, FNPC_NUM_PDOCS))

    print("\nFNPC Set all configuration registers to zero, clears thresholds => sensors don't generate warnings or alarms")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_SYS_48V1_V_TH, 0, FNPC_NUM_CONFIG_REGS))

    print("\nFNPC Confirm updated Configuration Registers")
    for i in range(FNPC_NUM_CONFIG_REGSETS):
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_V_TH + (i * 4), 4))

    print("\nFNPC Restore sensible thresholds into Configuration Registers")
    print("FNPC Write and confirm 48V voltage limits as %s" % (FNPC_48V_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_48V1_V_TH, FNPC_48V_LIMITS, VOLT_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V1_V_TH), CONV_TYPE_VOLTAGE)

    print("\nFNPC Write and confirm 48V current limits as %s" % (FNPC_CURR_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_48V_I_TH, FNPC_CURR_LIMITS, AMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V_I_TH), CONV_TYPE_CURRENT)

    print("\nFNPC Write and confirm Temperature limits at top and bottom of COSEL SMPS baseplate as %s" % (FNPC_COSEL_TEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_48V1_TEMP_TH, FNPC_COSEL_TEMP_LIMITS, TEMP_MULTIPLIER, 2))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_48V1_TEMP_TH, 2), CONV_TYPE_TEMP)

    print("\nFNPC Write and confirm FNCB Temperature limits as %s" % (FNPC_FNCBTEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_FNCBTEMP_TH, FNPC_FNCBTEMP_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_FNCBTEMP_TH), CONV_TYPE_TEMP)

    print("\nFNPC Write and confirm Humidity limits as %s" % (FNPC_HUMIDITY_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_HUMIDITY_TH, FNPC_HUMIDITY_LIMITS, HUMIDITY_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_HUMIDITY_TH), CONV_TYPE_HUMIDITY)

    print("\nFNPC Write and confirm FNDP EP Temperature limits - external surface of CG as %s" % (FNPC_SENSE_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_SENSE01_TH, FNPC_SENSE_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE01_TH), CONV_TYPE_TEMP)

    print("\nFNPC Write and confirm FNDP EP Temperature limits - external surface of PM as %s" % (FNPC_COSEL_TEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_SENSE02_TH, FNPC_COSEL_TEMP_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE02_TH), CONV_TYPE_TEMP)

    print("\nFNPC Write and confirm FNDP EP Temperature limits - \"floor\" of enclosure as %s" % (FNPC_SENSE_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_SENSE03_TH, FNPC_SENSE_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE03_TH), CONV_TYPE_TEMP)
    
    print("\nFNPC Write and confirm FNDP EP Temperature limits - \"roof\" of enclosure as %s" % (FNPC_SENSE_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_SENSE04_TH, FNPC_SENSE_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(FNPC_ADDR, FNPC_SYS_SENSE04_TH), CONV_TYPE_TEMP)
    
    
    

    target.close()
    
if __name__ == "__main__":
    main()
