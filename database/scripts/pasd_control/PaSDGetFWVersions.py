"""
Quick versions based on FullTestSequenceSer.py that is desigend for quickly verifying FNDH PDoC comms

Sets workable thresholds, turns all FNDH PDoCs on (one at a time with suitable short delay),
 then does the quick checks on SB01 (or am alternate Smartbox provided on the command line)

"""

VERSION = "1.1"

import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    GetFWVersions(target)

    target.close()
    
if __name__ == "__main__":
    main()
