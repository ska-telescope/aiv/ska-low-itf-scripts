import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

# delays in seconds
PDOC_STEP_DELAY = 3
SBOX_BOOT_DELAY = 2

"""
(Firmware) Functional Test FT-23009 part 1
Turn off ALL PDoCs, then turn them on two-ish second apart.
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNPC Set Status")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))

    print("FNPC Turn off all PDoCS")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFF, FNPC_NUM_PDOCS))

    print("--- wait %d seconds to guarantee all SMARTBoxes are off" % (SBOX_BOOT_DELAY))
    time.sleep(SBOX_BOOT_DELAY)

    print("\nFNPC Turn on PDoCs %d seconds apart" % (PDOC_STEP_DELAY))
    for i in range(FNPC_NUM_PDOCS):
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + i, FNPC_PDOC_ON))
        time.sleep(PDOC_STEP_DELAY)

    print("--- wait %d seconds to guarantee all SMARTBoxes are booted" % (SBOX_BOOT_DELAY))
    time.sleep(SBOX_BOOT_DELAY)

    print("\nFNSC(**) Read SYS_UPTIME registers")
    for i in range(PASD_NUM_SMARTBOXES):
        SendMessage(target, ModBusReadRegisters(SBAddress + i, SYS_UPTIME, 2), CONV_TYPE_UINT32, 'sec')
        # time.sleep(0.1)
    
    target.close()
    
if __name__ == "__main__":
    main()
