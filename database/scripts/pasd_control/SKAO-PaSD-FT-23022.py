import sys
import time
import argparse
import serial
from FNSCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23022
MCCS Read SMART Box FEM ports to confirm functionality of over-current detection.
USER observes interaction with tech button presses, MCCS STATUS_OK transitions, and
senors transitioning into/out of nominal values.
"""

POLLING_DELAY_SECONDS = 1
RESET_DELAY = 0.2
SECONDS_PER_TEST = 5
TIMES_TO_LOOP = int(SECONDS_PER_TEST / POLLING_DELAY_SECONDS)

RESET_CB = 0
RESET_STATUS_OK = 1

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    tryToReset = RESET_STATUS_OK
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_OFF))
    print("FNSC(%02d) SERVICE LED indicates the action is reset CB (LED on) or STATUS_OK (LED off)" % (SBAddress))
    while True:
        print("FNSC(%02d) Monitor Pxx_STATE for FEM Powered on (0x--40), CB Trip (0x--80) and TO bits" % (SBAddress))
        for i in range(TIMES_TO_LOOP):
            SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, 4), CONV_TYPE_TOBITS)
            time.sleep(POLLING_DELAY_SECONDS)
        if (tryToReset == RESET_CB):
            print("FNSC(%02d) Attempt circuit breaker reset - should clear tripped breakers only if overload removed" % (SBAddress))
            time.sleep(RESET_DELAY)
            SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_CB_RESET, FNSC_NUM_FEMS))
            time.sleep(RESET_DELAY)
            tryToReset = RESET_STATUS_OK # next time around
            SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_OFF))
        else:
            print("FNSC(%02d) Attempt transition to STATUS_OK - should clear TO=0b01, but NOT clear tripped breakers!" % (SBAddress))
            time.sleep(RESET_DELAY)
            SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
            time.sleep(RESET_DELAY)
            tryToReset = RESET_CB # next time around
            SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_ON))

    target.close()

if __name__ == "__main__":
    main()
