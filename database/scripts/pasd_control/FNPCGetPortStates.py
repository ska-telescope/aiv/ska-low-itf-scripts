import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNPC Read PDoC Port Registers")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS))

    target.close()
    
if __name__ == "__main__":
    main()
