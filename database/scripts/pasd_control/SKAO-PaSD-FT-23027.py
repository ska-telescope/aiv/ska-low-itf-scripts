import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23027
Read First 14 Polling registers from FNCC firmware

"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNCC Read Modbus Register Revision Number")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, SYS_MBRV), CONV_TYPE_UINT16, 'Rev')

    print("\nFNCC Read FNCB Board Revision Number")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, SYS_PCBREV), CONV_TYPE_PCBREV)

    print("\nFNCC Read Microchip CPU ID")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, SYS_CPUID, 2))

    print("\nFNCC Read Microchip Unique CHIP ID")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, SYS_CHIPID, 8))

    print("\nFNCC Read Firmware revision number ")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, SYS_FIRMVER), CONV_TYPE_UINT16, 'Rev')
    
    print("\nFNCC Read Uptime")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, SYS_UPTIME, 2), CONV_TYPE_UINT32, 'sec')

    target.close()

if __name__ == "__main__":
    main()

