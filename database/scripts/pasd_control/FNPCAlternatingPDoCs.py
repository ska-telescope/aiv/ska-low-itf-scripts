import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.1"

"""
Set PDoCs in FNDH so that ODD PDoCs are on, Even PDoCs are off
Pre-req: FNDH powered on and initialised
Suggest: FNPCSetStatus
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    DoAlternatePDoCSonoff(target)

    target.close()
    
if __name__ == "__main__":
    main()
