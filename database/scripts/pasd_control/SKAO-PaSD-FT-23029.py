import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23029
Read First 14 Polling registers from SMART Box firmware

"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Read Modbus Register Revision Number" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, SYS_MBRV), CONV_TYPE_UINT16, 'Rev')

    print("\nFNSC(%02d) Read SMART Box MCU Board Revision Number" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, SYS_PCBREV), CONV_TYPE_PCBREV)

    print("\nFNSC(%02d) Read Microchip CPU ID" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, SYS_CPUID, 2))

    print("\nFNSC(%02d) Read Microchip Unique CHIP ID" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, SYS_CHIPID, 8))

    print("\nFNSC(%02d) Read Firmware revision number " % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, SYS_FIRMVER), CONV_TYPE_UINT16, 'Rev')

    print("\nFNSC(%02d)Read Uptime" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, SYS_UPTIME, 2), CONV_TYPE_UINT32, 'sec')

    target.close()

if __name__ == "__main__":
    main()

