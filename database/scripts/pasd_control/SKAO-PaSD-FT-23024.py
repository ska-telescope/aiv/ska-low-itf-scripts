import sys
import time
import argparse
import serial
from FNSCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23024
Sequence of ops to detect SBox Shutdown, port move, and "detect" SBox on new PDoC
Initialise SMARTBox State OK plus all FEMS on
Wait for long-press on SBox
TechOverride OFF PDoC
Wait for short-press FNPC
FNPC STate OK to clear TO bits
Try read SBox - fail
Turn on next PDoC
Try read SBox - pass
Turn off initial PDoC
"""

POLLING_DELAY_SECONDS = 1
STATE_OK_DELAY = 0.5
SBOX_ON_DELAY = 1.5

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    # Select the next port in line after the one on the command line (or the default)
    PDoCAltPort = PDoCPort + 1
    if (PDoCAltPort > FNPC_NUM_PDOCS): # wrap!
        PDoCAltPort = 1

    print("FNPC Initial state, assumes SMART Box %02d is attached to PDoC %02d, and PDoC %02d has no SMART Box connected" % (SBAddress, PDoCPort, PDoCAltPort))
    print("Turn ON PDoC %02d, turn OFF PDoC %02d" % (PDoCPort, PDoCAltPort))
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort - 1), FNPC_PDOC_ON))
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCAltPort - 1), FNPC_PDOC_OFF))
    print("... Brief pause to allow SMART Box to boot")
    time.sleep(SBOX_ON_DELAY)
    print("Initialise SMART Box %02d" % (SBAddress))
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
    rVals = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
    if (rVals[0] == 0): # No SMARTBox, not much point continuing!
        print("\nERROR: SMART Box %02d did not appear on PDoC %02d!!" % (SBAddress, PDoCPort))
    else:
        SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ON, FNSC_NUM_FEMS))
        print("FNSC(%02d) Monitor for POWERDOWN" % (SBAddress))
        while True:
            rVals = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
            if (rVals[0] == 1) and (rVals[1] == FNSC_SYS_STATE_POWERDOWN):
                break
            time.sleep(POLLING_DELAY_SECONDS) # poll delay
        print()
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort - 1), FNPC_PDOC_POWERDOWN))
        print("FNPC Turned off PDoC %02d via TO bits, please move RG6 cable to PDoC port %02d" % (PDoCPort, PDoCAltPort))
        print("... then enter short-press on FNPC Tech button\n")
        print("FNPC Monitor PDoC %02d for Short-press (PDoc TO bits go to 0b01)" % (PDoCPort))
        while True:
            rVals = SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort-1)), CONV_TYPE_TOBITS)
            if (rVals[0] == 1) and (((rVals[1] >> TECHO_SHIFT) & TECHO_MASK) == TECH_SHORT_PRESS):
                break
            time.sleep(POLLING_DELAY_SECONDS) # poll delay
        print("\nFNPC Short-press detected, now setting Status OK to clear TO bits")
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort - 1)), CONV_TYPE_TOBITS)
        print("... Brief pause to allow SMART Box to boot")
        time.sleep(SBOX_ON_DELAY)
        print("FNSC(%02d) Try read status - should fail" % (SBAddress))
        rVals = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
        if (rVals[0] > 0): # read succeeded, it should have failed!!
            print("\nrVals")
            print("\nERROR: SMART Box %02d still attached to PDoC %02d!!" % (SBAddress, PDoCPort))
        else:
            print("SMART Box %02d not detected on PDoC %02d, as expected" % (SBAddress, PDoCPort))
            print("\nFNPC Turn on PDoC %02d" % (PDoCAltPort))
            SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCAltPort - 1), FNPC_PDOC_ON))
            print("... Brief pause to allow SMART Box to boot")
            time.sleep(SBOX_ON_DELAY)
            print("FNSC(%02d) Try read status again - should succeed" % (SBAddress))
            rVals = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
            if (rVals[0] == 0): # read failed, it should have succeeded!!
                print("\nERROR: SMART Box %02d not visible on either PDoC %02d or PDoC %02d" % (SBAddress, PDoCPort, PDoCAltPort))
            else:
                print("SMART Box %02d detected on PDoC %02d, as expected" % (SBAddress, PDoCAltPort))
                print("\nFNPC Turn off PDoc %02d" % (PDoCPort))
                SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort - 1), FNPC_PDOC_OFF))

    target.close()

if __name__ == "__main__":
    main()
