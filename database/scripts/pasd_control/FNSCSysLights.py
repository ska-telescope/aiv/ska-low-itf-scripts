import sys
import time
import argparse
import serial
from OpenComms import *
from ModBusSupport import *
from FNSCFuncs import *

VERSION="1.1"


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    FNSCSysLights(target, SBAddress)

    target.close()
    
if __name__ == "__main__":
    main()
