import sys
import time
import argparse
import serial
from OpenComms import *
from ModBusSupport import *

VERSION="1.0"


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNPC write 10Hz low-pass filter cutoff to 48V voltage polling register")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_48V1_V, 9105))
    
    print("FNPC write 10Hz low-pass filter cutoff to 48V current polling register")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_48V_I, 9105))
    
    target.close()
    
if __name__ == "__main__":
    main()
