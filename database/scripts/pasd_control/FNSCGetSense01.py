import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNSC(%02d) Get Thermistor SYS_SENSE01" % (SBAddress))
    while True:
        SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE01), CONV_TYPE_TEMP) # Final arg shows temp in C
        time.sleep(0.5)

    target.close()
    
if __name__ == "__main__":
    main()
