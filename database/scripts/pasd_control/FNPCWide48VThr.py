import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    print("\nFNPC Set wider 48V voltage and current thresholds")
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_48V1_V_TH, [55,52,44,40], VOLT_MULTIPLIER))
    SendMessage(target, ModBusWriteMultiValueLists(FNPC_ADDR, FNPC_SYS_48V_I_TH, [18,16,-2,-5], AMP_MULTIPLIER))
        
    target.close()
    
if __name__ == "__main__":
    main()
