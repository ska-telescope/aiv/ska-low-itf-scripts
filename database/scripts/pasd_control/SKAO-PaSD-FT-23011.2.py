import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23011 part 2
Set FEMs with DSON to OFF, and DSOFF to on!, so we can see it go offline/online the other way
User must wait five minutes to observe all FEMs turn on
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Set FEMs to turn on when Online and OFF when Offline" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_OFFON, FNSC_NUM_FEMS))

    target.close()
    
if __name__ == "__main__":
    main()
