import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.2"

"""
Set FEMs with DSON to on, and DSOFF to off, so we can see it go offline
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Turn on FEMs" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ONOFF, FNSC_NUM_FEMS))

    print("FNSC(%02d) FEM Currents" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)

    target.close()
    
if __name__ == "__main__":
    main()
