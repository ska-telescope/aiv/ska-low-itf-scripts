import sys
import time
import argparse
import serial
from OpenComms import *
from ModBusSupport import *
from FNSCFuncs import *

VERSION="1.1"

lightsrates = [SERVICE_LED_1HZ25, SERVICE_LED_0HZ625, SERVICE_LED_2HZ5]


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    for i in range(24):
        SendMessage(target, ModBusWriteRegister(i, FNSC_SYS_LIGHTS, lightsrates[i % 3]))

    target.close()
    
if __name__ == "__main__":
    main()
