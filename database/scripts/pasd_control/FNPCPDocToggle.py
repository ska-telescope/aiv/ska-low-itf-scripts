import sys
import time
import argparse
import serial
from ModBusSupport import *
#from FNPCConsts import *
from OpenComms import *

VERSION = "1.1"


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    while True:

        print("\nFNPC Turn on PDoC %d" % (PDoCPort))
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + PDoCPort - 1, FNPC_PDOC_ON))
        SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_SYS_STATUS), CONV_TYPE_STATUS)
        SendMessage(target, ModBusWriteRegister(FNCC_ADDR, FNCC_SYS_STATUS, FNCC_SYS_STATE_OK))

        time.sleep(2)

        print("\nFNPC Turn off PDoC %d" % (PDoCPort))
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + PDoCPort - 1, FNPC_PDOC_OFF))
        SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_SYS_STATUS), CONV_TYPE_STATUS)
        SendMessage(target, ModBusWriteRegister(FNCC_ADDR, FNCC_SYS_STATUS, FNCC_SYS_STATE_OK))

        time.sleep(2)

    target.close()
    
if __name__ == "__main__":
    main()
