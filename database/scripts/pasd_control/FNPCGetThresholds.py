import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    GetFNPCThresholds(target)
    
    target.close()
    
if __name__ == "__main__":
    main()
