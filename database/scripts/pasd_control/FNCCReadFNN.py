import sys
import time
import argparse
import serial
from ModBusSupport import *
from FNCCConsts import *
from OpenComms import *

VERSION = "1.0"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    print("\nFNCC Read Field Node Number")
    replyList = SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_FIELD_NODE_NUMBER), CONV_TYPE_UINT16, 'FN Number')

    target.close()
    
if __name__ == "__main__":
    main()
