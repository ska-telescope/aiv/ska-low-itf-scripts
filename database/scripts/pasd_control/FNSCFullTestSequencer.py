import sys
import time
import argparse
import serial
from FNPCFuncs import *
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    ServiceOnFNDH(target)

    ServiceOnSBox(target, SBAddress)

    StartSmartBox(target, SBAddress)

    FNSCGetThresholds(target, SBAddress)

    DoWalkingFEMs(target, SBAddress)

    ServiceOffSBox(target, SBAddress)

    ServiceOffFNDH(target)

    target.close()
    
if __name__ == "__main__":
    main()
