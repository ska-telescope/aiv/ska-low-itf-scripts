import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
Set PDoCs with DSON to on, and DSOFF to off, so we can see it go offline
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNDH Set PDoCs to turn on when Online and OFF when Offline")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_ONOFF, FNPC_NUM_PDOCS))

    target.close()
    
if __name__ == "__main__":
    main()
