import sys
import time
import argparse
import serial
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.3"

"""
(Firmware) Functional Test FT-23008
MCCS Read SMART Box warnings/alarms, try to set STATE_OK, turn on FEMs via DSON/DSOFF
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    FNSCGetALWarnBits(target, SBAddress)

    print("\nFNSC(%02d) Try to set SYS_STATUS to STATUS_OK" % (SBAddress))
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
    print("FNSC(%02d) Read SYS_STATUS" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nFNSC(%02d) Turn FEMs ON" % (SBAddress))
    for i in range(FNSC_NUM_FEMS):
        SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_P01_STATE + i, FNSC_FEM_ON))
        time.sleep(0.1)

    print("\nFNSC(%02d) Read FEM Port states" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
    
    target.close()
    
if __name__ == "__main__":
    main()
