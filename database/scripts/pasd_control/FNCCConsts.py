FNCC_CONSTS_VER = "1.2"

# FNCC ModBus address
FNCC_ADDR = 100

# FNCC Specific polling registers
FNCC_SYS_STATUS = 17 # Whether we've had a framing error or BUS STUCK signal
FNCC_FIELD_NODE_NUMBER = 18 # the four-digit switch reading

# FNCC Specific Config registers
FNCC_BUS_CONTROL = 1001 # if clear allows SBox comms both ways, if set, blocks both ways

FNCC_SYS_STATE_OK = 0
FNCC_SYS_STATE_RESET = 1
FNCC_SYS_STATE_MODBUS_FRAME_ERROR = 2
FNCC_SYS_STATE_MODBUS_STUCK = 3
FNCC_SYS_STATE_MODBUS_FRAME_ERROR_STUCK = 4

# Long status strings for mainline output, and short strings for in-line output
FNCC_STATE_STRINGS = ["STATE_OK", "STATE_RESET", "STATE_MODBUS_FRAME_ERROR", "STATE_MODBUS_STUCK", "STATE_MODBUS_FRAME_ERROR_STUCK"]
FNCC_CONV_STATUS = ['OK', 'Reset', 'Frame', 'Stuck', 'Both']

FNCC_BUS_FREE = 0
FNCC_BUS_LOCKED = 1
FNCC_LOCK_STRING = ["connected", "isolated"]
