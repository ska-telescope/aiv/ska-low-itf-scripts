from FNPCFuncs import *
from FNSCFuncs import *
from OpenComms import *

VERSION = "1.1"

"""
Set FEMs in a smartbox so that odd ones are on, even ones are off
Pre-req: Power from PDOC, Sbox initialised
Suggest: FNPCSetStatus, FNDHPDoCON --PDoC=1, SBSetStatus
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    DoAlternateFEMSonoff(target, SBAddress)
    
    target.close()
    
if __name__ == "__main__":
    main()
