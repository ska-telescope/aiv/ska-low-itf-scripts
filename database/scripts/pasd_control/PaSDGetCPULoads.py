import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1."

def main():
    (target, SBAddress, PDoCPort, FEMPorts) = OpenComms()

    print("\nFNCC:     ", end='')
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, CPU_LOAD), CONV_TYPE_UINT16, '%')

    print("FNPC:     ", end='')
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, CPU_LOAD), CONV_TYPE_UINT16, '%')

    for i in range(PASD_NUM_SMARTBOXES): 
        print("FNSC(%02d)" % (i+1), end=': ')
        SendMessage(target, ModBusReadRegisters(i+1, CPU_LOAD), CONV_TYPE_UINT16, '%')

    target.close()
    
if __name__ == "__main__":
    main()
