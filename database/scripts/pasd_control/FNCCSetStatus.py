import sys
import time
import argparse
import serial
from ModBusSupport import *
from FNCCConsts import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    print("\nFNCC Set SYS_STATUS to STATUS_OK")
    SendMessage(target, ModBusWriteRegister(FNCC_ADDR, FNCC_SYS_STATUS, FNCC_SYS_STATE_OK))
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_SYS_STATUS), CONV_TYPE_STATUS)

    target.close()
    
if __name__ == "__main__":
    main()
