import sys
import time
import argparse
import serial
from FNSCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.2"

"""
(Firmware) Functional Test FT-23015
MCCS Read SMART Box live sensor readings
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Tests:" % (SBAddress))
    print("-----------------")
    print("Read Incoming 48Vdc voltage")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_48V), CONV_TYPE_VOLTAGE)

    print("\nRead PSU Output voltage")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_PSU_V), CONV_TYPE_VOLTAGE)

    print("\nRead PSU temperature")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_PSUTEMP), CONV_TYPE_TEMP)

    print("\nRead FEM Package temperature - Sensor board")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_AMBTEMP), CONV_TYPE_TEMP)

    print("\nRead FNSC SYS_STATUS")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nRead FNSC System LEDs")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))

    print("\nRead FEM Package temperature - FEM#6")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE01), CONV_TYPE_TEMP)

    print("\nRead FEM Package temperature - FEM#12")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE02), CONV_TYPE_TEMP)

    print("\nRead FEM Package temperature - Heatsink#7-12")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE03), CONV_TYPE_TEMP)

    print("\nRead FEM Package temperature - Heatsink#1-6")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE04), CONV_TYPE_TEMP)

    print("\nRead FEM currents")
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)

    target.close()

if __name__ == "__main__":
    main()
