import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23027
Read First 13 Polling registers from FNCC firmware

"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNCC Read FieldNode Number")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_FIELD_NODE_NUMBER), CONV_TYPE_UINT16, 'FN Number')

    target.close()

if __name__ == "__main__":
    main()

