import sys
import time
import argparse
import serial
from FNPCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.2"

"""
(Firmware) Functional Test FT-23014
MCCS Read FNPC live sensor readings
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNPC Tests:")
    print("-----------")
    print("Read 48Vdc COSEL SMPS output voltage")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_V), CONV_TYPE_VOLTAGE)

    print("\nRead 48Vdc COSEL SMPS output current")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V_I), CONV_TYPE_CURRENT)

    print("\nRead 48Vdc COSEL SMPS temperature at top of COSEL base plate")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_TEMP), CONV_TYPE_TEMP)

    print("\nRead 48Vdc COSEL SMPS temperature at bottom of COSEL base plate")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V2_TEMP), CONV_TYPE_TEMP)

    print("\nRead FNCB Temperature")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_FNCBTEMP), CONV_TYPE_TEMP)

    print("\nRead FNCB Humidity")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_HUMIDITY), CONV_TYPE_HUMIDITY)

    print("\nRead FNPC Status")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nRead FNPC System LEDs")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_LIGHTS))

    print("\nRead FNDH EP temperature - external surface of CG")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE01), CONV_TYPE_TEMP)

    print("\nRead FNDH EP temperature - external surface of PM")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE02), CONV_TYPE_TEMP)

    print("\nRead FNDH EP temperature - \"floor\" of enclosure")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE03), CONV_TYPE_TEMP)

    print("\nRead FNDH EP temperature - \"roof\" of enclosure")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_SENSE04), CONV_TYPE_TEMP)

    target.close()

if __name__ == "__main__":
    main()
