import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23011 part 1
Set FEMs with DSON to on, and DSOFF to off, so we can see it go offline
User must wait five minutes to observe all FEMs turn off
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Set FEMs to turn on when Online and OFF when Offline" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ONOFF, FNSC_NUM_FEMS))

    target.close()
    
if __name__ == "__main__":
    main()
