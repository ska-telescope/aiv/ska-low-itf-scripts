import sys
import time
import argparse
import serial
from FNSCConsts import *
from FNPCConsts import *
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23025
Read Field Node Number register from FNCC, then 
Sequence of ops to verify that stuck bus detection register can be read (and maybe even return a glitch)
and that lockin the bus stops all SMART Box comms in both directions, but does not stop FNPC/FNCC comms.
Assumes SMART Box #01 is on PDoC #01, initial shared bus state is connected (unlocked)

FT23025 is pretty easy now, since the functionality of “Detect a shared bus problem” and “isolate the shared bus” have been separated in the FNCC firmware. We can just: 
Issue a SBox#01 STATUS register read, see it work
Issue a FNPC STATUS register read, see it work
Issue an “isolate-the-bus” command to the FNCC
Try reading the SBox STATUS reg again, see it fail
Try reading the FNPC STATUS reg again, and see it still work
Issue a “restore the bus” command to the FNCC
Repeat the register reads and see that the SBox is visible again.

For each of the above steps we also query and return the FNCC_SYS_STATUS register value which will indicate 0 if there are no bus errors, or 1,2 or 3 if there are either UART Framing errors, the bus is jammed low, or both! If we detect a non-zero return, we can try clearing the detections, by writing ZERO (OK) to the FNCC SYS_STATUS register.

We can possibly also actually tie the shared line low in hardware and see it detect that. Let me think about that one though.

"""

POLLING_DELAY_SECONDS = 1
STATE_OK_DELAY = 0.5
SBOX_BOOT_DELAY = 1.5


def printFNPCStatusExpectGood(theList):
    if (theList[0] != 1):
        print("FNPC Status read failed - test aborted")
        return(-1)
    FNPCStatString = FNPC_STATE_STRINGS[theList[1]]
    if (theList[1] > FNPC_SYS_STATE_WARNING):
        print("FNPC Status %s not suitable - test aborted" % (FNPCStatString))
        return(-2)
    print("FNPC Received SYS_STATUS as %s" % (FNPCStatString))
    return(0)

def printFNSCStatusExpectGood(SBAddr, theList):
    if (theList[0] != 1):
        print("FNSC(%02d) Status read failed - test aborted" % (SBAddr))
        return(-3)
    FNSCStatString = FNSC_STATE_STRINGS[theList[1]]
    if (theList[1] > FNSC_SYS_STATE_WARNING):
        print("FNSC(%02d) Status %s not suitable - test aborted" % (SBAddr, FNSCStatString))
        return(-4)
    print("FNSC(%02d) Received SYS_STATUS as %s" % (SBAddr, FNSCStatString))
    return(0)

def printFNSCStatusExpectLocked(SBAddr, theList):
    if (theList[0] != 1):
        print("FNSC(%02d) Status read failed - as expected" % (SBAddr))
        return(0)
    FNSCStatString = FNSC_STATE_STRINGS[theList[1]]
    print("FNSC(%02d) Got Status %s but bus should be isolated - test aborted" % (SBAddr, FNSCStatString))
    return(-6)

def FNCCReadAndClearStatus(conn):
    print("\nFNCC Read status", end=': ')
    SendMessage(conn, ModBusReadRegisters(FNCC_ADDR, FNCC_SYS_STATUS), CONV_TYPE_STATUS)
    print("FNCC Set STATUS_OK", end=': ')
    SendMessage(conn, ModBusWriteRegister(FNCC_ADDR, FNCC_SYS_STATUS, FNCC_SYS_STATE_OK))


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("Assumes shared bus is currently connected/unlocked\nFNCC Clear historical detected comms errors", end=': ')
    SendMessage(target, ModBusWriteRegister(FNCC_ADDR, FNCC_SYS_STATUS, FNCC_SYS_STATE_OK))

    print("\nFNCC Read FIELD NODE NUMBER (four-digit switch setting)")
    replyList = SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_FIELD_NODE_NUMBER))
    if (replyList[0] != 1):
        print(" Error reading Field Node number register")
        return(-7)
    print(" Field node number is %d" % (replyList[1]))

    print("\nQuick init FNPC and turn on PDOC %02d" % (PDoCPort))
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + (PDoCPort - 1), FNPC_PDOC_ON))
    FNCCReadAndClearStatus(target)
        
    print("\nFNPC Read Status, confirm it worked", end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)
    rv = printFNPCStatusExpectGood(replyList)
    if (rv != 0):
        return rv
    
    print("\n... brief wait then quick-init SMART Box %02d and turn on FEMs" % (SBAddress))
    time.sleep(SBOX_BOOT_DELAY)
    SendMessage(target, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ON, FNSC_NUM_FEMS))
    FNCCReadAndClearStatus(target)
        
    print("\nFNSC(%02d) Read Status, confirm it worked" % (SBAddress), end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
    rv = printFNSCStatusExpectGood(SBAddress, replyList)
    if (rv != 0):
        return rv
    
    FNCCReadAndClearStatus(target)

    print("\nFNCC Issue \"Block SMART Box comms\" command", end=': ')
    SendMessage(target, ModBusWriteRegister(FNCC_ADDR, FNCC_BUS_CONTROL, FNCC_BUS_LOCKED))
    print("FNCC Check SMART Box Bus Lock control register", end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_BUS_CONTROL))
    if (replyList[0] != 1) or (replyList[1] != FNCC_BUS_FREE and replyList[1] != FNCC_BUS_LOCKED):
        print(" Error reading Bus Lock control register")
        return(-1)
    print(" SMART Box shared comms Bus is %s" % (FNCC_LOCK_STRING[replyList[1]]))
    
    FNCCReadAndClearStatus(target)

    print("\nFNPC Read status, confirm it worked", end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)
    rv = printFNPCStatusExpectGood(replyList)
    if (rv != 0):
        return rv
    
    FNCCReadAndClearStatus(target)

    print("\nFNSC(%02d) Read Status, expect it to fail" % (SBAddress), end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
    rv = printFNSCStatusExpectLocked(SBAddress, replyList)
    if (rv != 0):
        return rv
    
    FNCCReadAndClearStatus(target)

    print("\nFNCC Issue \"Allow SMART Box comms\" command", end=': ')
    SendMessage(target, ModBusWriteRegister(FNCC_ADDR, FNCC_BUS_CONTROL, FNCC_BUS_FREE))
    print("FNCC Check SMART Box Bus Lock control register", end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_BUS_CONTROL))
    if (replyList[0] != 1) or (replyList[1] != FNCC_BUS_FREE and replyList[1] != FNCC_BUS_LOCKED):
        print(" Error reading Bus Lock control register")
        return(-1)
    print(" SMART Box shared comms Bus is %s" % (FNCC_LOCK_STRING[replyList[1]]))

    FNCCReadAndClearStatus(target)

    print("\nFNPC Read status, confirm it worked", end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)
    rv = printFNPCStatusExpectGood(replyList)
    if (rv != 0):
        return rv

    FNCCReadAndClearStatus(target)
    
    print("\nFNSC(%02d) Read Status, confirm it worked" % (SBAddress), end=': ')
    replyList = SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)
    rv = printFNSCStatusExpectGood(SBAddress, replyList)
    if (rv != 0):
        return rv

    FNCCReadAndClearStatus(target)

    target.close()

if __name__ == "__main__":
    main()

