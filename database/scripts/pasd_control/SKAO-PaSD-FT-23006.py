import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.4"

"""
(Firmware) Functional Test FT-23006
MCCS Read warnings/alarms, then try to set STATUS_OK, then turn on PDoCs via DSON/DSOFF
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    FNPCGetALWarnBits(target)

    print("\nFNPC Try to set SYS_STATUS to STATUS_OK")
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_SYS_STATUS, FNPC_SYS_STATE_OK))
    print("FNPC Read SYS_STATUS")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_STATUS), CONV_TYPE_STATUS)

    print("\nFNPC Turn PDoCs ON")
    for i in range(FNPC_NUM_PDOCS):
        SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + i, FNPC_PDOC_ON))
        time.sleep(0.1)

    print("\nFNPC Read PDoC states")
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE, FNPC_NUM_PDOCS))
    
    target.close()
    
if __name__ == "__main__":
    main()
