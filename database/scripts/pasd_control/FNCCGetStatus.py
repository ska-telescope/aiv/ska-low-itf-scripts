import sys
import time
import argparse
import serial
from ModBusSupport import *
from FNCCConsts import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    print("\nFNCC Get Status")
    SendMessage(target, ModBusReadRegisters(FNCC_ADDR, FNCC_SYS_STATUS), CONV_TYPE_STATUS)

    target.close()
    
if __name__ == "__main__":
    main()
