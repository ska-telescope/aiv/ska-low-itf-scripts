import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
Set PDoCs with DSON to on, and DSOFF to off, so we can see it go offline
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNDH set all PDoCs to OFF")
    SendMessage(target, ModBusWriteSameValues(FNPC_ADDR, FNPC_P01_STATE, FNPC_PDOC_OFF, FNPC_NUM_PDOCS))

    target.close()
    
if __name__ == "__main__":
    main()
