from ModBusSupport import *
from FNSCConsts import *
import time

FNSC_FUNCS_VER = "1.5"

def DoWalkingFEMs(conn, SBAddress=FNSC_01_ADDR, ToggleDelay=FEM_TOGGLE_DELAY, WalkStepDelay=WALKING_FEMS_DELAY):

    """ Send messages to control FEM states
    """

    SBOFFMessage = ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_OFF, FNSC_NUM_FEMS)
    
    print("FNSC(%02d) All FEMS off" % (SBAddress))
    SendMessage(conn,SBOFFMessage)
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
    time.sleep(ToggleDelay) # wait two seconds
    
    print("FNSC(%02d) All FEMS on" % (SBAddress))
    SendMessage(conn, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ON, FNSC_NUM_FEMS)) # turn on all FEMs
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
    time.sleep(ToggleDelay)
    
    print("FNSC(%02d) All FEMS off" % (SBAddress))
    SendMessage(conn,SBOFFMessage) # turn'em off again, reuse existing message
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
    time.sleep(ToggleDelay) # wait two seconds

    print("FNSC(%02d) Walking ones" % (SBAddress))
    # turn them all on, one at a time
    for i in (range(FNSC_NUM_FEMS)): # runs i from 0..11, so just add to P01_STATE
        SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_P01_STATE + i, FNSC_FEM_ON)) # turn on the given FEM
        SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
        time.sleep(WalkStepDelay)
      
    print("FNSC(%02d) Walking zeroes" % (SBAddress))
    # turn them all off, one at a time
    for i in (range(FNSC_NUM_FEMS)): # runs i from 0..11
        SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_P01_STATE + i, FNSC_FEM_OFF)) # turn off the given FEM
        SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
        time.sleep(WalkStepDelay)

def DoAlternateFEMSonoff(conn, SBnum=FNSC_01_ADDR):
    SBAddress = FNSC_01_ADDR + SBnum - 1    

    """ Send messages to control FEM states
    """

    print("FNSC(%02d): Odd FEMs ON, Even FEMs Off" % (SBAddress))
    SendMessage(conn, ModBusWriteMultiPortStateLists(SBAddress, FNSC_P01_STATE, [FNSC_FEM_ON, FNSC_FEM_OFF], FNSC_NUM_FEMS >> 1))
    time.sleep(0.2)
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))


# Show thresholds in SMART Box
def FNSCGetThresholds(conn, SBnum=FNSC_01_ADDR):
    SBAddress = FNSC_01_ADDR + SBnum - 1
    
    # Create PSU Voltage threshold messages and send them, one to the 48V registers and one to the 5V registers, each call is ONE threshold value list, hence the final argument
    print("\nFNSC(%02d) read 48VIn" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_48V), CONV_TYPE_VOLTAGE)
    print("FNSC(%02d) Get 48V thresholds" % (SBAddress))
    SendMessage(conn, ModBusReadThresholds(SBAddress, FNSC_SYS_48V_V_TH), CONV_TYPE_VOLTAGE) # read them back.

    print("\nFNSC(%02d) read PSU Out" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_PSU_V), CONV_TYPE_VOLTAGE)
    print("FNSC(%02d) Get PSU Out thresholds" % (SBAddress))
    SendMessage(conn, ModBusReadThresholds(SBAddress, FNSC_SYS_PSU_V_TH, CONV_TYPE_VOLTAGE)) # read them back.

    # Create and send SB EP Temperature thresholds, two sets of the same thresholds to two consectutive register sets
    print("\nFNSC(%02d) Read PSU Temp" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_PSUTEMP), CONV_TYPE_TEMP)
    print("FNSC(%02d) Get PSU/PCB temp limits" % (SBAddress))
    SendMessage(conn, ModBusReadThresholds(SBAddress, FNSC_SYS_EP_TEMPS_TH, FNSC_NUM_EPTEMPS), CONV_TYPE_TEMP) # confirmatory read.

    print("\nFNSC(%02d) Read AMB Temp and Four Temps, all in FEM Package enclosure" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_AMBTEMP, FNSC_NUM_AMBTEMPS), CONV_TYPE_TEMP)
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE01, FNSC_NUM_FEMPTEMPS), CONV_TYPE_TEMP)
    print("FNSC(%02d) Get Ambient and Four FEM Package temp limts" % (SBAddress))
    SendMessage(conn, ModBusReadThresholds(SBAddress, FNSC_SYS_AMBTEMP_TH, FNSC_NUM_FEMPTEMPS), CONV_TYPE_TEMP) # confirmatory read.

    print("\nFNSC(%02d) Check empty thresholds from %d for %d register sets" % (SBAddress, FNSC_SYS_EMPTY_TH, FNSC_NUM_EMPTIES))
    SendMessage(conn, ModBusReadThresholds(FNSC_01_ADDR, FNSC_SYS_EMPTY_TH, FNSC_NUM_AMBTEMPS + FNSC_NUM_EMPTIES)) # confirmatory read.

    # These limits are OK coz currents are always zero or positive
    # Write 12 (FNSC_NUM_FEMS) registers starting at FNSC_P01_CURRENT_TH with the 300mA (FEM_MAX_CURRENT)
    print("\nFNSC(%02d) Read 12 FEM Currents" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
    print("FNSC(%02d) Get current limits in mA for %d FEMs" % (SBAddress, FNSC_NUM_FEMS))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA) # confirmatory read.

def FNSCGetLiveValues(conn, SBnum=FNSC_01_ADDR):
    SBAddress = FNSC_01_ADDR + SBnum - 1
    
    print("\nFNSC(%02d) read 48VIn" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_48V), CONV_TYPE_VOLTAGE)

    print("\nFNSC(%02d) read PSU Out" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_PSU_V), CONV_TYPE_VOLTAGE)

    print("\nFNSC(%02d) Read PSU Temp" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_PSUTEMP), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Read AMB Temp and Four Temps, all in FEM Package enclosure" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_AMBTEMP, FNSC_NUM_AMBTEMPS), CONV_TYPE_TEMP)
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_SENSE01, FNSC_NUM_FEMPTEMPS), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Read 12 FEM Currents" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)


def ReadAllFEMPorts(conn, SBnum):    
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("FNSC(%02d) Read all FEM port states" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_P01_STATE, FNSC_NUM_FEMS))
    print("\nFNSC(%02d) Read 12 FEM Currents" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT, FNSC_NUM_FEMS), CONV_TYPE_FEM_MA)
    

def FNSCGetALWarnBits(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) Get Warnings LSW" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, WARNINGS_LSW), CONV_TYPE_ALWARN)
    print("FNSC(%02d) Get Alarms LSW" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, ALARMS_LSW), CONV_TYPE_ALWARN)

def FNSCClearALWarnBits(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) Try to clear Alarms/Warnings" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, ALARMS_LSW, 0))
    SendMessage(conn, ModBusWriteRegister(SBAddress, WARNINGS_LSW, 0))

def ClearAllCBs(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) Write 0x80 to all FEM port states to clear Circuit Breaker bits" % (SBAddress))
    SendMessage(conn, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, 0x80, FNSC_NUM_FEMS))

def ServiceOnSBox(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) Turn on service LED" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_ON))
    

def ServiceOffSBox(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) turn off service LED" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_OFF))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))
    
def StartSmartBox(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) Try to Set STATUS to OK" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_STATUS, FNSC_SYS_STATE_OK))
    print("FNSC(%02d) Get status" % (SBAddress))
    SendMessage(conn, ModBusReadRegisters(SBAddress, FNSC_SYS_STATUS), CONV_TYPE_STATUS)

def FNSCSysLights(conn, SBnum):
    SBAddress = FNSC_01_ADDR + SBnum - 1

    print("\nFNSC(%02d) Show current LED state" % (SBAddress))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))

    print("FNSC(%02d) Service LED on" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_ON))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))
    time.sleep(3)

    print("FNSC(%02d) Service LED 5Hz flash" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_5HZ))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))
    time.sleep(3)

    print("FNSC(%02d) Service LED 2.5Hz flash" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_2HZ5))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))
    time.sleep(3)

    print("FNSC(%02d) Service LED 1.25Hz flash" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_1HZ25))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))
    time.sleep(3)

    print("FNSC(%02d) Service LED 0.625Hz flash" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_0HZ625))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))
    time.sleep(3)

    print("FNSC(%02d) Service LED off" % (SBAddress))
    SendMessage(conn, ModBusWriteRegister(SBAddress, FNSC_SYS_LIGHTS, SERVICE_LED_OFF))
    SendMessage(conn,ModBusReadRegisters(SBAddress, FNSC_SYS_LIGHTS))

    
