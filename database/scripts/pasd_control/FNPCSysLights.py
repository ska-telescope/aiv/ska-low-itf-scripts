import sys
import time
import argparse
import serial
from OpenComms import *
from ModBusSupport import *
from FNPCFuncs import *

VERSION = "1.1"


def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    FNPCSysLights(target)

    target.close()
    
if __name__ == "__main__":
    main()
