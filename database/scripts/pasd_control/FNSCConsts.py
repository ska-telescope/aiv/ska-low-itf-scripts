FNSC_CONSTS_VER = "1.6"

# All in seconds - default timing for toggling/walking if you don't change it.
FEM_TOGGLE_DELAY = 1.5
WALKING_FEMS_DELAY = 1

# ModBus slave addresses in decimal
FNSC_01_ADDR = 1

FNSC_NUM_FEMS = 12 # nice and easy to change if we need.

# SMARTBox specific polling registers
FNSC_SYS_48V = 17 # incoming 48V voltage
FNSC_SYS_PSU_V= 18 # low voltage supply output
FNSC_SYS_PSUTEMP = 19 # doubles with PCBTEMP (Reg 20)
FNSC_SYS_PCBTEMP = 20 # copy of PSUTEMP
FNSC_SYS_AMBTEMP = 21 # Temperature on the Sensor PCB in FEM package.
FNSC_SYS_STATUS = 22 # Status register
FNSC_SYS_LIGHTS = 23 # Blue and Multi-colour lights
FNSC_SYS_SENSE01 = 24 # Four thermistors inside FEMP on FEMs and Heatsinks
FNSC_SYS_SENSE02 = 25
FNSC_SYS_SENSE03 = 26
FNSC_SYS_SENSE04 = 27
FNSC_SYS_SENSE05 = 28
FNSC_P01_STATE = 36 # register number of first FEM State register
FNSC_P01_CURRENT = 48 # register number of first FEM current register

# SMART Box Config registers
FNSC_SYS_48V_V_TH = 1001 # Voltage threshold registers - 48V first
FNSC_SYS_PSU_V_TH = 1005 # "5V" second
FNSC_SYS_PSUTEMP_TH = 1009 # Temperature threshold registers - SB EP Psu board temp, and copied to PCB temp.
FNSC_SYS_PCB_TEMP_TH = 1013
FNSC_SYS_EP_TEMPS_TH = FNSC_SYS_PSUTEMP_TH # Temp thresholds inside the SBox EP, only really one, but appears twice
FNSC_SYS_AMBTEMP_TH = 1017 # Thermistor mounted on sensor PCB in FEMP
FNSC_SYS_SENSE01_TH = 1021 # First of four thermistors mounted on FEMs and Heatsinks
FNSC_SYS_FEMPTEMPS_TH = FNSC_SYS_AMBTEMP_TH # Five sets of temp reading limits inside FEM package
FNSC_SYS_EMPTY_TH = 1037 # SENSE_05 onwards
FNSC_P01_CURRENT_TH = 1069 # register number of first FEM Current threshold register

FNSC_NUM_CONFIG_REGSETS = 17
FNSC_NUM_CONFIG_REGS = FNSC_NUM_CONFIG_REGSETS * 4
FNSC_NUM_CURRENT_THRESHOLDS = FNSC_NUM_FEMS

"""ModBus register counts in decimal"""
FNSC_NUM_EPTEMPS = 2
FNSC_NUM_AMBTEMPS = 1
FNSC_NUM_FEMPTEMPS = 4
FNSC_NUM_EMPTIES = 8

""" Thresholds defined for various things, in raw values, because they will be multiplied before application"""
# Special upper-limits-only values for FEMs in SMARTBoxes which don't have the ALARM/WARN feature but circuit breaker instead.
FEM_MAX_CURRENT = 495 # mA  This is enough for a FEM without an antenna, but probably needs to be higher in the field.

# SMARTBox voltage warning thresholds in volts, should be in the firwmare automatically now
FNSC_48V_LIMITS = [50, 49, 45, 40] # Volts
FNSC_5V_LIMITS = [5, 4.9, 4.4, 4.0]  # Volts

#thermistor temp alarm values in whole degrees C, also supposed to be in firmware
FNSC_EP_TEMP_LIMITS = [85, 70, 0, -5] # Degrees C
FNSC_FEMP_TEMP_LIMITS = [60, 45, 0, -5] # Degrees C

FNSC_FEM_OFF = 0x2800 # Value to write into Pxx_STATE register to turn a FEM off for both online and offline modes
FNSC_FEM_ON = 0x3C00 # Value to write into Pxx_STATE register to turn a FEM on for both online and offline modes
FNSC_FEM_ONOFF = 0x3800 # Value to write into Pxx_STATE register to turn a FEM on for online, and OFF for offline mode
FNSC_FEM_OFFON = 0x2C00 # Value to write into Pxx_STATE register to turn a FEM OFF for online, and ON for offline modes!

# Write this to a FEM port to attempt to reset the software circuit breaker
FNSC_CB_RESET = 0x0080

FNSC_SYS_STATE_OK = 0
FNSC_SYS_STATE_WARNING = 1
FNSC_SYS_STATE_ALARM = 2
FNSC_SYS_STATE_RECOVERY = 3
FNSC_SYS_STATE_UNINITIALISED = 4
FNSC_SYS_STATE_POWERDOWN = 5

# Long status strings for mainline output, and short strings for in-line output
FNSC_STATE_STRINGS = ["STATE_OK", "STATE_WARNING",  "STATE_ALARM", "STATE_RECOVERY", "STATE_UNINIT", "STATE_POWERDOWN"]
FNSC_CONV_STATUS = ['OK', 'Warn', 'ALARM!', 'Recovery', 'UnInit', 'PWRdn']

# Alarm & warning flag bits
FNSC_AW_MAXFLAGS = 11
FNSC_AW_48IN = 0x0001
FNSC_AW_PSUV = 0x0002
FNSC_AW_PSUT = 0x0004
FNSC_AW_PCBT = 0x0008
FNSC_AW_AMBT = 0x0010
FNSC_AW_SN01 = 0x0020
FNSC_AW_SN02 = 0x0040
FNSC_AW_SN03 = 0x0080
FNSC_AW_SN04 = 0x0100
FNSC_AW_SN05 = 0x0200
FNSC_AW_SN06 = 0x0400

FNSC_AW_STRINGS = ['48IN', 'PSUV', 'PSUT', 'PCBT', 'AMBT', 'SN01', 'SN02', 'SN03', 'SN04', 'SN05','SN06']

