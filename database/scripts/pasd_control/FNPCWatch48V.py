import sys
import time
import argparse
import serial
from FNPCFuncs import *
from OpenComms import *

VERSION = "1.2"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()
    
    ServiceOnFNDH(target)

    print("\nFNPC Read 48V")
    while(True):
        print("---")
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V1_V), CONV_TYPE_VOLTAGE)
        SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_SYS_48V_I), CONV_TYPE_CURRENT)
        time.sleep(0.1)
        
    ServiceOffFNDH(target)

    target.close()
    
if __name__ == "__main__":
    main()
