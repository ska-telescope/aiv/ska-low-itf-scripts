import sys
import time
import argparse
import serial
from ModBusSupport import *
from FNPCConsts import *
from OpenComms import *

VERSION = "1.1"

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("\nFNPC Powerdown PDoC %02d via TO bits" % (PDoCPort))
    SendMessage(target, ModBusWriteRegister(FNPC_ADDR, FNPC_P01_STATE + PDoCPort - 1, FNPC_PDOC_POWERDOWN))
    SendMessage(target, ModBusReadRegisters(FNPC_ADDR, FNPC_P01_STATE))

    target.close()
    
if __name__ == "__main__":
    main()
