import sys
import time
import argparse
import serial
from ModBusSupport import *
from OpenComms import *

VERSION = "1.1"

"""
(Firmware) Functional Test FT-23013
MCCS Updates SMART Box Configuration registers (write zero to demonstrate clearing them all)
    Then re-write sensible / default values and re-confirm
"""

def main():

    (target, SBAddress, PDoCPort, FEMPort) = OpenComms()

    print("FNSC(%02d) Restore DSON/DSOFF after previous tests" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_STATE, FNSC_FEM_ON, FNSC_NUM_FEMS))

    print("\nFNSC(%02d) Set all configuration registers to zero, clears thresholds => sensors don't generate warnings or alarms" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_SYS_48V_V_TH, 0, FNSC_NUM_CONFIG_REGS))
    print("FNSC(%02d) Confirm update Configuration Registers" % (SBAddress))
    for i in range(FNSC_NUM_CONFIG_REGSETS):
        SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_SYS_48V_V_TH + (i * 4), 4))

    print("\nFNSC(%02d) Set all FEM Current limits to 300mA, different from defaults" % (SBAddress))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_CURRENT_TH, 300, FNSC_NUM_CURRENT_THRESHOLDS))
    print("FNSC(%02d) Confirm updated current limits" % (SBAddress))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_CURRENT_THRESHOLDS), CONV_TYPE_FEM_MA)

    print("\nFNSC(%02d) Restore sensible thresholds into Configuration Registers")
    print("FNSC(%02d) Write and confirm 48V voltage limits as %s" % (SBAddress, FNSC_48V_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(SBAddress, FNSC_SYS_48V_V_TH, FNSC_48V_LIMITS, VOLT_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_48V_V_TH), CONV_TYPE_VOLTAGE)

    print("\nFNSC(%02d) Write and confirm PSU output voltage limits as %s" % (SBAddress, FNSC_5V_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(SBAddress, FNSC_SYS_PSU_V_TH, FNSC_5V_LIMITS, VOLT_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_PSU_V_TH), CONV_TYPE_VOLTAGE)

    print("\nFNSC(%02d) Write and confirm PSU Temperature limits as %s" % (SBAddress, FNSC_EP_TEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(SBAddress, FNSC_SYS_PSUTEMP_TH, FNSC_EP_TEMP_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_PSUTEMP_TH), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Write and confirm FEMP Ambient Temperature limits as %s" % (SBAddress, FNSC_FEMP_TEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(SBAddress, FNSC_SYS_AMBTEMP_TH, FNSC_FEMP_TEMP_LIMITS, TEMP_MULTIPLIER))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_AMBTEMP_TH), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Write and confirm FEM / Heatsink Temperature limits as %s" % (SBAddress, FNSC_FEMP_TEMP_LIMITS))
    SendMessage(target, ModBusWriteMultiValueLists(SBAddress, FNSC_SYS_SENSE01_TH, FNSC_FEMP_TEMP_LIMITS, TEMP_MULTIPLIER, 4))
    SendMessage(target, ModBusReadThresholds(SBAddress, FNSC_SYS_SENSE01_TH, 4), CONV_TYPE_TEMP)

    print("\nFNSC(%02d) Write and confirm FEM current limits as %smA" % (SBAddress, FEM_MAX_CURRENT))
    SendMessage(target, ModBusWriteSameValues(SBAddress, FNSC_P01_CURRENT_TH, FEM_MAX_CURRENT, FNSC_NUM_CURRENT_THRESHOLDS))
    SendMessage(target, ModBusReadRegisters(SBAddress, FNSC_P01_CURRENT_TH, FNSC_NUM_CURRENT_THRESHOLDS), CONV_TYPE_FEM_MA)
    
    
    target.close()
    
if __name__ == "__main__":
    main()
