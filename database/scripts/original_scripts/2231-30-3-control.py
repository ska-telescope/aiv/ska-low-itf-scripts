import pyvisa as visa #Use pyvisa library
import time

address = "ASRL/dev/ttyUSB0::INSTR" #Raspberry Pi USB Serial Port ASRL String

rm = visa.ResourceManager('@py') #Use the pyvisa-py backend
inst = rm.open_resource(address)

inst.write("*RST")
print(inst.query("*IDN?"))

## Enable device remote control.
inst.write("SYST:REM")

## Set device Channel 1 to 28.0 V and turn it on.
inst.write("OUTP 0")
inst.write("INST CH1")
inst.write("VOLT 28")
inst.write("CHAN:OUTP 1")

inst.close()
rm.close()
