# -*- coding: utf-8 -*-
"""
@author: Lahiru.Raffel

Test instruments need to be pre-configured to run this script correctly
"""

import pyvisa as visa
import numpy as np
import time
import socket
import csv

AWG_local_port = 5025
AWG_device_ip = "10.135.151.42"

RSA_local_port = 5025
RSA_device_ip = "10.135.151.41"

RC4DAT_device_ip = "10.135.151.34"
RC4DAT_device_port = 23

frequency_list = [50e6,80e6,110e6,160e6,220e6,280e6,340e6,350e6]
#frequency_list = [50e6,80e6]
AWG_power_list = [10,0,-10]
#AWG_power_list = [0]
RC4DAT_list = [0,10,20,30,40]
#RC4DAT_list = [0,10]
channel = 2
sample_rate = 2500000000

## Define path, open .csv file and define columns
PATH = 'C:\\Users\\Lahiru.Raffel\\Dropbox (SKAO)\\system ITF\\Requirements and Test Cases sITF\\LOW\\TE\\5 - TE VE - Analog chain IE\\0 - Test Cases\TCLITF112111\\Ch2_RF_AN_ps.csv'
caldata = open(PATH,'w')
caldata.write(str(time.time()))
caldata.write('\n')
caldata.write('AWG setting dBm,AWG Frequency Hz,RC4DAT setting dB,RSA measurement dBm\n')

# Change this to connect to your AWG and RSA as needed
"""#################SEARCH/CONNECT#################"""
rm = visa.ResourceManager()
print(rm.list_resources())
awg = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (AWG_device_ip, AWG_local_port))
rsa = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (RSA_device_ip, RSA_local_port))

awg.timeout = 25000
awg.encoding = 'latin_1'
awg.write_termination = '\n'
awg.read_termination = '\n'

rsa.timeout = 25000
rsa.encoding = 'latin_1'
rsa.write_termination = '\n'
rsa.read_termination = '\n'


print(awg.query('*idn?'))
##awg.write('*rst')
##awg.write('*cls')

print(rsa.query('*idn?'))


# Connect to the RC4DAT

rc4dat = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

rc4dat.connect((RC4DAT_device_ip, RC4DAT_device_port))
rc4dat.settimeout(1)
received_data = rc4dat.recv(1024)

command = "MN?\n"
print("-> " + command, end="")
data = rc4dat.send(command.encode("utf-8"))
        
received_data = rc4dat.recv(1024)
print((received_data).decode("utf-8"), end="")


## Set up the AWG.
awg.write('clock:source efixed')
awg.write('clock:srate %i' % sample_rate)
#awg.write('fgen:channel1:type sine')
awg.write('fgen:channel2:type sine')

def send_command_rc4dat(command):
    rc4dat = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    rc4dat.connect((RC4DAT_device_ip, RC4DAT_device_port))
    rc4dat.settimeout(1)
    data = rc4dat.send(command.encode("utf-8"))
    rc4dat.close()

def send_awg_command():
    
    return

def send_rsa_command():
    return

for ap in AWG_power_list:
    
    #awg.write('fgen:channel1:amplitude:power %i' % ap)
    awg.write('fgen:channel2:amplitude:power %i' % ap)

    
    for f in frequency_list:
        #awg.write('fgen:channel1:frequency %i' % f)
        awg.write('fgen:channel2:frequency %i' % f)
        rsa.write('spec:freq:cent %i' % f)
        
        time.sleep(15)
        
        
        for att in RC4DAT_list:
            command = ":SetAttPerChan:%s:%s\n" % (channel, att)
            data = rc4dat.send(command.encode("utf-8")) 
            #send_command_rc4dat(command)
          
            
            received_data = rc4dat.recv(1024)
            #awg.write('output1:state on')
            awg.write('output2:state on')
            #awg.write('awgcontrol:run:immediate')
            time.sleep(1)
            rsa.write('spec:cle:res')
            time.sleep(4)
            rsa.write('calc:spec:mark:max')
            time.sleep(2)
            meas = rsa.query('calc:spec:mark:y?')
            caldata.write('%i,%i,%i,%s\n' % (ap,f,att,meas))
            print('%i,%i,%i,%s\n' % (ap,f,att,meas))
            time.sleep(1)
            #awg.write('output1:state off')
            awg.write('output2:state off')
            
            
caldata.write(str(time.time()))
caldata.write('\n')
caldata.close()
awg.close()
rsa.close()
rc4dat.close()
    
