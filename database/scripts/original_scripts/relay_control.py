# -*- coding: utf-8 -*-
"""
Created on Mon Jan  9 11:32:30 2023

@author: Cameron.Mcdonald
"""

import sys
import time
import socket
import serial
import subprocess

################################################################################

## NCD packet format example:
## 0xAA 	NCD API Header Byte 	
## 0x03 	NCD API Byte Count (only payload bytes counted)
## 0xFE Payload Byte 1
## 0x6C Payload Byte 2
## 0x01 Payload Byte 3
## 0x18 NCD API Checksum (8-bit sum of bytes )

## Relay mapping
## R1 ON: [0xFE, 108, 0x01] - R1 OFF: [0xFE, 100, 0x01]
## R2 ON: [0xFE, 109, 0x01] - R2 OFF: [0xFE, 101, 0x01]
## R3 ON: [0xFE, 110, 0x01] - R3 OFF: [0xFE, 102, 0x01]
## R4 ON: [0xFE, 111, 0x01] - R4 OFF: [0xFE, 103, 0x01]
## R5 ON: [0xFE, 112, 0x01] - R5 OFF: [0xFE, 104, 0x01]
## R6 ON: [0xFE, 113, 0x01] - R6 OFF: [0xFE, 105, 0x01]
## R7 ON: [0xFE, 114, 0x01] - R7 OFF: [0xFE, 106, 0x01]
## R8 ON: [0xFE, 115, 0x01] - R8 OFF: [0xFE, 107, 0x01]

################################################################################

## Method of device control. Options are LAN and USB.
control = "LAN"

serial_port_name = "COM3"

## IP settings configured in the NCD board.
device_ip = "10.135.151.35" ## NCD board uses DHCP, IP fixed in router.
device_port = 5025

ssh_wrap = False

## Local IP settings dependent on whether SSH port forwarding is used.
if ssh_wrap is True:
    local_ip = "localhost"
    local_port = 1200
else:
    local_ip = device_ip
    local_port =  device_port
    

################################################################################

def create_ncd_packet(payload):

    ncd_api_header = 0xAA
    command_length = len(payload)
    
    packet = [ncd_api_header, command_length]
    packet += payload

    ## NCD API checksum is the 8-bit sum of bytes in the packet.
    checksum = 0
    
    for byte in packet:
        checksum += byte
        
    checksum = checksum % 256
    packet.append(checksum)
    
    return bytes(packet)

##################################### MAIN #####################################

## List of commands that will be sent to the NCD board.
payload_list = []

## Process the program call arguements.
if (len(sys.argv) < 2) or (len(sys.argv) > 9):
    print("Usage: python relay_control.py [control=LAN/USB] RX:A RY:B ...")
    print("X and Y is the relay number (1 - 8).")
    print("A and B is the relay state 0 (OFF) or 1 (ON).")
    sys.exit(1)

for argument in sys.argv[1:]:
    if argument[0:8] == "control=":
        if argument[8:] == "LAN":
            control = "LAN"
        elif argument[8:] == "USB":
            control = "USB"
        else:
            print("Usage: python relay_control.py [control=LAN/USB] RX:A RY:B ...")
            print("X and Y is the relay number (1 - 8).")
            print("A and B is the relay state 0 (OFF) or 1 (ON).")
            sys.exit(1)
    elif (len(argument) == 4) and (argument[0] == "R") and (argument[2] == ":"):
        try:
            relay_number = int(argument[1])
            relay_state = int(argument[3])
            
            code = 100 + (relay_number - 1) + (8 * relay_state)
            payload_list.append([0xFE, code, 0x01])
        except:
            print("Usage: python relay_control.py [control=LAN/USB] RX:A RY:B ...")
            print("X and Y is the relay number (1 - 8).")
            print("A and B is the relay state 0 (OFF) or 1 (ON).")
            sys.exit(1)
            
        if (relay_number < 1) or (relay_number > 8):
            print("Usage: python relay_control.py [control=LAN/USB] RX:A RY:B ...")
            print("X and Y is the relay number (1 - 8).")
            print("A and B is the relay state 0 (OFF) or 1 (ON).")
            sys.exit(1)
            
        if (relay_state != 0) and (relay_state != 1):
            print("Usage: python relay_control.py [control=LAN/USB] RX:A RY:B ...")
            print("X and Y is the relay number (1 - 8).")
            print("A and B is the relay state 0 (OFF) or 1 (ON).")
            sys.exit(1)
    else:
        print("Usage: python relay_control.py [control=LAN/USB] RX:A RY:B ...")
        print("X and Y is the relay number (1 - 8).")
        print("A and B is the relay state 0 (OFF) or 1 (ON).")
        sys.exit(1)

if control == "LAN":
    if ssh_wrap is True:
        ## Forward a test equipment port to a localhost port.
        ## The -N option is "Do not execute a remote command". Good for port forwarding.
        ssh_proc = subprocess.Popen("ssh -L %i:%s:%i sitf@skao-eoc.duckdns.org -p 8976 -i ./msf_pi_key -N" % (local_port, device_ip, device_port))
    
    ## Open a socket to the device and send the relay commands.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as interface:
        interface.connect((local_ip, local_port))
        interface.settimeout(1)
        
        for payload in payload_list:
            ## Generate the NCD API packets from the payload bytes.
            write_command = create_ncd_packet(payload)
        
            print("Sending Write Command: " + str(write_command))
            interface.send(write_command)
            time.sleep(0.1)
            response = interface.recv(1024)
            print(response.hex())
            
    if ssh_wrap is True:
        ## End the SSH process that is forwarding the port.
        ssh_proc.terminate()
else:
    with serial.Serial(serial_port_name, baudrate=115200, timeout=1) as serial_port:
        
        for payload in payload_list:
            write_command = create_ncd_packet(payload)
            
            print("Sending Write Command: " + str(write_command))
            serial_port.write(write_command)
            time.sleep(0.1)
            response = serial_port.readall()
            print(response.hex())
            
sys.exit(0)