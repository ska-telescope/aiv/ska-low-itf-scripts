# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for generating and playing AWG5208 waveforms.
"""

import sys
import socket
import numpy as np

device_port = 5025
device_ip = "10.135.150.3"

## Instrument settings.
##sample_rate = 5e9

wave_name_1 = "python_wave_1"

frequency_1 = 200e6
frequency_2 = 201e6

record_length = 16

period = 1 / frequency_1
sample_rate = 1 / (period / record_length)

## Solve for where the waveform crosses zero.
##record_length = sample_rate / (frequency_2 - frequency_1)

##print(sample_rate)
##sys.exit(0)

new_wave_data_1 = np.empty(record_length)

##noise_length = 5000

for i in range(0, record_length):
    new_wave_data_1[i] = np.sin(2 * np.pi * frequency_1 * (i / sample_rate))
    ##new_wave_data_1[i] += np.sin(2 * np.pi * frequency_2 * (i / sample_rate))
    ##new_wave_data_2[i] += 0.1 * (2 * np.random.random() - 1)
    
    ##awg.write('wlist:waveform:new "%s", %i' % (wave_name_1, record_length))

print(new_wave_data_1)

try:
    ## Open a socket connection and issue commands.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((device_ip, device_port))
        s.settimeout(10)
        
        ## Get the generator product number.
        command = "*IDN?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        received_data = s.recv(1024)
        print("<- " + (received_data).decode("utf-8"), end="")
        
        # ## Turn off the signal generator RF output.
        command = 'wlist:waveform:new "%s", %i\n' % (wave_name_1, record_length)
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        command = "*opc?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        received_data = s.recv(1024)
        print("<- " + (received_data).decode("utf-8"), end="")
        
        command = 'wlist:waveform:data "%s", 0, %i, ' % (wave_name_1, record_length)
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        for byte in new_wave_data_1:
            s.send(byte)
            
        command = "*opc?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        received_data = s.recv(1024)
        print("<- " + (received_data).decode("utf-8"), end="")
        
        # command = "clock:srate {}".format(sample_rate)
        # print("-> " + command, end="")
        # data = s.send(command.encode("utf-8"))
        
        # command = 'source1:casset:waveform "%s"' % wave_name_1
        # print("-> " + command, end="")
        # data = s.send(command.encode("utf-8"))
        
        # command = "awgcontrol:run:immediate"
        # print("-> " + command, end="")
        # data = s.send(command.encode("utf-8"))
        
        # command = "*opc?"
        # print("-> " + command, end="")
        # data = s.send(command.encode("utf-8"))
except Exception as error:
    print(str(error))

sys.exit(0)