# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the RC-2SP6T rack-mount switch set.
"""

import sys
##import subprocess
import socket

ssh_proc = None

device_ip = "10.135.151.37"
device_port = 23

local_ip = "localhost"
local_port = 1101

## Forward a test equipment port to a localhost port.
## The -N option is "Do not execute a remote command". Good for port forwarding.
##ssh_proc = subprocess.Popen("ssh -L %i:%s:%i sitf@skao-eoc.duckdns.org -p 8976 -i ./msf_pi_key -N" % (local_port, device_ip, device_port))

if (len(sys.argv) < 2) or (len(sys.argv) > 3):
    print("Usage: python3 sp6t_switch.py A# B#")
    print("Where # is the state, 0 to 6, of the corresponding switch.")
    sys.exit(1)

switch_states = sys.argv[1:]

a_count = 0
b_count = 0

## Bit of input parsing.
for item in switch_states:
    if len(item) != 2:
        print("Usage: python3 sp6t_switch.py A# B#")
        print("Where # is the state, 0 to 6, of the corresponding switch.")
        sys.exit(1)

    if (item[0] != "A") and (item[0] != "B"):
        print("Usage: python3 sp6t_switch.py A# B#")
        print("Where # is the state, 0 to 6, of the corresponding switch.")
        sys.exit(1)

    if (item[0] == "A"):
        a_count += 1

    if (item[0] == "B"):
        b_count += 1

  
    if (item[1] != "0") and (item[1] != "1") and (item[1] != "2") and (item[1] != "3") and (item[1] != "4") and (item[1] != "5") and (item[1] != "6"):
        print("Usage: python3 sp6t_switch.py A# B#")
        print("Where # is the state, 0 to 6, of the corresponding switch.")
        sys.exit(1)

if (a_count > 1) or (b_count > 1):
    print("Multiple states given for one switch.")
    sys.exit(2)

try:
    ## Open a socket connection and issue commands.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        ##s.connect(("localhost", local_port))
        s.connect((device_ip, device_port))
        s.settimeout(1)

        received_data = s.recv(1024)

        command = "MN?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        received_data = s.recv(1024)
        print((received_data).decode("utf-8"), end="")

        for item in switch_states:
            command = "SP6T%s:STATE:%s\n" % (item[0], item[1])
            print("-> " + command, end="")
            data = s.send(command.encode("utf-8"))
            received_data = s.recv(1024)
            print((received_data).decode("utf-8"), end="")
except Exception as e:
    print(str(e))

## End the SSH process that is forwarding the port.
##ssh_proc.terminate()

sys.exit(0)
