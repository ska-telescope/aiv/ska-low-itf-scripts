"""
VISA: SourceXpress/AWG Sequence Builder and Channel Skew Adjuster
Author: Morgan Allison
Date created: 5/17
Date edited: 5/17
Creates a sequence of two waveforms with an external trigger dependency
and configures the AWG to change its phase between waveform outputs
Windows 7 64-bit
Python 3.6.0 64-bit (Anaconda 4.3.0)
NumPy 1.11.2, PyVISA 1.8, PyVISA-py 0.2
Download Anaconda: http://continuum.io/downloads
Anaconda includes NumPy
"""

import pyvisa as visa
import numpy as np

local_port = 5025
device_ip = "10.135.150.42"

frequency = 349.9905424e6

## Use a fixed sample rate of 2.5 GHz.
sample_rate = 2500000000

# Change this to connect to your AWG as needed
rm = visa.ResourceManager()
print(rm.list_resources())
awg = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (device_ip, local_port))

awg.timeout = 25000
awg.encoding = 'latin_1'
awg.write_termination = '\n'
awg.read_termination = '\n'

print(awg.query('*idn?'))
##awg.write('*rst')
##awg.write('*cls')

## Set up the instrument.
awg.write('clock:source efixed')
awg.write('clock:srate %i' % sample_rate)

awg.write('fgen:channel1:type sine')
awg.write('fgen:channel1:frequency 349.9905424e6')
awg.write('fgen:channel1:amplitude:power 0')
awg.write('output1:state on')

awg.write('awgcontrol:run:immediate')
awg.query('*opc?')

print(awg.query('system:error:all?'))
awg.close()
