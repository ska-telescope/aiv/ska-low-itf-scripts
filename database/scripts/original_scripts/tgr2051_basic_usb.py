# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the 4SPDT rack-mount switch set.
"""

import pyvisa as visa #Use pyvisa library
#address = "ASRL/dev/ttyUSB0::INSTR" #Raspberry Pi USB Serial Port ASRL String
##address = "USB0::103E::04EC::::0::INSTR"
address = "ASRL9::INSTR"
rm = visa.ResourceManager() #Use the pyvisa-py backend
print(rm.list_resources())
inst = rm.open_resource(address)
print(inst.query("*IDN?\n"))

#inst.write(":OUTPut:STATe OFF\n")
#inst.write(":SOURce:FREQuency:MODE FIXed\n")
#inst.write(":UNIT:POWer DBM\n")

## Use external oscillator.
##inst.write(":SOURce:ROSCillator:SOURce EXTernal\n")

##inst.write(":SOURce:LOWSpur:STATe OFF")
#inst.write(":SOURce:FREQuency:FIXed 81.25 MHZ\n")
#inst.write(":SOURce:POWer:LEVel:IMMediate:AMPLitude -10 DBM\n")
#inst.write(":OUTPut:STATe ON\n")

##inst.write(":SYSTem:COMMunicate:LAN:IPCONFig MANual\n")
##inst.write(":SYSTem:COMMunicate:LAN:IPADdress:DESired 202.9.15.138\n")
##inst.write(":SYSTem:COMMunicate:LAN:GATeway:DESired 202.9.15.129\n")
##inst.write(":SYSTem:COMMunicate:LAN:SMASk:DESired 255.255.255.224\n")
##inst.write(":SYSTem:COMMunicate:LAN:IPUPDate\n")

print(inst.query(":SYSTem:COMMunicate:LAN:GATeway:RESolved?"))
print(inst.query(":SYSTem:COMMunicate:LAN:IPADdress:RESolved?"))
print(inst.query(":SYSTem:COMMunicate:LAN:SMASk:RESolved?"))

## Set device Channel 1 to 28.0 V and turn it on.
inst.close()
rm.close()
