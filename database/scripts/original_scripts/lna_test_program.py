# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the 4SPDT rack-mount switch set.
"""

import sys
##import subprocess
import socket
import pyvisa as visa

ssh_proc = None

device_ip = "10.135.151.55"
device_port = 5025

test_voltage_level = 5.0

sys.exit(0)

try:
    ## Open a socket connection and issue commands.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((device_ip, device_port))
        s.settimeout(10)

        received_data = s.recv(1024)

        command = "*IDN?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        received_data = s.recv(1024)
        print((received_data).decode("utf-8"), end="")
        
        input("Press enter to continue...")
        
        ## Set the active directory as SD Card.
        command = "MMEM:CDIR \"[SDCARD]:\"\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        command = "MMEM:LOAD:STAT \"AAVS3_15dBAtt.sta\"\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        input("Press enter when load is complete...")
        
        ## CONFIGURING THE DC OUTPUT VOLTAGE
        
        ## Make sure voltage source is off before changing the settings.
        command = "SYST:VVS:ENAB OFF\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        ## Set the voltage level to 5.00 V.
        command = "SYST:VVS:VOLT %f\n" % (test_voltage_level)
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        ## Double check the voltage before proceeding.
        ## If voltage returned = voltage requested, proceed.
        command = "SYST:VVS:RVOL?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        received_data = s.recv(1024)
        print((received_data).decode("utf-8"), end="")
        
        requested_voltage = float((received_data).decode("utf-8"))
        
        if requested_voltage != test_voltage_level:
            sys.exit(1)
        
        input("Press enter to turn on the voltage source...")

        ## Turn on the voltage source.
        command = "SYST:VVS:ENAB ON\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))




        for item in switch_states:
            command = "SET%s=%s\n" % (item[0], item[1])
            print("-> " + command, end="")
            data = s.send(command.encode("utf-8"))
            received_data = s.recv(1024)
            print((received_data).decode("utf-8"), end="")
except Exception as e:
    print(str(e))

## End the SSH process that is forwarding the port.
##ssh_proc.terminate()

sys.exit(0)

turn on fieldfox
Connect cable

## Set the active directory as SD Card.
MMEM:CDIR "[SDCARD]:"

## Recall state file.
MMEM:LOAD:STAT "AAVS3_15dBAtt.sta"

## Make sure voltage source is off before changing the settings.
SYSTem:VVS:ENABle OFF

## Set the voltage level to 5.00 V.
SYSTem:VVS:VOLTage 5.00

## Double check the voltage before proceeding.
## If voltage returned = voltage requested, proceed.
SYSTem:VVS:RVOLtage?

## Turn on the voltage source
SYSTem:VVS:ENABle ON

## Check there is no fault and the voltage and current matches what is expected.
SYSTem:VVS:STATe?
SYSTem:VVS:MVOLTage?
SYSTem:VVS:CURRent?

Sleep(20)

## Check graphs manually.

## Remove the 15 dB attenuation.
POW:ATT:AUTO OFF
POW:ATT 30

## Check graphs manually.
Is clipping occuring?

## Save data as csv and png file.
MMEMory:STORe:FDATa "BASEXXX-Black-Dome"
MMEMory:STORe:FDATa "This_is_a_fresh_test_file"
MMEMory:STORe:IMAGe BASEXXX-Black-Dome
MMEMory:STORe:IMAGe "This_is_a_fresh_test_file"

## Readd the 15 dB attenuation.
POW:ATT 15

## Turn off the voltage source.
SYSTem:VVS:ENABle OFF

Put the Fieldfox in suspended mode.
SYSTem:PWR:SUSP 1