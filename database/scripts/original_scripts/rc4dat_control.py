# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the RC4DAT programmable attenuator.
"""

import sys
##import subprocess
import socket

ssh_proc = None

device_ip = "10.135.151.34"
device_port = 23

##local_ip = "localhost"
##local_port = 1102

## Forward a test equipment port to a localhost port.
## The -N option is "Do not execute a remote command". Good for port forwarding.
##ssh_proc = subprocess.Popen("ssh -L %i:%s:%i sitf@skao-eoc.duckdns.org -p 8976 -i ./msf_pi_key -N" % (local_port, device_ip, device_port))

if (len(sys.argv) < 3) or (len(sys.argv) > 6):
    print("Usage: python3 rc4dat_control.py device_number 1:X 2:Y ...")
    print("Where X and Y are the attenuation of the corresponding channel, in dB.")
    sys.exit(1)
    
## Added to handle the addressing of the two seperate attenuators within the SSG.
if sys.argv[1] == "1":
    device_ip = "10.135.151.34"
elif sys.argv[1] == "2":
    device_ip = "10.135.151.33"
else:
    print("Usage: python3 rc4dat_control.py device_number 1:X 2:Y ...")
    print("For device_number, use 1 to control RC4DAT 1 and 2 to control RC4DAT 2.")
    sys.exit(1)

atten_states = sys.argv[2:]

ch1_count = 0
ch2_count = 0
ch3_count = 0
ch4_count = 0

## Bit of input parsing.
for item in atten_states:
    # if len(item) != 2:
    #     print("Usage: python3 rc4dat_control.py 1:X 2:Y ...")
    #     print("Where X and Y are the attenuation of the corresponding channel, in dB.")
    #     sys.exit(1)
    
    if (item[1] != ":"):
        print("Usage: python3 rc4dat_control.py 1:X 2:Y ...")
        print("Where X and Y are the attenuation of the corresponding channel, in dB.")
        sys.exit(1)        

    if (item[0] != "1") and (item[0] != "2") and (item[0] != "3") and (item[0] != "4"):
        print("Usage: python3 rc4dat_control.py 1:X 2:Y ...")
        print("Where X and Y are the attenuation of the corresponding channel, in dB.")
        sys.exit(1)

    if (item[0] == "1"):
        ch1_count += 1

    if (item[0] == "2"):
        ch2_count += 1

    if (item[0] == "3"):
        ch3_count += 1

    if (item[0] == "4"):
        ch4_count += 1
        
    attenuation = None
        
    ## Attenuation input parsing.
    try:
        amplitude = float(item[2:])
    except Exception:
        print("Attenuation must be a number between 0 and 95.")
        sys.exit(1)
        
    if (amplitude < 0) or (amplitude > 95):
        print("Attenuation must be a number between 0 and 95.")
        sys.exit(1)

if (ch1_count > 1) or (ch2_count > 1) or (ch3_count > 1) or (ch4_count > 1):
    print("Multiple states given for one switch.")
    sys.exit(2)

try:
    ## Open a socket connection and issue commands.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        ##s.connect((local_ip, local_port))
        s.connect((device_ip, device_port))
        s.settimeout(1)

        received_data = s.recv(1024)

        command = "MN?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        received_data = s.recv(1024)
        print((received_data).decode("utf-8"), end="")

        for item in atten_states:
            command = ":SetAttPerChan:%s:%s\n" % (item[0], float(item[2:]))
            print("-> " + command, end="")
            data = s.send(command.encode("utf-8"))
            received_data = s.recv(1024)
            print((received_data).decode("utf-8"), end="")
            
        command = ":ATT?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        received_data = s.recv(1024)
        print((received_data).decode("utf-8"), end="")                
except Exception as e:
    print(str(e))

## End the SSH process that is forwarding the port.
##ssh_proc.terminate()

sys.exit(0)
