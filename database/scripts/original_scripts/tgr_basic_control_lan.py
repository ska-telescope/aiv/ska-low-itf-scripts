# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the TGR2051.
"""

import sys
import subprocess
import socket

ssh_proc = None

device_port = 5025
local_port = 1027

##device_ip = "192.168.1.27"
device_ip = "202.9.15.138"

##ssh_command = "ssh -L %i:%s:%i sitf@skao-eoc.duckdns.org -p 8976 -i ./msf_pi_key -N" % (local_port, device_ip, device_port)
ssh_command = "ssh -L %i:%s:%i mcd322@venice.atnf.csiro.au -N" % (local_port, device_ip, device_port)

if (len(sys.argv) < 2) or (len(sys.argv) > 4):
    print("Usage: python3 tgr_basic_control_lan.py [on/off] freq amp")
    sys.exit(1)

gen_settings = sys.argv[1:]

gen_state = gen_settings[0]
frequency = None
amplitude = None

## Generator state input parsing.
if (gen_state != "on") and (gen_state != "off"):
    print("Usage: python3 tgr_basic_control_lan.py [on/off] freq amp")
    sys.exit(1)
  
## Frequency input parsing.
try:
    frequency = float(gen_settings[1])
except Exception:
    print("Frequency must be a number between 0.150 and 1500.")
    sys.exit(1)  
    
if (frequency < 0.150) or (frequency > 1500):
    print("Frequency must be a number between 0.150 and 1500.")
    sys.exit(1)

## Amplitude input parsing.
try:
    amplitude = float(gen_settings[2])
except Exception:
    print("Amplitude must be a number between -127 and +13.")
    sys.exit(1)
    
# if (amplitude < -127) or (amplitude > 13):
    # print("Amplitude must be a number between -127 and +13.")
    # sys.exit(1)

## Upper limit of -2.7 dB added to avoid ADC damage.
if (amplitude < -127) or (amplitude > -7):
    print("Amplitude must be a number between -127 and -2.7.")
    sys.exit(1)
    
print("INPUT PASSED")

## Forward a test equipment port to a localhost port.
## The -N option is "Do not execute a remote command". Good for port forwarding.
ssh_proc = subprocess.Popen(ssh_command)

try:
    ## Open a socket connection and issue commands.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect(("localhost", local_port))
        s.settimeout(10)
        
        ## Get the generator product number.
        command = "*IDN?\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        received_data = s.recv(1024)
        print((received_data).decode("utf-8"), end="")
        
        ## Turn off the signal generator RF output.
        command = ":OUTPut:STATe OFF\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))       
        
        ## Set the output frequency mode to fixed.
        command = ":SOURce:FREQuency:MODE FIXed\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        ## Set the default power unit as dBm.
        command = ":UNIT:POWer DBM\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        ## Use the external 10 MHz frequency reference.
        command = ":SOURce:ROSCillator:SOURce EXTernal\n"
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))        
        
        ## Set the frequency of the RF output.
        command = ":SOURce:FREQuency:FIXed %f MHZ\n" % (frequency)
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        ## Set the amplitude of the RF output.
        command = ":SOURce:POWer:LEVel:IMMediate:AMPLitude %f DBM\n" % (amplitude)
        print("-> " + command, end="")
        data = s.send(command.encode("utf-8"))
        
        if gen_state == "on":
            command = ":OUTPut:STATe ON\n"
            print("-> " + command, end="")
            data = s.send(command.encode("utf-8")) 
except Exception as error:
    print(str(error))

## End the SSH process that is forwarding the port.
try:
    ssh_proc.terminate()
except Exception as error:
    print(str(error))
    pass

sys.exit(0)