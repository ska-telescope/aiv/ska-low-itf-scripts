# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the TGR2051.
"""

import sys
import pyvisa as visa

if (len(sys.argv) < 2) or (len(sys.argv) > 4):
    print("Usage: python tgr_basic_control_usb.py [on/off] freq amp")
    sys.exit(1)

gen_settings = sys.argv[1:]

gen_state = gen_settings[0]
frequency = None
amplitude = None

## Generator state input parsing.
if (gen_state != "on") and (gen_state != "off"):
    print("Usage: python tgr_basic_control_usb.py [on/off] freq amp")
    sys.exit(1)
  
## Frequency input parsing.
try:
    frequency = float(gen_settings[1])
except Exception:
    print("Frequency must be a number between 0.150 and 1500.")
    sys.exit(1)  
    
if (frequency < 0.150) or (frequency > 1500):
    print("Frequency must be a number between 0.150 and 1500.")
    sys.exit(1)

## Amplitude input parsing.
try:
    amplitude = float(gen_settings[2])
except Exception:
    print("Amplitude must be a number between -127 and +13.")
    sys.exit(1)
    
if (amplitude < -127) or (amplitude > 13):
    print("Amplitude must be a number between -127 and +13.")
    sys.exit(1)

## The instrument address. Can be found by printing the addresses found by the 
## pyvisa resource manager.
address = "ASRL9::INSTR"

try:
    rm = visa.ResourceManager()
    print(rm.list_resources())
    inst = rm.open_resource(address)
    
    ## Get the generator product number.
    command = "*IDN?\n"
    print("-> " + command, end="")
    print(inst.query(command), end="")
    
    ## Turn off the signal generator RF output.
    command = ":OUTPut:STATe OFF\n"
    print("-> " + command, end="")
    inst.write(command)       
    
    ## Set the output frequency mode to fixed.
    command = ":SOURce:FREQuency:MODE FIXed\n"
    print("-> " + command, end="")
    inst.write(command)
    
    ## Set the default power unit as dBm.
    command = ":UNIT:POWer DBM\n"
    print("-> " + command, end="")
    inst.write(command)
    
    ## Use the external 10 MHz frequency reference.
    command = ":SOURce:ROSCillator:SOURce EXTernal\n"
    print("-> " + command, end="")
    inst.write(command)
    
    ## Set the frequency of the RF output.
    command = ":SOURce:FREQuency:FIXed %f MHZ\n" % (frequency)
    print("-> " + command, end="")
    inst.write(command)
    
    ## Set the amplitude of the RF output.
    command = ":SOURce:POWer:LEVel:IMMediate:AMPLitude %f DBM\n" % (amplitude)
    print("-> " + command, end="")
    inst.write(command)
    
    if gen_state == "on":
        command = ":OUTPut:STATe ON\n"
        print("-> " + command, end="")
        inst.write(command)
except Exception as error:
    print(str(error))
    pass

## End the SSH process that is forwarding the port.
try:
    inst.close()
    rm.close()
except Exception as error:
    print(str(error))
    pass

sys.exit(0)