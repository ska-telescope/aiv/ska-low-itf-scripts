# -*- coding: utf-8 -*-
"""
Created on Mon May 30 14:38:23 2022

@author: Cameron.Mcdonald
"""

import sys
import signal
import subprocess
import socket

ssh_proc = None

def clean_up_handler(signal_number, frame):
    print("Received signal: %s" % signal_number)
    ssh_proc.terminate()
    sys.exit(0)

local_port = 1394
device_ip = "192.168.1.13"

## Forward a test equipment port to a localhost port.
## The -N option is "Do not execute a remote command". Good for port forwarding.
ssh_proc = subprocess.Popen("ssh -L %i:%s:5024 msf-pi@skao-eoc.duckdns.org -p 8974 -i ./msf_pi_key -N" % (local_port, device_ip))

signal.signal(signal.SIGINT, clean_up_handler)
signal.signal(signal.SIGTERM, clean_up_handler)

## Open a socket connection and issue commands.
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(("localhost", local_port))
    s.settimeout(0.1)
    
    try:
        received_data = s.recv(1024)
        print(str(received_data))
    except Exception as e:
        print(str(e))

    print("CONNECTION READY - ENTER COMMANDS")
    
    while True:
        command = input().encode("utf-8") + b"\n"
        data = s.send(command)
        
        ## If the test equipment does not send a reply, "timed out" will be printed.
        try:
            received_data = s.recv(1024)
            print(str(received_data))
        except Exception as e:
            print(str(e))
