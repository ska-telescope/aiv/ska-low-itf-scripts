import pyvisa as visa #Use pyvisa library
import time
#address = "ASRL/dev/ttyUSB0::INSTR" #Raspberry Pi USB Serial Port ASRL String
address = "USB0::1510::8752::802900010737120009::0::INSTR"
rm = visa.ResourceManager('@py') #Use the pyvisa-py backend
inst = rm.open_resource(address)
inst.write("*RST")
print(inst.query("*IDN?"))
## Enable device remote control.
inst.write("SYST:REM")
## Turn all channels off.
inst.write("OUTP 0")
inst.close()
rm.close()
