# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Listens on a socket for SCPI commands and forwards them to a USB 
connected SCPI device. Sends the USB device replies back through the socket.
"""

import sys
import pyvisa as visa #Use pyvisa library
import time
import socket

host = ""
port = 5025

#address = "ASRL/dev/ttyUSB0::INSTR" #Raspberry Pi USB Serial Port ASRL String
usb_address = "USB0::1510::8752::802900010737120009::0::INSTR"

visa_inst = None
rm_session = None
conn_name = None
socket_name = None

program_time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
log_file_name = program_time_stamp + ("-psc-usb-logfile.txt")
log_file_path = "./" + log_file_name

def usb_clean_up():
    try:
        visa_inst.close()
    except Exception as error:
        write_to_log(str(error), True)

    try:
        rm_session.close()
    except Exception as error:
        write_to_log(str(error), True)

    return

def socket_clean_up():
    try:
        conn_name.close()
    except Exception as error:
        write_to_log(str(error), True)
    
    try:
        socket_name.close()
    except Exception as error:
        write_to_log(str(error), True)

    return

def write_to_log(text_to_log, print_text):
    log_time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime())
    
    with open(log_file_path, "a") as file:
        file.write(log_time_stamp + ": " + text_to_log + "\n")
        
    if print_text is True:
        print(text_to_log)
        
## Establish the log file for error messages.
with open(log_file_path, "w") as file:
    file.write("Program: " + sys.argv[0] + "\n")
    file.write("Start Time: " + program_time_stamp + "\n\n")
    file.write("Error Log:\n\n")

## Set up the connection to the power supply via USB.
try:
    rm_session = visa.ResourceManager('@py')
    visa_inst = rm_session.open_resource(usb_address)

    ##visa_inst.write("*RST")
    write_to_log(visa_inst.query("*IDN?"), True)

    ## Enable device remote control.
    visa_inst.write("SYST:REM")
except Exception as error:
    write_to_log("Failed to setup connection with instrument via USB.", True)
    write_to_log(str(error), True)
    usb_clean_up()
    sys.exit(1)

## Open a listening socket.
try:
    socket_name = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    ## Socket can be immediately reused after the program (and socket) closes.
    socket_name.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    socket_name.bind((host, port))
    socket_name.listen(1)
except Exception as error:
    write_to_log("Failed to open socket for listening.", True)
    write_to_log(str(error), True)
    socket_clean_up()
    usb_clean_up()
    sys.exit(2)

## Block until an incoming connection arrives.
while True:
    try:
        ## Accept and establish the connection.
        conn_name, addr = socket_name.accept()
    except Exception as error:
        write_to_log("Error in accepting incoming connection.", True)
        write_to_log(str(error), True)
        socket_clean_up()
        usb_clean_up()
        sys.exit(3)

    write_to_log('Connected by %s' % str(addr), True)
    conn_last_active = time.time()
    
    ## The connection will be closed after a period of time with no data received.
    receive_timout = 30
    timeout_status = False
    
    while True:
        data = b""

        ## Listen for commands coming through the socket.
        while True:
            byte = b""
            
            ## Receive bytes one at a time, non-blocking.
            try:
                byte = conn_name.recv(1, socket.MSG_DONTWAIT)
            except BlockingIOError:
                pass

            timeout_status = False
    
            if byte != b"":
                conn_last_active = time.time()
                data += byte
            
            ## We have received a full command.
            if byte == b"\n":
                break
            
            ## Timeout has occurred.
            if (byte == b"") and (time.time() > (conn_last_active + receive_timout)):
                print("Connection closed due to timeout.")
                timeout_status = True
                break
            
        if timeout_status == True:
            break
        
        command = data[:-1].decode("utf-8")

        ## Forward the received command to the instrument.
        if command[-1] == "?":
            query_resp = visa_inst.query(command)
            print(query_resp)
            
            ## Send the query response back through the socket to the connected user.
            conn_name.sendall(query_resp.encode("utf-8"))
        else:
            visa_inst.write(command)
        
    ## Close the existing connection, if it is not already closed.
    try:
        conn_name.close()
    except Exception as error:
        write_to_log(str(error), True)
   
socket_clean_up()
usb_clean_up()
sys.exit(4)
