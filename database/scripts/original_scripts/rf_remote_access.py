# mdo simple plot
# python v3.x, pyvisa v1.8
# should work with MSO70k, DPO7k, MSO5k, MDO4k, MDO3k, and MSO2k series
# 5/6 Series MSO 

# incompatible with TDS2k and TBS1k series (see tbs simple plot)

import time # std module
import sys
import signal
import subprocess

ssh_proc = None

device_ip = "10.30.10.62"

## Forward a test equipment port to a localhost port.
## The -N option is "Do not execute a remote command". Good for port forwarding.
ssh_proc_1 = subprocess.Popen("ssh -L 80:%s:80 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))
ssh_proc_2 = subprocess.Popen("ssh -L 443:%s:443 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))
ssh_proc_3 = subprocess.Popen("ssh -L 2947:%s:2947 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))
ssh_proc_4 = subprocess.Popen("ssh -L 4880:%s:4880 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))
ssh_proc_5 = subprocess.Popen("ssh -L 4881:%s:4881 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))
ssh_proc_6 = subprocess.Popen("ssh -L 37000:%s:37000 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))
ssh_proc_7 = subprocess.Popen("ssh -L 37001:%s:37001 debian@skao-eoc.duckdns.org -p 5238 -i ./eoc_bbb_key -N" % (device_ip))

input("Press enter to close the connections.")

ssh_proc_1.terminate()
ssh_proc_2.terminate()
ssh_proc_3.terminate()
ssh_proc_4.terminate()
ssh_proc_5.terminate()
ssh_proc_6.terminate()
ssh_proc_7.terminate()