# -*- coding: utf-8 -*-
"""
Created on 26/04/2022

@author: Cameron.McDonald

Description: Script for testing the RSA5103B socket server.
"""

import sys
import socket

device_ip = "10.135.151.41"
device_port = 5025

log_file_path = ""

## Local time zone offset in seconds.
time_zone_offset = 8 * 3600

def log_data(log_line):
    time_stamp = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))

    with open(log_file_path, "a") as f:
        f.write(time_stamp + " " + log_line + "\n")

while True:
    log_line = time.strftime("%Y-%m-%d-%H-%M-%S", time.gmtime(time.time() + time_zone_offset))

    try:
        ## Open a socket connection and issue commands.
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((device_ip, device_port))
            s.settimeout(1)

            received_data = s.recv(1024)

            command = "IDN?\n"
            s.send(command.encode("utf-8"))
            log_data("<- " + command)
            
            received_data = s.recv(1024)
            log_data((received_data).decode("utf-8"))            
    except Exception as e:
        print(str(e))

sys.exit(0)
