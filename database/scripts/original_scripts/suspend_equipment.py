"""
VISA: SourceXpress/AWG Sequence Builder and Channel Skew Adjuster
Author: Morgan Allison
Date created: 5/17
Date edited: 5/17
Creates a sequence of two waveforms with an external trigger dependency
and configures the AWG to change its phase between waveform outputs
Windows 7 64-bit
Python 3.6.0 64-bit (Anaconda 4.3.0)
NumPy 1.11.2, PyVISA 1.8, PyVISA-py 0.2
Download Anaconda: http://continuum.io/downloads
Anaconda includes NumPy
"""

import pyvisa as visa
import numpy as np

local_ports = [1012, 1013, 1015, 1016]
ip_address = "10.30.10.105"

# Change this to connect to your AWG as needed
"""#################SEARCH/CONNECT#################"""

for i in range(0, len(local_ports)):
    rm = visa.ResourceManager()
    inst = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (ip_address, local_port))
    inst.timeout = 25000
    inst.encoding = 'latin_1'
    inst.write_termination = '\n'
    inst.read_termination = '\n'

    print(inst.query('*idn?'))
    inst.write('*rst')
    inst.write('*cls')
    inst.close()
