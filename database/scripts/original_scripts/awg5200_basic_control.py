# -*- coding: utf-8 -*-
"""
@author: Cameron.Mcdonald
"""

import pyvisa as visa
import numpy as np

local_port = 5025
device_ip = "10.135.151.42"

# Change this to connect to your AWG as needed
"""#################SEARCH/CONNECT#################"""
rm = visa.ResourceManager()
print(rm.list_resources())
awg = rm.open_resource("TCPIP0::%s::%i::SOCKET" % (device_ip, local_port))

awg.timeout = 25000
awg.encoding = 'latin_1'
awg.write_termination = '\n'
awg.read_termination = '\n'

print(awg.query('*idn?'))
##awg.write('*rst')
##awg.write('*cls')

frequency = 200e6
record_length = 5000

## Use a fixed sample rate of 2.5 GHz.
sample_rate = 2500000000

wave_name = "new_wave_1"
new_wave_data_array = []

t = 0

while True:
    new_wave_data_array.append(np.sin(2 * np.pi * frequency * (t / sample_rate)))
    
    ## Try to get the last sample as close to zero as possible.
    if ((new_wave_data_array[t] < 0.001) & (new_wave_data_array[t] > -0.001)):
        if ((t > record_length) & (new_wave_data_array[t] > new_wave_data_array[t - 1])):
            break
            
    t += 1
    
new_wave_data = np.asarray(new_wave_data_array[0:-1])
record_length = len(new_wave_data)

print(new_wave_data)
print(len(new_wave_data))

## Script overwrites any exisitng waveform with the same name.
awg.write('wlist:waveform:del \"%s\"' % wave_name)
awg.write('wlist:waveform:new "%s", %i' % (wave_name, record_length))

## Load the data array into the AWG.
load_command = 'wlist:waveform:data "%s", 0, %i, ' % (wave_name, record_length)
awg.write_binary_values(load_command, new_wave_data)
awg.query('*opc?')

awg.write('clock:srate {}'.format(sample_rate))

## Turn on and output from Channel 2.
awg.write('source2:casset:waveform "%s"' % wave_name)
awg.write('output2:state on')

awg.write('awgcontrol:run:immediate')
awg.query('*opc?')

print(awg.query('system:error:all?'))
awg.close()
