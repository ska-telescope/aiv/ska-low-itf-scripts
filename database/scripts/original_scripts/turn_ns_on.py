# -*- coding: utf-8 -*-
"""
Created on 28/09/2022

@author: Cameron.McDonald

Description: Basic script for controlling the 4SPDT rack-mount switch set.
"""

import pyvisa as visa #Use pyvisa library
import time
#address = "ASRL/dev/ttyUSB0::INSTR" #Raspberry Pi USB Serial Port ASRL String
address = "USB0::1510::8752::802900010737120009::0::INSTR"
rm = visa.ResourceManager('@py') #Use the pyvisa-py backend
#print(rm.list_resources())
inst = rm.open_resource(address)
inst.write("*RST")
print(inst.query("*IDN?"))
## Enable device remote control.
inst.write("SYST:REM")
## Set device Channel 1 to 28.0 V and turn it on.
inst.write("OUTP 0")
inst.write("INST CH1")
inst.write("VOLT 28")
inst.write("CHAN:OUTP 1")
time.sleep(3)
print(inst.query("FETC:CURR?"))
#inst.write("DISP:TEXT \"Felipe\"")
inst.close()
rm.close()
