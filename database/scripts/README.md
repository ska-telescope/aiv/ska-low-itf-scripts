# SKA Low ITF scripts

This project is intended to contain Low ITF user-provided scripts and notebooks that implement ITF test cases.

## Getting started

See the [Getting Started guide](docs/getting-started.md) on how to get started with using the environment.

## Developing notebooks

tba

## Support module

This project also contains some helper functions for notebooks, e.g. configuration capture.
When ska-low-itf-scripts is mounted in your jupyterhub, please add this directory to your `PYTHONPATH`. See the `shared-data` examples for some inspiration.
