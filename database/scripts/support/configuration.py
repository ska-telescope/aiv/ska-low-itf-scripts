"""Helper module for configuration in notebooks."""
import tango
from dsconfig.dump import get_db_data
from ska_k8s_config_exporter.utils import (
    get_helm_info,
    get_pod_list,
    initialise_k8s,
)
import pandas

__all__ = [
    "get_tango_config",
    "get_pods",
    "get_charts",
]

NAMESPACES = ("test-environment", "sut")
DATABASE_NAME = "tango-databaseds"
PORT = 10000
GIT_URL = "https://gitlab.com/ska-telescope/aiv/ska-low-itf-config/-/raw/main/"


def database_uri(db_name, namespace) -> str:
    """Get database with namespace domain.

    :param db_name: the host name
    :param namespace: the namespace
    :return: new host string
    """
    return f"{db_name}.{namespace}"


def get_tango_config(
    namespaces: list[str] = NAMESPACES,
    db_port: int = PORT,
    db_name=DATABASE_NAME,
) -> dict:
    """Get tango database device server configuration.

    :param namespaces: list of database names, defaults to NAMESPACES
    :param db_port: the port on the host, defaults to PORT
    :param db_name: the name of the tango database host, defaults to
                    DATABASE_NAME
    :return: a dictionary configurations
    """
    config = {}
    for namespace in namespaces:
        db_uri = database_uri(db_name, namespace)
        database = tango.Database(db_uri, db_port)
        config[namespace] = get_db_data(database)
    return config


def get_pods(namespaces: tuple[str] = NAMESPACES) -> dict:
    """Get the pod configuration for given namespaces.

    :param namespaces: the namespaces to inspects, defaults to NAMESPACES
    :return: a (json) dict
    """
    core_v1_api = initialise_k8s()
    pods_info = {}
    for namespace in namespaces:
        pods_info[namespace] = get_pod_list(
            core_v1_api, namespace, pip_inspect=False
        )
    return pods_info


def get_charts(namespaces: tuple[str] = NAMESPACES) -> dict:
    """Get the chart configuration for given namespaces.

    :param namespaces: the namespaces to inspects, defaults to NAMESPACES
    :return: a (json) dict
    """
    core_v1_api = initialise_k8s()
    chart_info = {}
    for namespace in namespaces:
        chart_info.update(get_helm_info(core_v1_api, namespace))
    return chart_info


def get_hardware_config(
    config_files: list[str] = None, git_url: str = None
) -> dict:
    """Get the configuration from a git repository.

    :param git_url: the git url
    :return: dict of config items
    """
    if git_url is None:
        git_url = GIT_URL
    data_frames = []
    for cfg_file in config_files:
        csv_file = f"{git_url}{cfg_file}"
        data_frames.append(pandas.read_csv(csv_file, keep_default_na=False))
    return data_frames


if __name__ == "__main__":
    print(get_charts())
    print(get_pods())
    print(get_tango_config())
