# SKA Low ITF scripts

This project is intended to contain Low ITF user-provided scripts and notebooks.

Notebooks that implement Low ITF Integration Event Test Cases used to live here,
but are now located in the [ska-low-tests](https://gitlab.com/ska-telescope/ska-low-tests) repository.


## Getting started

See the [Getting Started guide](docs/getting-started.md) on how to get started with using the environment.
